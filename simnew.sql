-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 12, 2020 at 12:28 PM
-- Server version: 10.3.25-MariaDB-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simnew`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `kode_barang` varchar(128) DEFAULT NULL,
  `nama_barang` varchar(128) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `keuntungan` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `kode_barang`, `nama_barang`, `harga_beli`, `harga_jual`, `keuntungan`, `stok`, `id_kategori`, `id_satuan`, `create_at`, `update_at`) VALUES
(28, '001', 'tes', 3000, 4000, 1000, 26, 14, 3, '2020-10-20 01:45:42', NULL),
(29, '002', 'tes2', 5000, 7000, 2000, 1, 13, 3, '2020-10-20 01:45:42', NULL),
(30, '003', 'tes3', 1000, 2000, 1000, 9, 13, 4, '2020-10-20 01:45:42', NULL),
(31, '004', 'Bensin', 10000, 15000, 5000, 0, 13, 4, '2020-10-20 10:50:11', NULL),
(32, '005', 'AleAle', 1000, 1500, 500, 15, 13, 2, '2020-11-11 23:20:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `id_barang_keluar` int(11) NOT NULL,
  `nota` varchar(128) DEFAULT NULL,
  `kode_barang` varchar(128) DEFAULT NULL,
  `nama_barang` varchar(128) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `keuntungan` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_keluar`
--

INSERT INTO `barang_keluar` (`id_barang_keluar`, `nota`, `kode_barang`, `nama_barang`, `tanggal`, `stok`, `harga_jual`, `harga_beli`, `diskon`, `keuntungan`, `id_kategori`, `id_satuan`, `create_at`) VALUES
(12, 'N2020070027', '001', 'tes', '2020-11-07', 1, 4000, 3000, 10, 600, 14, 3, '2020-11-07'),
(13, 'N2020080027', '001', 'tes', '2020-11-08', 1, 16000, 3000, 0, 4000, 14, 3, '2020-11-08'),
(15, 'N2020110027', '002', 'tes2', '2020-11-11', 3, 7000, 5000, 20, 1800, 13, 3, '2020-11-12'),
(16, 'N2020110027', '003', 'tes3', '2020-11-11', 1, 2000, 1000, 5, 900, 13, 4, '2020-11-12'),
(18, 'N2020110027', '001', 'tes', '2020-11-11', 1, 4000, 3000, 10, 600, 14, 3, '2020-11-12'),
(20, 'N2020120027', '005', 'AleAle', '2020-11-12', 1, 1500, 1000, 0, 500, 13, 2, '2020-11-12'),
(21, 'N2020120027', '005', 'AleAle', '2020-11-12', 2, 1500, 1000, 5, 850, 13, 2, '2020-11-12'),
(24, 'N2020290001', '001', 'tes', '2020-10-29', 3, 4000, 3000, 10, 1800, 14, 3, '2020-10-29'),
(25, 'N2020290001', '002', 'tes2', '2020-10-29', 2, 7000, 5000, 30, -200, 13, 3, '2020-10-29'),
(26, 'N2020290001', '003', 'tes3', '2020-10-29', 1, 7000, 5000, 0, 2000, 13, 4, '2020-10-29'),
(27, 'N2020120027', '005', 'AleAle', '2020-11-12', 1, 1500, 1000, 0, 500, 13, 2, '2020-11-12');

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar_tmp`
--

CREATE TABLE `barang_keluar_tmp` (
  `id_barang_keluar` int(11) NOT NULL,
  `nota` varchar(128) DEFAULT NULL,
  `kode_barang` varchar(128) DEFAULT NULL,
  `nama_barang` varchar(128) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `keuntungan` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_keluar_tmp`
--

INSERT INTO `barang_keluar_tmp` (`id_barang_keluar`, `nota`, `kode_barang`, `nama_barang`, `tanggal`, `stok`, `harga_jual`, `harga_beli`, `diskon`, `keuntungan`, `id_kategori`, `id_satuan`, `create_at`) VALUES
(5, 'N2020210004', '004', 'Bensin', '2020-10-21', 10, 15000, 10000, 0, 25000, 13, 4, '2020-10-21'),
(9, 'N2020220009', '003', 'tes3', '2020-10-22', 2, 3000, 1000, 0, 4000, 13, 4, '2020-10-21'),
(11, 'N2020220009', '003', 'tes3', '2020-10-22', 1, 3000, 1000, 1, 1970, 13, 4, '2020-10-22');

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id_barang_masuk` int(11) NOT NULL,
  `nota` varchar(128) DEFAULT NULL,
  `kode_barang` varchar(128) DEFAULT NULL,
  `nama_barang` varchar(128) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `diskon` int(11) NOT NULL,
  `keuntungan` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id_barang_masuk`, `nota`, `kode_barang`, `nama_barang`, `tanggal`, `stok`, `harga_jual`, `harga_beli`, `diskon`, `keuntungan`, `id_kategori`, `id_satuan`, `create_at`) VALUES
(1, '001', '003', 'testes', '2020-11-07', 1, 2000, 1000, 0, 1000, 12, 3, '2020-11-07'),
(2, '001', '001', 'tes', '2020-10-29', 5, 4000, 3000, 0, 1000, 12, 2, '2020-10-29'),
(3, '002', '002', 'tes2', '2020-10-29', 3, 7000, 5000, 0, 2000, 12, 2, '2020-10-29'),
(4, '003', '003', 'tes4', '2020-10-29', 10, 7000, 5000, 0, 2000, 12, 2, '2020-10-29'),
(6, 'B0001', '005', 'AleAle', '2020-11-12', 5, 1500, 1000, 0, 500, 13, 2, '2020-11-12');

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk_tmp`
--

CREATE TABLE `barang_masuk_tmp` (
  `id_barang_masuk` int(11) NOT NULL,
  `nota` varchar(128) DEFAULT NULL,
  `kode_barang` varchar(128) DEFAULT NULL,
  `nama_barang` varchar(128) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `keuntungan` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_umum`
--

CREATE TABLE `jurnal_umum` (
  `id_jurnal_umum` int(11) NOT NULL,
  `kode_akun` int(5) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `debet` int(11) DEFAULT 0,
  `kredit` int(11) DEFAULT 0,
  `create_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jurnal_umum`
--

INSERT INTO `jurnal_umum` (`id_jurnal_umum`, `kode_akun`, `keterangan`, `debet`, `kredit`, `create_at`) VALUES
(1, 1111, 'yagitu deh', 500000, NULL, '2020-11-08 15:37:31'),
(2, 2102, 'yagitu deh', NULL, 200000, '2020-11-08 15:37:31'),
(3, 1111, 'gitudeh', 10000000, 0, '2020-11-08 16:00:09'),
(4, 4101, 'gitudeh', 0, 10000000, '2020-11-08 16:00:09'),
(5, 1121, 'gini', 2000000, 0, '2020-11-08 16:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `kas_keluar`
--

CREATE TABLE `kas_keluar` (
  `id_kas_keluar` int(11) NOT NULL,
  `kode_akun` int(5) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `debet` int(11) DEFAULT 0,
  `kredit` int(11) DEFAULT 0,
  `tanggal` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kas_keluar`
--

INSERT INTO `kas_keluar` (`id_kas_keluar`, `kode_akun`, `keterangan`, `debet`, `kredit`, `tanggal`) VALUES
(2, 2101, 'ngutang kopi satu renceng', 0, 1500000, '2020-11-08'),
(3, 3001, 'ngabisin modal', 0, 100000, '2020-11-08'),
(4, 2102, 'ga ada duit buat bayar gaji', 0, 50000, '2020-11-08');

-- --------------------------------------------------------

--
-- Table structure for table `kas_masuk`
--

CREATE TABLE `kas_masuk` (
  `id_kas_masuk` int(11) NOT NULL,
  `kode_akun` int(5) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `debet` int(11) DEFAULT 0,
  `kredit` int(11) DEFAULT 0,
  `tanggal` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kas_masuk`
--

INSERT INTO `kas_masuk` (`id_kas_masuk`, `kode_akun`, `keterangan`, `debet`, `kredit`, `tanggal`) VALUES
(1, 1111, 'jualan keliling', 300000, 0, '2020-11-08'),
(2, 1112, 'Jualan Aja', 150000, 0, '2020-11-08'),
(3, 1112, 'Dapet pinjeman nich', 30000000, 0, '2020-11-08'),
(4, 1112, 'Dapet duit gratis', 20000, 0, '2020-11-08'),
(6, 1121, 'deposito', 100000, 0, '2020-11-08');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(128) DEFAULT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`, `create_at`) VALUES
(12, 'Minuman', '2020-10-27 09:12:45'),
(13, 'Snack', '2020-10-27 09:12:45'),
(14, 'ATK', '2020-10-27 09:12:45'),
(20, 'souvenir', '2020-10-27 09:12:45'),
(21, 'Sembako', '2020-10-27 09:12:45');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_akun`
--

CREATE TABLE `kategori_akun` (
  `kode_akun` int(5) NOT NULL,
  `nama_akun` varchar(200) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `kelompok` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori_akun`
--

INSERT INTO `kategori_akun` (`kode_akun`, `nama_akun`, `jenis`, `kelompok`) VALUES
(1111, 'Kas', 'DEBET', 'NRC'),
(1112, 'Bank', 'DEBET', 'NRC'),
(1121, 'Deposito di BPR', 'DEBET', 'NRC'),
(1131, 'Piutang Dagang', 'DEBET', 'NRC'),
(1132, 'Cadangan Kerugian Piutang', 'DEBET', 'NRC'),
(1133, 'Piutang Karyawan', 'DEBET', 'NRC'),
(1134, 'Piutang Lain-lain', 'DEBET', 'NRC'),
(1141, 'Persediaan Barang', 'DEBET', 'NRC'),
(1151, 'Persekot Biaya Perjalanan', 'DEBET', 'NRC'),
(1152, 'Persekot Biaya Asuransi', 'DEBET', 'NRC'),
(1153, 'Sewa Dibayar Dimuka', 'DEBET', 'NRC'),
(1201, 'Investasi Jangka Panjang', 'DEBET', 'NRC'),
(1311, 'Tanah', 'DEBET', 'NRC'),
(1312, 'Peralatan Toko', 'DEBET', 'NRC'),
(1313, 'Kendaraan', 'DEBET', 'NRC'),
(1322, 'Ak. Penyusutan Peralatan Toko', 'DEBET', 'NRC'),
(1323, 'Ak. Penyusutan Kendaraan', 'DEBET', 'NRC'),
(2101, 'Utang Dagang', 'KREDIT', 'NRC'),
(2102, 'Utang Gaji', 'KREDIT', 'NRC'),
(2103, 'Utang Jangka Pendek Bank', 'KREDIT', 'NRC'),
(2104, 'Utang Jangka Pendek Lain-lain', 'KREDIT', 'NRC'),
(2201, 'Utang Jangka Panjang dari Bank', 'KREDIT', 'NRC'),
(3001, 'Modal Pemilik', 'KREDIT', 'NRC'),
(3002, 'Prive Pemilik', 'DEBET', 'NRC'),
(3003, 'Laba Periode Berjalan', 'KREDIT', 'NRC'),
(4101, 'Penjualan', 'KREDIT', 'LR '),
(4201, 'Retur penjualan', 'DEBET', 'LR'),
(4202, 'Potongan Penjualan', 'DEBET', 'LR'),
(4203, 'Retur Pembelian', 'KREDIT', 'LR'),
(4204, 'Potongan Pembelian', 'KREDIT', 'LR'),
(5101, 'Harga Pokok Penjualan ', 'DEBET', 'LR'),
(5211, 'Biaya Gaji ', 'DEBET', 'LR '),
(5212, 'Biaya Pemasaran', 'DEBET', 'LR '),
(5213, 'Biaya Penyusutan Peralatan Toko', 'DEBET', 'LR '),
(5214, 'Biaya Pengiriman', 'DEBET', 'LR'),
(5215, 'Biaya Penjualan Rupa-rupa', 'DEBET', 'LR '),
(5216, 'Biaya Sewa', 'DEBET', 'LR '),
(5217, 'Biaya Penyusutan Kendaraan', 'DEBET', 'LR '),
(5218, 'Biaya Perjalanan', 'DEBET', 'LR '),
(5219, 'Biaya Asuransi', 'DEBET', 'LR '),
(5220, 'Biaya Perlengkapan Toko', 'DEBET', 'LR '),
(5221, 'Biaya Administrasi Rupa-rupa', 'DEBET', '  LR '),
(5222, 'Biaya Listrik, Telpon & Air', 'DEBET', 'LR'),
(5223, 'Biaya Lain-lain', 'DEBET', '  LR '),
(6101, 'Pendapatan Lain-lain', 'KREDIT', '  LR ');

-- --------------------------------------------------------

--
-- Table structure for table `log_stok`
--

CREATE TABLE `log_stok` (
  `id_log` int(11) NOT NULL,
  `kode_barang` varchar(128) DEFAULT NULL,
  `id_barang_keluar` int(11) DEFAULT NULL,
  `id_barang_masuk` int(11) DEFAULT NULL,
  `id_retur` int(11) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `stok` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `log_stok`
--

INSERT INTO `log_stok` (`id_log`, `kode_barang`, `id_barang_keluar`, `id_barang_masuk`, `id_retur`, `tanggal`, `stok`, `keterangan`) VALUES
(1, '001', NULL, 2, NULL, '2020-10-29', 5, 'Barang Masuk'),
(2, '002', NULL, 3, NULL, '2020-10-29', 3, 'Barang Masuk'),
(3, '003', NULL, 4, NULL, '2020-10-29', 10, 'Barang Masuk'),
(4, '001', 24, NULL, NULL, '2020-10-29', 3, 'Barang Keluar'),
(5, '002', 25, NULL, NULL, '2020-10-29', 2, 'Barang Keluar '),
(6, '003', 26, NULL, NULL, '2020-10-29', 1, 'Barang Keluar'),
(8, '001', NULL, 2, 1, '2020-10-29', 1, 'Retur Barang Masuk'),
(9, '001', NULL, 2, 2, '2020-10-29', 2, 'Retur Barang Masuk'),
(10, '001', 24, NULL, 3, '2020-10-29', 1, 'Retur Barang Keluar'),
(11, '001', 25, NULL, 4, '2020-10-29', 1, 'Retur Barang Keluar'),
(12, '001', 12, NULL, NULL, '2020-11-07', 1, 'Barang Keluar'),
(13, '003', NULL, 1, NULL, '2020-11-07', 1, 'Barang Masuk'),
(14, '001', 13, NULL, NULL, '2020-11-08', 1, 'Barang Keluar'),
(15, '005', NULL, 5, NULL, '2020-11-12', 5, 'Barang Masuk'),
(16, '005', NULL, 6, NULL, '2020-11-12', 5, 'Barang Masuk'),
(17, '002', 15, NULL, NULL, '2020-11-12', 3, 'Barang Keluar'),
(18, '003', 16, NULL, NULL, '2020-11-12', 1, 'Barang Keluar'),
(19, '001', 18, NULL, NULL, '2020-11-12', 1, 'Barang Keluar'),
(20, '005', 19, NULL, NULL, '2020-11-12', 1, 'Barang Keluar'),
(21, '005', 20, NULL, NULL, '2020-11-12', 1, 'Barang Keluar'),
(22, '005', 21, NULL, NULL, '2020-11-12', 2, 'Barang Keluar'),
(23, '005', 27, NULL, NULL, '2020-11-12', 1, 'Barang Keluar');

-- --------------------------------------------------------

--
-- Table structure for table `modal`
--

CREATE TABLE `modal` (
  `id_modal` int(11) NOT NULL,
  `modal` int(11) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modal`
--

INSERT INTO `modal` (`id_modal`, `modal`, `create_at`, `update_at`) VALUES
(67, 10000, '2020-09-10 17:27:12', '2020-09-11 17:00:00'),
(68, 2147483647, '2020-09-12 00:19:57', '2020-09-11 17:00:00'),
(69, 1000000, '2020-09-15 23:25:17', '2020-09-14 17:00:00'),
(71, 50000, '2020-09-17 04:51:28', NULL),
(72, 10000, '2020-09-18 22:53:38', NULL),
(73, 10000, '2020-10-10 08:39:16', NULL),
(74, 1000000, '2020-10-15 08:41:56', NULL),
(75, 1000000, '2020-10-17 07:11:29', NULL),
(76, 100000, '2020-10-19 11:06:36', NULL),
(77, 100000, '2020-10-20 01:01:18', NULL),
(78, 100000, '2020-10-20 17:34:30', NULL),
(79, 100000, '2020-10-22 09:22:36', NULL),
(80, 100000, '2020-10-24 07:18:24', NULL),
(81, 1000000, '2020-10-25 07:26:22', NULL),
(82, 100000, '2020-10-27 02:10:52', NULL),
(83, 1000000, '2020-10-28 04:45:03', NULL),
(84, 100000, '2020-10-29 02:30:49', NULL),
(85, 100000, '2020-10-30 06:31:29', NULL),
(86, 200000, '2020-10-31 04:03:44', NULL),
(88, 100000, '2020-11-07 15:06:58', NULL),
(89, 100000, '2020-11-08 03:18:14', NULL),
(90, 100000, '2020-11-09 11:29:48', NULL),
(91, 1000000, '2020-11-10 03:55:25', NULL),
(93, 100000, '2020-11-10 23:58:35', NULL),
(94, 100000, '2020-11-11 22:08:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `retur`
--

CREATE TABLE `retur` (
  `id_retur` int(11) NOT NULL,
  `nota` varchar(128) NOT NULL,
  `kode_barang` varchar(128) NOT NULL,
  `nama_barang` varchar(128) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `diskon` int(11) DEFAULT NULL,
  `sub_total` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `nama_supplier` varchar(128) NOT NULL,
  `tipe_retur` varchar(128) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retur`
--

INSERT INTO `retur` (`id_retur`, `nota`, `kode_barang`, `nama_barang`, `stok`, `harga_jual`, `diskon`, `sub_total`, `tanggal`, `nama_supplier`, `tipe_retur`, `create_at`) VALUES
(1, '001', '001', 'tes', 1, 4000, 0, 4000, '2020-10-29', 'Budi', 'pembelian', '2020-10-29 00:00:00'),
(2, '001', '001', 'tes', 2, 4000, 0, 8000, '2020-10-29', 'Budi', 'pembelian', '2020-10-29 00:00:00'),
(3, 'N2020290001', '001', 'tes', 1, 4000, 10, 3600, '2020-10-29', 'Bambang', 'penjualan', '2020-10-29 00:00:00'),
(4, 'N2020290001', '001', 'tes', 1, 4000, 10, 3600, '2020-10-29', 'Bambang', 'penjualan', '2020-10-29 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id_satuan` int(11) NOT NULL,
  `nama_satuan` varchar(128) DEFAULT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `nama_satuan`, `create_at`) VALUES
(2, 'Pack', '2020-10-27 09:13:05'),
(3, 'Biji', '2020-10-27 09:13:05'),
(4, 'Liter', '2020-10-27 09:13:05');

-- --------------------------------------------------------

--
-- Table structure for table `surat`
--

CREATE TABLE `surat` (
  `id_surat` int(11) NOT NULL,
  `nomor_surat` varchar(128) DEFAULT NULL,
  `instansi` varchar(123) DEFAULT NULL,
  `topik` varchar(128) DEFAULT NULL,
  `type` enum('masuk','keluar') DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `scan` varchar(128) DEFAULT 'default.jpg',
  `create_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surat`
--

INSERT INTO `surat` (`id_surat`, `nomor_surat`, `instansi`, `topik`, `type`, `tanggal`, `scan`, `create_at`) VALUES
(26, 'asdass', 'sdasd', 'asdasdasd', 'masuk', '2020-10-30', '3.JPG', '2020-10-28 00:00:00'),
(28, '123123123ssss', '1231231', '123123123', 'keluar', '2020-10-31', '111sss.png', '2020-10-29 00:00:00'),
(30, '34354', '345345', '345345', 'keluar', '2020-10-29', '20191021_163939.jpg', '2020-10-31 00:00:00'),
(31, '1234', 'gitudeh', 'gininih', 'masuk', '2020-10-31', '3.JPG', '2020-10-31 06:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `kd_user` varchar(128) DEFAULT NULL,
  `nama_user` varchar(128) DEFAULT NULL,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `alamat` varchar(128) DEFAULT NULL,
  `group_user` enum('Administrator','User') DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `kd_user`, `nama_user`, `username`, `password`, `alamat`, `group_user`, `create_at`, `update_at`) VALUES
(56, 'SKPM-2020020001', 'admin', 'admin', '$2y$10$mqXSKZbnsTvOPoJeQKGz.O7f7F4DPCTsypGXLwfBmygsZTq5lIvjy', 'KOPMA-UMM', 'Administrator', '2020-05-02 07:54:04', NULL),
(79, 'SKPM-2020020010', 'Ida firdiana', 'admin', '$2y$10$4g9TnpBiGXVQbSn4tNsHgu6eHjwcVaRxNElYa85yFvAaJBJST4sf2', 'Jl tirto utomo no 36', 'Administrator', '2020-05-02 12:07:36', NULL),
(284, 'SKPM-2020090011', 'user', 'user', '$2y$10$psjXv.Jz/WJ5.D58Q4CZ0uRS4y5CyIHKohpwIxPQTx8d1olP/h3vm', 'adsasasd', 'User', '2020-05-09 07:20:18', NULL),
(289, 'SKPM-2020130016', 'tes', 'tes', '$2y$10$qkCtdqwLfuY9AlabRqb37e3HvHM2Zq12OcWy9rfRr39NoHPA.tsqS', 'tes', 'Administrator', '2020-06-13 10:36:08', NULL),
(292, 'SKPM-2020040017', 'asdkaksd', 'askdkasd', '$2y$10$5qHBXfPMpk.tJRChjRjXIuBfD4OeJMyZKjl78a6O8.67vLne678jW', 'aksdkaskd', 'Administrator', '2020-07-04 11:45:36', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id_barang_keluar`);

--
-- Indexes for table `barang_keluar_tmp`
--
ALTER TABLE `barang_keluar_tmp`
  ADD PRIMARY KEY (`id_barang_keluar`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id_barang_masuk`);

--
-- Indexes for table `barang_masuk_tmp`
--
ALTER TABLE `barang_masuk_tmp`
  ADD PRIMARY KEY (`id_barang_masuk`);

--
-- Indexes for table `jurnal_umum`
--
ALTER TABLE `jurnal_umum`
  ADD PRIMARY KEY (`id_jurnal_umum`);

--
-- Indexes for table `kas_keluar`
--
ALTER TABLE `kas_keluar`
  ADD PRIMARY KEY (`id_kas_keluar`);

--
-- Indexes for table `kas_masuk`
--
ALTER TABLE `kas_masuk`
  ADD PRIMARY KEY (`id_kas_masuk`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kategori_akun`
--
ALTER TABLE `kategori_akun`
  ADD PRIMARY KEY (`kode_akun`);

--
-- Indexes for table `log_stok`
--
ALTER TABLE `log_stok`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `modal`
--
ALTER TABLE `modal`
  ADD PRIMARY KEY (`id_modal`);

--
-- Indexes for table `retur`
--
ALTER TABLE `retur`
  ADD PRIMARY KEY (`id_retur`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `id_barang_keluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `barang_keluar_tmp`
--
ALTER TABLE `barang_keluar_tmp`
  MODIFY `id_barang_keluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id_barang_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `barang_masuk_tmp`
--
ALTER TABLE `barang_masuk_tmp`
  MODIFY `id_barang_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jurnal_umum`
--
ALTER TABLE `jurnal_umum`
  MODIFY `id_jurnal_umum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kas_keluar`
--
ALTER TABLE `kas_keluar`
  MODIFY `id_kas_keluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kas_masuk`
--
ALTER TABLE `kas_masuk`
  MODIFY `id_kas_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `log_stok`
--
ALTER TABLE `log_stok`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `modal`
--
ALTER TABLE `modal`
  MODIFY `id_modal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `retur`
--
ALTER TABLE `retur`
  MODIFY `id_retur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=293;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
