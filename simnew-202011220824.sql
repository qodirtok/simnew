-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: simnew
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barang`
--

DROP TABLE IF EXISTS `barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(128) DEFAULT NULL,
  `nama_barang` varchar(128) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `keuntungan` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang`
--

LOCK TABLES `barang` WRITE;
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` VALUES (28,'001','tes',3000,4000,1000,11,14,3,'2020-10-20 01:45:42',NULL),(29,'002','tes2',5000,7000,2000,0,13,3,'2020-10-20 01:45:42',NULL),(30,'003','tes3',1000,2000,1000,6,13,4,'2020-10-20 01:45:42',NULL),(31,'004','Bensin',10000,15000,5000,0,13,4,'2020-10-20 10:50:11',NULL),(32,'005','AleAle',1000,1500,500,15,13,2,'2020-11-11 23:20:10',NULL);
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_keluar`
--

DROP TABLE IF EXISTS `barang_keluar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_keluar` (
  `id_barang_keluar` int(11) NOT NULL AUTO_INCREMENT,
  `nota` varchar(128) DEFAULT NULL,
  `kode_barang` varchar(128) DEFAULT NULL,
  `nama_barang` varchar(128) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `keuntungan` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL,
  PRIMARY KEY (`id_barang_keluar`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_keluar`
--

LOCK TABLES `barang_keluar` WRITE;
/*!40000 ALTER TABLE `barang_keluar` DISABLE KEYS */;
INSERT INTO `barang_keluar` VALUES (12,'N2020070027','001','tes','2020-11-07',1,4000,3000,10,600,14,3,'2020-11-07'),(13,'N2020080027','001','tes','2020-11-08',1,16000,3000,0,4000,14,3,'2020-11-08'),(15,'N2020110027','002','tes2','2020-11-11',3,7000,5000,20,1800,13,3,'2020-11-12'),(16,'N2020110027','003','tes3','2020-11-11',1,2000,1000,5,900,13,4,'2020-11-12'),(18,'N2020110027','001','tes','2020-11-11',1,4000,3000,10,600,14,3,'2020-11-12'),(20,'N2020120027','005','AleAle','2020-11-12',1,1500,1000,0,500,13,2,'2020-11-12'),(21,'N2020120027','005','AleAle','2020-11-12',2,1500,1000,5,850,13,2,'2020-11-12'),(24,'N2020290001','001','tes','2020-10-29',3,4000,3000,10,1800,14,3,'2020-10-29'),(25,'N2020290001','002','tes2','2020-10-29',2,7000,5000,30,-200,13,3,'2020-10-29'),(26,'N2020290001','003','tes3','2020-10-29',1,7000,5000,0,2000,13,4,'2020-10-29'),(27,'N2020120027','005','AleAle','2020-11-12',1,1500,1000,0,500,13,2,'2020-11-12'),(29,'N2020180029','001','tes','2020-11-18',2,4000,3000,0,2000,14,3,'2020-11-18'),(30,'N2020180030','001','tes','2020-11-18',3,3800,3000,0,2850,14,3,'2020-11-18'),(31,'N2020190031','002','tes2','2020-11-19',1,6300,5000,0,1800,13,3,'2020-11-19'),(32,'N2020190032','003','tes3','2020-11-19',2,2000,1000,0,2000,13,4,'2020-11-19'),(33,'N2020190033','003','tes3','2020-11-19',1,2000,1000,0,1000,13,4,'2020-11-19');
/*!40000 ALTER TABLE `barang_keluar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_keluar_tmp`
--

DROP TABLE IF EXISTS `barang_keluar_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_keluar_tmp` (
  `id_barang_keluar` int(11) NOT NULL AUTO_INCREMENT,
  `nota` varchar(128) DEFAULT NULL,
  `kode_barang` varchar(128) DEFAULT NULL,
  `nama_barang` varchar(128) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `keuntungan` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL,
  PRIMARY KEY (`id_barang_keluar`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_keluar_tmp`
--

LOCK TABLES `barang_keluar_tmp` WRITE;
/*!40000 ALTER TABLE `barang_keluar_tmp` DISABLE KEYS */;
INSERT INTO `barang_keluar_tmp` VALUES (5,'N2020210004','004','Bensin','2020-10-21',10,15000,10000,0,25000,13,4,'2020-10-21'),(9,'N2020220009','003','tes3','2020-10-22',2,3000,1000,0,4000,13,4,'2020-10-21'),(11,'N2020220009','003','tes3','2020-10-22',1,3000,1000,1,1970,13,4,'2020-10-22');
/*!40000 ALTER TABLE `barang_keluar_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_masuk`
--

DROP TABLE IF EXISTS `barang_masuk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_masuk` (
  `id_barang_masuk` int(11) NOT NULL AUTO_INCREMENT,
  `nota` varchar(128) DEFAULT NULL,
  `kode_barang` varchar(128) DEFAULT NULL,
  `nama_barang` varchar(128) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `diskon` int(11) NOT NULL,
  `keuntungan` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL,
  PRIMARY KEY (`id_barang_masuk`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_masuk`
--

LOCK TABLES `barang_masuk` WRITE;
/*!40000 ALTER TABLE `barang_masuk` DISABLE KEYS */;
INSERT INTO `barang_masuk` VALUES (1,'001','003','testes','2020-11-07',1,2000,1000,0,1000,12,3,'2020-11-07'),(2,'001','001','tes','2020-10-29',5,4000,3000,0,1000,12,2,'2020-10-29'),(3,'002','002','tes2','2020-10-29',3,7000,5000,0,2000,12,2,'2020-10-29'),(4,'003','003','tes4','2020-10-29',10,7000,5000,0,2000,12,2,'2020-10-29'),(6,'B0001','005','AleAle','2020-11-12',5,1500,1000,0,500,13,2,'2020-11-12');
/*!40000 ALTER TABLE `barang_masuk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_masuk_tmp`
--

DROP TABLE IF EXISTS `barang_masuk_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_masuk_tmp` (
  `id_barang_masuk` int(11) NOT NULL AUTO_INCREMENT,
  `nota` varchar(128) DEFAULT NULL,
  `kode_barang` varchar(128) DEFAULT NULL,
  `nama_barang` varchar(128) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `keuntungan` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL,
  PRIMARY KEY (`id_barang_masuk`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_masuk_tmp`
--

LOCK TABLES `barang_masuk_tmp` WRITE;
/*!40000 ALTER TABLE `barang_masuk_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `barang_masuk_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jurnal_umum`
--

DROP TABLE IF EXISTS `jurnal_umum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jurnal_umum` (
  `id_jurnal_umum` int(11) NOT NULL AUTO_INCREMENT,
  `kode_akun` int(5) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `debet` int(11) DEFAULT 0,
  `kredit` int(11) DEFAULT 0,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_jurnal_umum`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jurnal_umum`
--

LOCK TABLES `jurnal_umum` WRITE;
/*!40000 ALTER TABLE `jurnal_umum` DISABLE KEYS */;
INSERT INTO `jurnal_umum` VALUES (1,1111,'yagitu deh',500000,NULL,'2020-11-08 15:37:31'),(2,2102,'yagitu deh',NULL,200000,'2020-11-08 15:37:31'),(3,1111,'gitudeh',10000000,0,'2020-11-08 16:00:09'),(4,4101,'gitudeh',0,10000000,'2020-11-08 16:00:09'),(5,1121,'gini',2000000,0,'2020-11-08 16:00:27');
/*!40000 ALTER TABLE `jurnal_umum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kas_keluar`
--

DROP TABLE IF EXISTS `kas_keluar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kas_keluar` (
  `id_kas_keluar` int(11) NOT NULL AUTO_INCREMENT,
  `kode_akun` int(5) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `debet` int(11) DEFAULT 0,
  `kredit` int(11) DEFAULT 0,
  `tanggal` date NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_kas_keluar`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kas_keluar`
--

LOCK TABLES `kas_keluar` WRITE;
/*!40000 ALTER TABLE `kas_keluar` DISABLE KEYS */;
INSERT INTO `kas_keluar` VALUES (2,2101,'ngutang kopi satu renceng',0,1500000,'2020-11-08'),(3,3001,'ngabisin modal',0,100000,'2020-11-08'),(4,2102,'ga ada duit buat bayar gaji',0,50000,'2020-11-08');
/*!40000 ALTER TABLE `kas_keluar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kas_masuk`
--

DROP TABLE IF EXISTS `kas_masuk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kas_masuk` (
  `id_kas_masuk` int(11) NOT NULL AUTO_INCREMENT,
  `kode_akun` int(5) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `debet` int(11) DEFAULT 0,
  `kredit` int(11) DEFAULT 0,
  `tanggal` date NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_kas_masuk`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kas_masuk`
--

LOCK TABLES `kas_masuk` WRITE;
/*!40000 ALTER TABLE `kas_masuk` DISABLE KEYS */;
INSERT INTO `kas_masuk` VALUES (1,1111,'jualan keliling',300000,0,'2020-11-08'),(2,1112,'Jualan Aja',150000,0,'2020-11-08'),(3,1112,'Dapet pinjeman nich',30000000,0,'2020-11-08'),(4,1112,'Dapet duit gratis',20000,0,'2020-11-08'),(6,1121,'deposito',100000,0,'2020-11-08');
/*!40000 ALTER TABLE `kas_masuk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(128) DEFAULT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori`
--

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` VALUES (12,'Minuman','2020-10-27 09:12:45'),(13,'Snack','2020-10-27 09:12:45'),(14,'ATK','2020-10-27 09:12:45'),(20,'souvenir','2020-10-27 09:12:45'),(21,'Sembako','2020-10-27 09:12:45');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori_akun`
--

DROP TABLE IF EXISTS `kategori_akun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori_akun` (
  `kode_akun` int(5) NOT NULL,
  `nama_akun` varchar(200) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `kelompok` varchar(5) NOT NULL,
  PRIMARY KEY (`kode_akun`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori_akun`
--

LOCK TABLES `kategori_akun` WRITE;
/*!40000 ALTER TABLE `kategori_akun` DISABLE KEYS */;
INSERT INTO `kategori_akun` VALUES (1111,'Kas','DEBET','NRC'),(1112,'Bank','DEBET','NRC'),(1121,'Deposito di BPR','DEBET','NRC'),(1131,'Piutang Dagang','DEBET','NRC'),(1132,'Cadangan Kerugian Piutang','DEBET','NRC'),(1133,'Piutang Karyawan','DEBET','NRC'),(1134,'Piutang Lain-lain','DEBET','NRC'),(1141,'Persediaan Barang','DEBET','NRC'),(1151,'Persekot Biaya Perjalanan','DEBET','NRC'),(1152,'Persekot Biaya Asuransi','DEBET','NRC'),(1153,'Sewa Dibayar Dimuka','DEBET','NRC'),(1201,'Investasi Jangka Panjang','DEBET','NRC'),(1311,'Tanah','DEBET','NRC'),(1312,'Peralatan Toko','DEBET','NRC'),(1313,'Kendaraan','DEBET','NRC'),(1322,'Ak. Penyusutan Peralatan Toko','DEBET','NRC'),(1323,'Ak. Penyusutan Kendaraan','DEBET','NRC'),(2101,'Utang Dagang','KREDIT','NRC'),(2102,'Utang Gaji','KREDIT','NRC'),(2103,'Utang Jangka Pendek Bank','KREDIT','NRC'),(2104,'Utang Jangka Pendek Lain-lain','KREDIT','NRC'),(2201,'Utang Jangka Panjang dari Bank','KREDIT','NRC'),(3001,'Modal Pemilik','KREDIT','NRC'),(3002,'Prive Pemilik','DEBET','NRC'),(3003,'Laba Periode Berjalan','KREDIT','NRC'),(4101,'Penjualan','KREDIT','LR '),(4201,'Retur penjualan','DEBET','LR'),(4202,'Potongan Penjualan','DEBET','LR'),(4203,'Retur Pembelian','KREDIT','LR'),(4204,'Potongan Pembelian','KREDIT','LR'),(5101,'Harga Pokok Penjualan ','DEBET','LR'),(5211,'Biaya Gaji ','DEBET','LR '),(5212,'Biaya Pemasaran','DEBET','LR '),(5213,'Biaya Penyusutan Peralatan Toko','DEBET','LR '),(5214,'Biaya Pengiriman','DEBET','LR'),(5215,'Biaya Penjualan Rupa-rupa','DEBET','LR '),(5216,'Biaya Sewa','DEBET','LR '),(5217,'Biaya Penyusutan Kendaraan','DEBET','LR '),(5218,'Biaya Perjalanan','DEBET','LR '),(5219,'Biaya Asuransi','DEBET','LR '),(5220,'Biaya Perlengkapan Toko','DEBET','LR '),(5221,'Biaya Administrasi Rupa-rupa','DEBET','  LR '),(5222,'Biaya Listrik, Telpon & Air','DEBET','LR'),(5223,'Biaya Lain-lain','DEBET','  LR '),(6101,'Pendapatan Lain-lain','KREDIT','  LR ');
/*!40000 ALTER TABLE `kategori_akun` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_stok`
--

DROP TABLE IF EXISTS `log_stok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_stok` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(128) DEFAULT NULL,
  `id_barang_keluar` int(11) DEFAULT NULL,
  `id_barang_masuk` int(11) DEFAULT NULL,
  `id_retur` int(11) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `stok` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_stok`
--

LOCK TABLES `log_stok` WRITE;
/*!40000 ALTER TABLE `log_stok` DISABLE KEYS */;
INSERT INTO `log_stok` VALUES (1,'001',NULL,2,NULL,'2020-10-29',5,'Barang Masuk'),(2,'002',NULL,3,NULL,'2020-10-29',3,'Barang Masuk'),(3,'003',NULL,4,NULL,'2020-10-29',10,'Barang Masuk'),(4,'001',24,NULL,NULL,'2020-10-29',3,'Barang Keluar'),(5,'002',25,NULL,NULL,'2020-10-29',2,'Barang Keluar '),(6,'003',26,NULL,NULL,'2020-10-29',1,'Barang Keluar'),(8,'001',NULL,2,1,'2020-10-29',1,'Retur Barang Masuk'),(9,'001',NULL,2,2,'2020-10-29',2,'Retur Barang Masuk'),(10,'001',24,NULL,3,'2020-10-29',1,'Retur Barang Keluar'),(11,'001',25,NULL,4,'2020-10-29',1,'Retur Barang Keluar'),(12,'001',12,NULL,NULL,'2020-11-07',1,'Barang Keluar'),(13,'003',NULL,1,NULL,'2020-11-07',1,'Barang Masuk'),(14,'001',13,NULL,NULL,'2020-11-08',1,'Barang Keluar'),(15,'005',NULL,5,NULL,'2020-11-12',5,'Barang Masuk'),(16,'005',NULL,6,NULL,'2020-11-12',5,'Barang Masuk'),(17,'002',15,NULL,NULL,'2020-11-12',3,'Barang Keluar'),(18,'003',16,NULL,NULL,'2020-11-12',1,'Barang Keluar'),(19,'001',18,NULL,NULL,'2020-11-12',1,'Barang Keluar'),(20,'005',19,NULL,NULL,'2020-11-12',1,'Barang Keluar'),(21,'005',20,NULL,NULL,'2020-11-12',1,'Barang Keluar'),(22,'005',21,NULL,NULL,'2020-11-12',2,'Barang Keluar'),(23,'005',27,NULL,NULL,'2020-11-12',1,'Barang Keluar'),(24,'001',28,NULL,NULL,'2020-11-18',10,'Barang Keluar'),(25,'001',29,NULL,NULL,'2020-11-18',2,'Barang Keluar'),(26,'001',30,NULL,NULL,'2020-11-18',3,'Barang Keluar'),(27,'002',31,NULL,NULL,'2020-11-19',1,'Barang Keluar'),(28,'003',32,NULL,NULL,'2020-11-19',2,'Barang Keluar'),(29,'003',33,NULL,NULL,'2020-11-19',1,'Barang Keluar');
/*!40000 ALTER TABLE `log_stok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modal`
--

DROP TABLE IF EXISTS `modal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modal` (
  `id_modal` int(11) NOT NULL AUTO_INCREMENT,
  `modal` int(11) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_modal`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modal`
--

LOCK TABLES `modal` WRITE;
/*!40000 ALTER TABLE `modal` DISABLE KEYS */;
INSERT INTO `modal` VALUES (67,10000,'2020-09-10 17:27:12','2020-09-11 17:00:00'),(68,2147483647,'2020-09-12 00:19:57','2020-09-11 17:00:00'),(69,1000000,'2020-09-15 23:25:17','2020-09-14 17:00:00'),(71,50000,'2020-09-17 04:51:28',NULL),(72,10000,'2020-09-18 22:53:38',NULL),(73,10000,'2020-10-10 08:39:16',NULL),(74,1000000,'2020-10-15 08:41:56',NULL),(75,1000000,'2020-10-17 07:11:29',NULL),(76,100000,'2020-10-19 11:06:36',NULL),(77,100000,'2020-10-20 01:01:18',NULL),(78,100000,'2020-10-20 17:34:30',NULL),(79,100000,'2020-10-22 09:22:36',NULL),(80,100000,'2020-10-24 07:18:24',NULL),(81,1000000,'2020-10-25 07:26:22',NULL),(82,100000,'2020-10-27 02:10:52',NULL),(83,1000000,'2020-10-28 04:45:03',NULL),(84,100000,'2020-10-29 02:30:49',NULL),(85,100000,'2020-10-30 06:31:29',NULL),(86,200000,'2020-10-31 04:03:44',NULL),(88,100000,'2020-11-07 15:06:58',NULL),(89,100000,'2020-11-08 03:18:14',NULL),(90,100000,'2020-11-09 11:29:48',NULL),(91,1000000,'2020-11-10 03:55:25',NULL),(93,100000,'2020-11-10 23:58:35',NULL),(94,100000,'2020-11-11 22:08:14',NULL),(95,0,'2020-11-13 02:55:44',NULL),(96,0,'2020-11-15 10:04:36',NULL),(97,0,'2020-11-16 01:54:51',NULL),(98,100000,'2020-11-18 01:03:55',NULL),(99,100000,'2020-11-19 06:22:34',NULL);
/*!40000 ALTER TABLE `modal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retur`
--

DROP TABLE IF EXISTS `retur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retur` (
  `id_retur` int(11) NOT NULL AUTO_INCREMENT,
  `nota` varchar(128) NOT NULL,
  `kode_barang` varchar(128) NOT NULL,
  `nama_barang` varchar(128) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `diskon` int(11) DEFAULT NULL,
  `sub_total` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `nama_supplier` varchar(128) NOT NULL,
  `tipe_retur` varchar(128) NOT NULL,
  `create_at` datetime NOT NULL,
  PRIMARY KEY (`id_retur`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retur`
--

LOCK TABLES `retur` WRITE;
/*!40000 ALTER TABLE `retur` DISABLE KEYS */;
INSERT INTO `retur` VALUES (1,'001','001','tes',1,4000,0,4000,'2020-10-29','Budi','pembelian','2020-10-29 00:00:00'),(2,'001','001','tes',2,4000,0,8000,'2020-10-29','Budi','pembelian','2020-10-29 00:00:00'),(3,'N2020290001','001','tes',1,4000,10,3600,'2020-10-29','Bambang','penjualan','2020-10-29 00:00:00'),(4,'N2020290001','001','tes',1,4000,10,3600,'2020-10-29','Bambang','penjualan','2020-10-29 00:00:00');
/*!40000 ALTER TABLE `retur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `satuan`
--

DROP TABLE IF EXISTS `satuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `satuan` (
  `id_satuan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_satuan` varchar(128) DEFAULT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_satuan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `satuan`
--

LOCK TABLES `satuan` WRITE;
/*!40000 ALTER TABLE `satuan` DISABLE KEYS */;
INSERT INTO `satuan` VALUES (2,'Pack','2020-10-27 09:13:05'),(3,'Biji','2020-10-27 09:13:05'),(4,'Liter','2020-10-27 09:13:05');
/*!40000 ALTER TABLE `satuan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surat`
--

DROP TABLE IF EXISTS `surat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surat` (
  `id_surat` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_surat` varchar(128) DEFAULT NULL,
  `instansi` varchar(123) DEFAULT NULL,
  `topik` varchar(128) DEFAULT NULL,
  `type` enum('masuk','keluar') DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `scan` varchar(128) DEFAULT 'default.jpg',
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_surat`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surat`
--

LOCK TABLES `surat` WRITE;
/*!40000 ALTER TABLE `surat` DISABLE KEYS */;
INSERT INTO `surat` VALUES (26,'asdass','sdasd','asdasdasd','masuk','2020-10-30','3.JPG','2020-10-28 00:00:00'),(28,'123123123ssss','1231231','123123123','keluar','2020-10-31','111sss.png','2020-10-29 00:00:00'),(30,'34354','345345','345345','keluar','2020-10-29','20191021_163939.jpg','2020-10-31 00:00:00'),(31,'1234','gitudeh','gininih','masuk','2020-10-31','3.JPG','2020-10-31 06:00:00');
/*!40000 ALTER TABLE `surat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kd_user` varchar(128) DEFAULT NULL,
  `nama_user` varchar(128) DEFAULT NULL,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `alamat` varchar(128) DEFAULT NULL,
  `group_user` enum('Administrator','User') DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=293 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (56,'SKPM-2020020001','admin','admin','$2y$10$mqXSKZbnsTvOPoJeQKGz.O7f7F4DPCTsypGXLwfBmygsZTq5lIvjy','KOPMA-UMM','Administrator','2020-05-02 07:54:04',NULL),(79,'SKPM-2020020010','Ida firdiana','admin','$2y$10$4g9TnpBiGXVQbSn4tNsHgu6eHjwcVaRxNElYa85yFvAaJBJST4sf2','Jl tirto utomo no 36','Administrator','2020-05-02 12:07:36',NULL),(284,'SKPM-2020090011','user','user','$2y$10$psjXv.Jz/WJ5.D58Q4CZ0uRS4y5CyIHKohpwIxPQTx8d1olP/h3vm','adsasasd','User','2020-05-09 07:20:18',NULL),(289,'SKPM-2020130016','tes','tes','$2y$10$qkCtdqwLfuY9AlabRqb37e3HvHM2Zq12OcWy9rfRr39NoHPA.tsqS','tes','Administrator','2020-06-13 10:36:08',NULL),(292,'SKPM-2020040017','asdkaksd','askdkasd','$2y$10$5qHBXfPMpk.tJRChjRjXIuBfD4OeJMyZKjl78a6O8.67vLne678jW','aksdkaskd','Administrator','2020-07-04 11:45:36',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'simnew'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-22  8:24:16
