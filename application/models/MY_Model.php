<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get($table = null, $id = null, $atribut = null, $dbjoin1 = null, $aksijoin1 = null, $dbjoin2 = null, $aksijoin2 = null, $order = null)
	{
		# code...
		if ($id != null) {
			# code...
			$this->db->SELECT('*');
			$this->db->FROM($table);
			if ($dbjoin1 != null) {
				# code...
				$this->db->JOIN($dbjoin1, $aksijoin1);
			}
			if ($dbjoin2) {
				# code..
				$this->db->JOIN($dbjoin2, $aksijoin2);
			}
			$this->db->WHERE($atribut, $id);
		} else {
			$this->db->SELECT('*');
			$this->db->FROM($table);
		}
		$orders = '';
		if ($order != null) {
			# code...
			$orders = $order . 'create_at';
		} else {
			$orders = 'create_at';
		}
		// die($orders);
		$this->db->order_by($orders, 'desc');
		$query = $this->db->get();

		return $query;
	}

	public function get_MultipleWhere($table = null, $where = null,  $dbjoin1 = null, $aksijoin1 = null, $dbjoin2 = null, $aksijoin2 = null)
	{
		# code...
		# code...
		if ($where != null) {
			# code...
			$this->db->SELECT('*');
			$this->db->FROM($table);
			if ($dbjoin1 != null) {
				# code...
				$this->db->JOIN($dbjoin1, $aksijoin1);
			}
			if ($dbjoin2) {
				# code..
				$this->db->JOIN($dbjoin2, $aksijoin2);
			}
			$this->db->WHERE($where);
		} else {
			$this->db->SELECT('*');
			$this->db->FROM($table);
		}
		$this->db->order_by('create_at', 'desc');
		$query = $this->db->get();

		return $query;
	}

	public function get_select($db, $select = '*', $where = null)
	{
		# code...
		$this->db->SELECT($select);
		$this->db->FROM($db);
		$this->db->WHERE($where);

		$query = $this->db->get();

		return $query;
	}

	public function insert($db, $object)
	{
		# code...
		$this->db->insert($db, $object);
		return $this->db->affected_rows();
	}

	public function update($db, $id, $object, $data)
	{
		# code...
		$this->db->update($db, $data, [$id => $object]);
		return $this->db->affected_rows();
	}

	public function delete($db, $object, $id)
	{
		$this->db->WHERE($object, $id)
			->DELETE($db);
		return $this->db->affected_rows();
	}

	public function count_Surat($type)
	{
		# code...
		$sql = "SELECT COUNT(*) FROM `surat` WHERE type = $type";

		$query = $this->db->query($sql);

		return $query->row_array();
	}



	public function count_all($db)
	{
		# code...
		$this->db->SELECT()
			->FROM($db);
		$query = $this->db->count_all_results();

		return $query;
	}

	public function count_stok($db, $aksi)
	{
		$this->db->select($aksi)
			->from($db);

		$query = $this->db->get()->row_array();

		return $query;
	}

	public function sum($db, $aksi, $where = null)
	{
		$this->db->select("(SELECT SUM(" . $aksi . ") FROM " . $db . " WHERE " . $where . ") as total", FALSE);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function sum_month($db, $aksi, $where = null)
	{
		$query = $this->db->query('select year(tanggal) as year, month(tanggal) as month, COALESCE(sum(' . $aksi . '), 0) as total from barang_masuk where ' . $where . ' group by year(tanggal), month(tanggal)');

		return $query;
	}

	public function sum_week($db, $aksi, $where = null)
	{
		$query = $this->db->query('select date(tanggal) as day, COALESCE(sum(' . $aksi . '), 0) as total from barang_masuk where ' . $where . ' group by date(tanggal)');

		return $query;
	}

	public function getModalByNow()
	{
		# code...
		$this->db->SELECT('*')
			->FROM('modal')
			->WHERE('create_at >= CURRENT_DATE()');
		$query = $this->db->get()->row();
		// $sql = "SELECT * FROM `modal` WHERE `create_at` >= CURRENT_DATE()";
		// $query = $this->db->query($sql);
		return $query;
	}

	public function getMultipleModal($db, $tanggal, $tanggal2)
	{
		$this->db->SELECT('*')->FROM($db);

		if ($tanggal == $tanggal2) {
			$this->db->WHERE('create_at BETWEEN "' . date('Y-m-d', strtotime($tanggal)) . '" AND "' . date('Y-m-d', strtotime('+1 day', strtotime($tanggal))) . '"');
		} else {
			$this->db->WHERE('create_at BETWEEN "' .  date('Y-m-d', strtotime($tanggal)) . '" AND "' . date('Y-m-d', strtotime('+1 day', strtotime($tanggal2))) . '"');
		}

		$this->db->order_by('create_at', 'ASC');

		$query = $this->db->get();
		return $query;
	}

	public function getLaporan($db, $tanggal, $tanggal2, $where = null)
	{
		$this->db->SELECT('*')
			->FROM($db)
			->WHERE('tanggal BETWEEN "' . $tanggal . '" AND "' . $tanggal2 . '"');
		if ($where != null || $where != '') {
			$this->db->WHERE($where);
		}
		$this->db->order_by('tanggal', 'ASC');

		$query = $this->db->get();
		return $query;
	}

	public function getLaporanJoin($db, $select = null, $where = null, $order_by = null, $dbjoin1 = null, $aksijoin1 = null, $dbjoin2 = null, $aksijoin2 = null, $dbjoin3 = null, $aksijoin3 = null)
	{
		if ($select != null) {
			$this->db->SELECT($select);
		} else {
			$this->db->SELECT('*');
		}
		$this->db->FROM($db);

		if ($dbjoin1 != null) {
			# code...
			$this->db->JOIN($dbjoin1, $aksijoin1);
		}
		if ($dbjoin2 != null) {
			# code..
			$this->db->JOIN($dbjoin2, $aksijoin2);
		}
		if ($dbjoin2 != null) {
			# code..
			$this->db->JOIN($dbjoin3, $aksijoin3);
		}
		if ($where != null) {
			$this->db->WHERE($where);
		}
		if ($order_by != null) {
			$this->db->order_by($order_by);
		}

		$query = $this->db->get();
		return $query;
	}

	public function get_join($db, $where = null, $order_by = null, $dbjoin1 = null, $aksijoin1 = null, $dbjoin2 = null, $aksijoin2 = null, $dbjoin3 = null, $aksijoin3 = null)
	{
		$this->db->SELECT('*')
			->FROM($db);

		if ($dbjoin1 != null) {
			# code...
			$this->db->JOIN($dbjoin1, $aksijoin1);
		}
		if ($dbjoin2 != null) {
			# code..
			$this->db->JOIN($dbjoin2, $aksijoin2);
		}
		if ($dbjoin2 != null) {
			# code..
			$this->db->JOIN($dbjoin3, $aksijoin3);
		}
		if ($where != null) {
			$this->db->WHERE($where);
		}
		if ($order_by != null) {
			$this->db->order_by($order_by);
		}

		$query = $this->db->get();
		return $query;
	}

	public function insert_id()
	{
		$id = $this->db->insert_id();
		return $id;
	}
}

/* End of file MY_Model.php */
/* Location: ./application/models/MY_Model.php */
