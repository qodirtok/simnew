<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'core/Mypdf.php');
class Tpenjualan extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		MY_Controller::is_logged_in();
	}

	public function class_data()
	{
		# code...
		$data['folder'] = 'tpenjualan';
		$data['file']	= 'view.php';
		$data['listData'] = 'data-list" data-link="' . base_url() . "tpenjualan/tampilListPenjualan" . '';
		$data['listSearch'] = 'data-list-search" data-link="' . base_url() . "tpenjualan/getBarang" . '';
		$data['buttonSave'] = '<button class="btn generatecode btn-info btn-save-jual" type="submit" data-link="' . base_url() . "tpenjualan/store" . '" data-method="tambah" value="Simpan">Simpan</button> ';

		return $data;
	}

	public function getModal()
	{
		# code...
		$get = $this->mod->getModalByNow();

		if ($get == null || $get == '') {
			# code...
			$data['modal'] = 'Rp.' . number_format(0);
		} else {
			# code...
			$data['modal'] = 'Rp.' . number_format($get->modal);
		}

		// var_dump(json_encode($get));
		// die();
		print json_encode($data);
	}

	public function getStruk($nota)
	{
	}

	public function index()
	{
		$data = self::class_data();

		$data['page'] 	= 'tpenjualan';
		$data['title'] = 'Penjualan';
		$data['subtitle'] = '';

		$data['sub_total'] = $this->get_subtotal();

		$nota = $this->createcode->Gcode('barang_keluar', 'id_barang_keluar');
		$data['nota'] = $nota;

		$check = $this->mod->getModalByNow();

		// die();
		if ($check != null || $check != '') {
			# code...
			$data['modals'] = $check;
			$this->template->layouts($data);
		} else {
			# code...
			$data['modals'] = $check;
			$this->session->set_flashdata('modalMessage', 'input modal');
			$this->template->layouts($data);
		}
	}

	public function getBarang()
	{
		$kobar = $this->input->post('id', true);
		$data['getSearch'] = $this->mod->get_MultipleWhere('barang', "kode_barang = '" . $kobar . "' or nama_barang = '" . $kobar . "'")->result_array();

		$this->load->view('tpenjualan/detail-barang', $data);
	}

	public function tampilListPenjualan()
	{
		$data['tmp_penjualan'] = $this->mod->get('barang_keluar_tmp', date('Y-m-d'), 'create_at')->result_array();
		$this->load->view('tpenjualan/tampil', $data);
	}

	public function get_subtotal()
	{
		$data['sub_total'] = 0;
		$tmp_penjualan = $this->mod->get_MultipleWhere('barang_keluar_tmp', "create_at = '" . date('Y-m-d') . "'")->result_array();
		$dataCount = count($tmp_penjualan);
		for ($i = 0; $i < $dataCount; $i++) {
			$data['sub_total'] += intval(($tmp_penjualan[$i]['harga_jual'] - ($tmp_penjualan[$i]['harga_jual'] * ($tmp_penjualan[$i]['diskon'] / 100))) * $tmp_penjualan[$i]['stok']);
		}

		return $data['sub_total'];
	}

	public function ShowPopUpModals()
	{
		# code...
		$data['title']	= 'Detail barang';
		$data['body'] = '
		<ul class="nav nav-tabs" id="myTab" role="tablist">

			<li class="nav-item">
				<a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Update</a>
			</li>

		</ul>
		<div class="tab-content" id="myTabContent">

			<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
			<form class="form-update">
			<input type="hidden" name="id" value="">
						<div class="form-group">
							<label for="Kode_User">Kode User</label>
							<input type="text" class=" form-control change" value="" name="name" >
						</div>


						<div class="form-group text-right">

						</div>
					</form>
			</div>

		</div>

	';
		$data['modal'] = $this->modal->formModal($data['title'], $data['body'], $data['buttonSave'], $data['buttonRestart']);

		print json_encode($data);
	}

	public function addTocart()
	{
		$kode_barang = $this->input->post('kodeBarang');
		$qty = $this->input->post('qty');
		$diskon = $this->input->post('diskon') / 100;
		$data = [];

		$getBarang = $this->mod->get_MultipleWhere('barang', "kode_barang = '" . $kode_barang . "' or nama_barang = '" . $kode_barang . "'")->row_array();

		if ($getBarang != null) {
			$tmp_penjualan = $this->mod->get_MultipleWhere('barang_keluar_tmp', "(kode_barang = '" . $kode_barang . "' or nama_barang = '" . $kode_barang . "') and create_at = '" . date('Y-m-d') . "'")->row_array();

			if (isset($tmp_penjualan)) {
				if ($getBarang['kode_barang'] == $tmp_penjualan['kode_barang']) {
					$sisaStok = ($getBarang['stok'] - $tmp_penjualan['stok']) - $qty;
					if ($sisaStok < 0) {
						$data['msg'] = 'gagal';
						$data['msgvalue'] = 'Jumlah barang melebihi stok yang tersedia';
					} else {
						$tmp_penjualan['stok'] = $tmp_penjualan['stok'] + $qty;
						$updateTmp = $this->mod->update('barang_keluar_tmp', 'id_barang_keluar', $tmp_penjualan['id_barang_keluar'], $tmp_penjualan);
						$data['msg'] = 'berhasil';
						$data['msgvalue'] = 'Barang berhasil diperbaharui';
					}
				}
			} else if ($qty < 1) {
				$data['msg'] = 'gagal';
				$data['msgvalue'] = 'Anda belum input Jumlah Barang';
			} else if ($getBarang['stok'] < $qty) {
				$data['msg'] = 'gagal';
				$data['msgvalue'] = 'Jumlah barang melebihi stok yang tersedia';
			} else {
				// hitung keuntungan
				$keuntungan = (($getBarang['harga_jual'] - ($getBarang['harga_jual'] * $diskon)) - $getBarang['harga_beli']) * $qty;

				// data cart
				$insertData = [
					'nota' => $this->input->post('nota'),
					'kode_barang' => $getBarang['kode_barang'],
					'nama_barang' => $getBarang['nama_barang'],
					'tanggal' => date('Y-m-d'),
					'stok' => $qty,
					'harga_jual' => $getBarang['harga_jual'],
					'harga_beli' => $getBarang['harga_beli'],
					'diskon' => $this->input->post('diskon'),
					'keuntungan' => $keuntungan,
					'id_kategori' => $getBarang['id_kategori'],
					'id_satuan' => $getBarang['id_satuan'],
					'create_at' => date('Y-m-d')
				];

				$insert = $this->mod->insert('barang_keluar_tmp', $insertData);
				$data['msg'] = 'berhasil';
			}
		} else {
			$data['msg'] = 'gagal';
			$data['msgvalue'] = 'Barang tidak tersedia';
		}

		$data['sub_total'] = $this->get_subtotal();

		echo json_encode($data);
	}

	public function store()
	{
		// input diskon cart
		$bayar = $this->input->post('bayar');
		$kembalian = $this->input->post('kembalian');
		$diskon = $this->input->post('diskon');

		if ($bayar == null || $bayar == 0) {
			$data['msg'] = 'gagal';
			$data['msgvalue'] = 'Masukan jumlah pembayaran';
		} else if ($kembalian < 0) {
			$data['msg'] = 'gagal';
			$data['msgvalue'] = 'Pembayaran kurang';
		} else {
			$insertCount = 0;
			$data = $this->mod->get_MultipleWhere('barang_keluar_tmp', "create_at = '" . date('Y-m-d') . "'")->result_array();
			$dataCount = count($data);

			for ($i = 0; $i < $dataCount; $i++) {
				$getBarang = $this->mod->get_MultipleWhere('barang', "kode_barang = '" . $data[$i]['kode_barang'] . "'")->result_array();

				$getBarang[0]['stok'] = intval($getBarang[0]['stok']) - intval($data[$i]['stok']);
				$updateBarang = $this->mod->update('barang', 'kode_barang', $getBarang[0]['kode_barang'], $getBarang[0]);

				$data[$i]['harga_jual'] = $data[$i]['harga_jual'] - ($data[$i]['harga_jual'] * ($diskon / 100));
				$data[$i]['keuntungan'] = $data[$i]['keuntungan'] - ($data[$i]['keuntungan'] * ($diskon / 100));

				$id_barang_keluar = $data[$i]['id_barang_keluar'];
				unset($data[$i]['id_barang_keluar']);
				$insert = $this->mod->insert('barang_keluar', $data[$i]);
				$last_id = $this->mod->insert_id();

				$insertLog = [
					'kode_barang' => $data[$i]['kode_barang'],
					'id_barang_keluar' => $last_id,
					'tanggal' => $data[$i]['tanggal'],
					'stok' => $data[$i]['stok'],
					'keterangan' => 'Barang Keluar',
				];

				$insertLog = $this->mod->insert('log_stok', $insertLog);

				$insertCount++;
			}

			if ($insertCount == $dataCount) {
				# code...
				for ($j = 0; $j < $dataCount; $j++) {
					$delete = $this->mod->delete('barang_keluar_tmp', 'id_barang_keluar', $id_barang_keluar);
				}
				$data['msg'] = 'berhasil';
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
		}

		$data['sub_total'] = $this->get_subtotal();
		$nota = $this->createcode->Gcode('barang_keluar', 'id_barang_keluar');
		$data['nota'] = $nota;

		echo json_encode($data);
	}

	public function destroy($id)
	{
		# code...
		if ($id != '' || $id != null) {
			# code...
			$delete = $this->mod->delete('barang_keluar_tmp', 'id_barang_keluar', $id);
			if ($delete > 0) {
				# code...
				$data['msg'] = 'berhasil';
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
		}

		$data['sub_total'] = $this->get_subtotal();

		print json_encode($data);
	}

	public function struk($nota)
	{
		$pdf = new \MYPDF();

		$pdf->AddPage();
		$pdf->setY(25);
		$pdf->SetAutoPageBreak(true, 30);
		$pdf->SetMargins('5', '30', '5', false);
		$pdf->SetTitle('Struk Transaksi');

		$penjualan = $this->mod->get_select('barang_keluar', '*', 'nota = "' . $nota . '"')->result_array();

		$pdf->SetFont('', 'B', 6);
		$pdf->Cell(10, 8, "Nota " . $nota, 0, 1, 'C');
		$pdf->SetFont('', 'B', 6);
		$pdf->Cell(10, 8, "Nama", 0, 0, 'C');
		$pdf->Cell(10, 8, "Harga", 0, 0, 'C');
		$pdf->Cell(10, 8, "Diskon", 0, 0, 'C');
		$pdf->Cell(10, 8, "Qty", 0, 0, 'C');
		$pdf->Cell(20, 8, "Sub Total", 0, 1, 'C');
		$pdf->SetFont('', '', 6);

		$sub_total = 0;
		$saldo = 0;
		foreach ($penjualan as $k => $value) {
			$sub_total += $value['harga_jual'] - ($value['harga_jual'] * ($value['diskon'] / 100)) * $value['stok'];
			$saldo += $sub_total;
			$this->addRow($pdf, $value, $sub_total);
		}

		$pdf->SetFont('', 'B', 6);
		$pdf->Cell(40, 8, 'Total', 0, 0, 'C');
		$pdf->Cell(20, 8, number_format($saldo, 2, ',', '.'), 0, 1, 'C');

		$pdf->Cell(60, 8, 'Terima Kasih Telah Berbelanja,', 0, 1, 'C');
		$pdf->Cell(60, 8, 'Di Koperasi Universitas Muhammadiyah Malang :D', 0, 1, 'C');
		$tanggal = date('d-m-Y');
		$pdf->Output('Laporan Order - ' . $tanggal . '.pdf');
	}

	public function addRow($pdf, $value, $sub_total)
	{
		$pdf->Cell(10, 8, $value['nama_barang'], 0, 0, 'C');
		$pdf->Cell(10, 8, number_format($value['harga_jual'], 2, ',', '.'), 0, 0, 'C');
		$pdf->Cell(10, 8, $value['diskon'] . '%', 0, 0, 'C');
		$pdf->Cell(10, 8, $value['stok'], 0, 0, 'C');
		$pdf->Cell(20, 8, number_format($sub_total, 2, ',', '.'), 0, 1, 'C');
	}
}



/* End of file Tpenjualan.php */
