<?php

defined('BASEPATH') or exit('No direct script access allowed');

class surat extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		MY_Controller::is_logged_in();
		MY_Controller::checkAdmin();
	}


	public function class_data()
	{
		# code...
		$data['listData'] = 'data-list" data-link="' . base_url() . "surat/tampil" . '';
		$data['buttonSave'] = '<button class="btn generatecode btn-info btn-save" type="submit" data-link="' . base_url() . "surat/store" . '" data-method="tambah" value="Simpan">Simpan</button> ';
		$data['buttonUpdate'] = '<button class="btn generatecode btn-info btn-update" type="submit" data-link="' . base_url() . "surat/update" . '" data-aksi="update" value="Simpan">Simpan</button> ';
		$data['buttonRestart'] = '<input class="btn btn-success" type="reset" value="Reset">';
		$data['buttonLinktoSuratMasuk'] = '<button class="btn btn-primary klik-menu" data-link="' . base_url() . "surat/addSurat" . '"><i class="fas fa-plus"></i> Tambah surat masuk</button>';
		return $data;
	}

	public function index()
	{
		$data = $this->class_data();

		$data['folder'] = 'surat/masuk';
		$data['file']	= 'view';
		$data['page'] 	= 'surat';
		$data['title'] = 'Surat';
		$data['subtitle'] = '';

		$this->template->layouts($data);
	}

	public function tampil()
	{
		# code...
		$data['surat'] = $this->mod->get('surat', 'masuk', 'type')->result_array();
		$this->load->view('surat/masuk/list_surat', $data);

		// print json_encode($data);
	}

	public function addSurat()
	{
		# code...
		$data = $this->class_data();

		$data['folder'] = 'surat/masuk';
		$data['file']	= 'addSurat';
		$data['page'] 	= 'surat';
		$data['title'] = 'Tambah Surat';
		$data['subtitle'] = '';

		$this->template->layouts($data);
	}

	public function store()
	{
		# code...
		$this->form_validation->set_rules('nomor_surat', 'Nomor surat', 'trim|required');
		$this->form_validation->set_rules('instansi', 'Nama Instansi', 'trim|required');
		$this->form_validation->set_rules('tanggal', 'Tanggal surat', 'trim|required');
		$this->form_validation->set_rules('topik', 'Topik', 'trim|required');


		if ($this->form_validation->run() === TRUE) {
			# code...

			if (isset($_FILES['scan'])) {
				# code...
				if ($_FILES['scan']['name'] != '') {
					# code...
					$name = 'scan';
					$path = './assets/img/uploads/surat/masuk/';
					if (!is_dir($path)) {
						# code...
						mkdir($path, 0777, TRUE);
					}
					$data = [
						'nomor_surat' 	=> $this->input->post('nomor_surat', true),
						'instansi' 		=> $this->input->post('instansi', true),
						'topik' 		=> $this->input->post('topik', true),
						'tanggal' => $this->input->post('tanggal', true),
						'scan'			=> $this->do_upload($_FILES['scan'], $name, $path),
						'type' 			=> 'masuk',
					];
					// var_dump($_FILES['scan']);
					// die;
				}
			}
			$insert = $this->mod->insert('surat', $data);


			if ($insert > 0) {
				# code...
				$data['msg'] = 'berhasil';
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
		}
		print json_encode($data);
	}

	public function show($id)
	{
		# code...
		// die($id);
		$data = $this->class_data();

		$data['surat'] = $this->mod->get_MultipleWhere('surat', 'id_surat = ' . $id)->result_array();

		$data['folder'] = 'surat/masuk';
		$data['file']	= 'showSurat';
		$data['page'] 	= 'surat';
		$data['title'] = 'Edit Surat';
		$data['subtitle'] = '';

		$this->template->layouts($data);
	}

	public function update()
	{
		# code...
		if (isset($_FILES['scan'])) {
			# code...

			$name = 'scan';
			$path = './assets/img/uploads/surat/masuk/';

			if (!is_dir($path)) {
				# code...
				mkdir($path, 0777, TRUE);
			}
			$old_scan = $this->input->post('old_scan', true);
			$new_scan = $this->do_upload($_FILES['scan'], $name, $path);
			if ($_FILES['scan']['name'] != '') {
				$uploads['movetoserver'] = $_FILES['scan'];
				$uploads['db'] = $new_scan;
			} else {
				# code...
				$uploads['movetoserver'] = $_FILES['scan'];
				$uploads['db'] = $old_scan;
			}

			$this->image = $this->do_upload($uploads['movetoserver'], $name, $path);

			$data = [
				'id_surat'		=> $this->input->post('id', true),
				'nomor_surat' 	=> $this->input->post('nomor_surat', true),
				'instansi' 		=> $this->input->post('instansi', true),
				'topik' 		=> $this->input->post('topik', true),
				'tanggal' => $this->input->post('tanggal', true),
				'scan'			=> $uploads['db'],
				'type' 			=> 'masuk',
			];

			$insert = $this->mod->update('surat', 'id_surat', $data['id_surat'], $data);

			if ($insert > 0) {
				# code...
				$data['msg'] = 'berhasil';
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
			print json_encode($data);
		} else {
			# code...
			$data['msg'] = 'gagal';
			print json_encode($data);
		}
	}



	public function destroy($id)
	{
		# code...
		if ($id != '' || $id != null) {
			# code...
			$path_delete = 'assets/img/uploads/surat/masuk/';
			// $file = $this->mod->get('surat', 'id_surat', $id)->result_array();

			// echo "<pre>";
			// print_r($file);
			// echo "</pre>";
			// die();

			// $this->do_delete($id, $path_delete, $file['scan']);
			$delete = $this->mod->delete('surat', 'id_surat', $id);
			if ($delete > 0) {
				# code...
				$data['msg'] = 'berhasil';
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
		}
		print json_encode($data);
	}
}

/* End of file surat.php */
