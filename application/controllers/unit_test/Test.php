<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Test extends MY_Controller
{

  public function index()
  {
    $test = 1 + 1;

    $expected_result = 2;

    $test_name = 'Adds one plus one';

    // echo $this->unit->report();
    // echo $this->unit->result();
    print $this->unit->run($test, $expected_result, $test_name);
    // die('masuk');
  }
}

/* End of file Test.php */
