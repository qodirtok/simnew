<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller
{



	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		MY_Controller::is_logged_in();
		MY_Controller::checkAdmin();
	}


	public function class_data()
	{
		# code...
		$data['listData'] = 'data-list" data-link="' . base_url() . "user/tampil" . '';
		$data['buttonSave'] = '<button class="btn generatecode btn-info btn-save" type="submit" data-link="' . base_url() . "user/store" . '" data-method="tambah" value="Simpan">Simpan</button> ';
		$data['buttonRestart'] = '<input class="btn btn-success" type="reset" value="Reset">';
		$data['buttonLinktoFormAdd'] = '<button class="btn btn-primary klik-menu" data-link="' . base_url() . "user/add" . '"><i class="fas fa-plus"></i> Tambah user</button>';
		$data['buttonUpdate'] = '<button class="btn generatecode btn-info btn-update" type="submit" data-link="' . base_url() . "user/update" . '" data-aksi="update" value="Simpan">Simpan</button> ';
		return $data;
	}


	public function index()
	{
		$data = Self::class_data();

		$data['folder'] = 'user';
		$data['file']	= 'view.php';
		$data['page'] 	= 'user';
		$data['title'] = 'User';
		$data['subtitle'] = '';
		$this->template->layouts($data);
	}

	public function tampil()
	{
		# code...

		$data['user'] =	$this->mod->get('user', null)->result_array();
		$this->load->view('user/list_user', $data);



		print json_encode('succeess');
	}

	public function show($id)
	{
		# code...
		$data = $this->class_data();

		// $id = $this->input->get('id');
		if ($id != '' || $id != null) {
			# code...
			$data['show'] = $this->mod->get('user', $id, 'id_user')->result_array();
			if ($data['show'][0]['group_user'] === 'Administrator') {
				# code...
				$radio = 'checked=checked';
			} else {
				# code...
				$radio = '';
			}
			if ($data['show'][0]['group_user'] === 'User') {
				# code...
				$radio2 = 'checked=checked';
			} else {
				# code...
				$radio2 = '';
			}
			if ($data['show'] > 0) {
				# code...
				$data['title']	= 'Detail User';
				$data['body'] = '<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Detail</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Update</a>
				</li>
		
			</ul>
			<div class="tab-content" id="myTabContent">
			<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
					<table class="table table-striped">

					<tbody>
					  <tr>
						<th scope="row">Kode User</th>
						<td>' . $data['show'][0]['kd_user'] . '</td>
					  </tr>
					  <tr>
					  <th scope="row">Nama user</th>
					  <td>' . $data['show'][0]['nama_user'] . '</td>
					</tr>
					<tr>
						<th scope="row">Username</th>
						<td>' . $data['show'][0]['username'] . '</td>
					  </tr>
					  <tr>
						<th scope="row">Alamat</th>
						<td>' . $data['show'][0]['alamat'] . '</td>
					  </tr>
					  <tr>
						<th scope="row">Hak akses</th>
						<td>' . $data['show'][0]['group_user'] . '</td>
					  </tr>
					</tbody>
				  </table>
					</div>
				<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
				<form class="form-update">
				<input type="hidden" name="id" value="' . $data['show'][0]['id_user'] . '">
							<div class="form-group">
								<label for="Kode_User">Kode User</label>
								<input type="text" class=" form-control change" readonly value="' . $data['show'][0]['kd_user'] . '" >
							</div>
							<div class="form-group">
								<label for="Kode_User">Kode User</label>
								<input type="text" class=" form-control change" value="' . $data['show'][0]['nama_user'] . '" name="name" >
							</div>
							<div class="form-group">
								<label for="Kode_User">Kode User</label>
								<input type="text" class=" form-control change" value="' . $data['show'][0]['username'] . '" name="username" >
							</div>
							<div class="form-group">
								<label for="Kode_User">Kode User</label>
								<textarea class="form-control" name="address" cols="30" rows="10">' . $data['show'][0]['alamat'] . '</textarea>
							</div>
							<div class="form-group">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="customRadioInline3" ' . $radio . ' name="role" class="custom-control-input" value="Administrator">
										<label class="custom-control-label" for="customRadioInline3">Administrator</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="customRadioInline4" ' . $radio2 . ' name="role" class="custom-control-input" value="User">
										<label class="custom-control-label" for="customRadioInline4">User</label>
									</div>
								</div>
							
							
							<div class="form-group text-right">
								' . $data['buttonUpdate'] . '
								' . $data['buttonRestart'] . '
							</div>
						</form>
				</div>
		
			</div>';
				$data['modal'] = $this->modal->formModal($data['title'], $data['body'], $data['buttonSave'], $data['buttonRestart']);
			}
		}

		print json_encode($data);
	}

	public function getKode()
	{
		# code...
		$data['kode'] = $this->createcode->Gcode('user', 'kd_user');

		print json_encode($data);
	}

	public function store()
	{
		# code...
		$kode = $this->createcode->Gcode('user', 'kd_user');

		// if ($this->input->is_ajax_request()) {
		$this->form_validation->set_rules('name', 'Nama', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('address', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('role', 'Hak Akses', 'trim|required');

		if ($this->form_validation->run() == TRUE) {
			# code...
			$data = [
				'kd_user' => $this->input->post('kode', true),
				'nama_user' => $this->input->post('name', true),
				'username' => $this->input->post('username', true),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'alamat' => $this->input->post('address', true),
				'group_user' => $this->input->post('role')
			];

			// var_dump($data);
			// die();

			$insert = $this->mod->insert('user', $data);


			if ($insert > 0) {
				# code...
				$data['kode'] = $kode;
				$data['msg'] = "berhasil";
			} else {
				# code...
				$data['kode'] = $kode;
				$data['msg'] = "gagal";
			}
		} else {
			# code...
			$data['kode'] = $kode;
			$data['msg'] = "gagal";
		}
		// }
		print json_encode($data);
	}

	public function update()
	{
		# code...
		$id = $this->input->post('id');
		if ($id != null || $id != '') {
			# code...
			$this->form_validation->set_rules('name', 'Nama', 'trim');
			$this->form_validation->set_rules('username', 'Username', 'trim');
			$this->form_validation->set_rules('address', 'Alamat', 'trim');

			if ($this->form_validation->run() == TRUE) {
				# code...
				$data = [
					'id_user'	=>	$id,
					'nama_user' => $this->input->post('name', true),
					'username' => $this->input->post('username', true),
					'alamat' => $this->input->post('address', true),
					'group_user' => $this->input->post('role')
				];
				$update = $this->mod->update('user', 'id_user', $id, $data);

				if ($update > 0) {
					# code...
					$data['msg'] = 'berhasil';
				} else {
					# code...
					$data['msg'] = 'gagal';
				}
			} else {
				# code...
				$data['msg'] = 'gagal insert';
			}
		} else {
			# code...
			$data['msg'] = 'gagal insert id';
		}
		print json_encode($data);
	}

	public function destroy($id)
	{
		# code...
		// $id = $_POST['id'];
		// $id = $this->input->post('id');
		if ($id != '' || $id != null) {
			# code...
			$this->mod->delete('user', 'id_user', $id);
			$data['msg'] = "berhasil";
		}

		// var_dump($id);
		print json_encode($data);
	}
}

/* End of file User.php */
/* Location: ./application/controllers/User.php */
