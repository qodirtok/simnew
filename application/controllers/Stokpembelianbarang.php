<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stokpembelianbarang extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		MY_Controller::is_logged_in();
	}


	public function class_data()
	{
		# code...

		$data['listData'] = 'data-list" data-link="' . base_url() . "stokpembelianbarang/tampilListPembelian" . '';
		$data['buttonSave'] = '<button class="btn generatecode btn-info btn-save" type="submit" data-link="' . base_url() . "stokpembelianbarang/store" . '" data-method="tambah" value="Simpan">Simpan</button> ';
		$data['buttonRestart'] = '<input class="btn btn-success" type="reset" value="Reset">';
		$data['buttonLinktoFormAdd'] = '<button class="btn btn-primary klik-menu" data-link="' . base_url() . "barang/add" . '"><i class="fas fa-plus"></i> Tambah Barang</button>';
		$data['buttonUpdate'] = '<button class="btn generatecode btn-info btn-update" type="submit" data-link="' . base_url() . "barang/update" . '" data-aksi="update" value="Simpan">Simpan</button> ';
		return $data;
	}

	public function getModal()
	{
		# code...
		$get = $this->mod->getModalByNow();

		if ($get == null || $get == '') {
			# code...
			$data['modal'] = 'Rp.' . number_format(0);
		} else {
			# code...
			$data['modal'] = 'Rp.' . number_format($get->modal);
		}

		// var_dump(json_encode($get));
		// die();
		print json_encode($data);
	}

	public function index()
	{
		$data = $this->class_data();

		$data['folder'] = 'stokpembelianbarang';
		$data['file']	= 'view';
		$data['page'] 	= 'stokpembelianbarang';
		$data['title'] = 'Barang';
		$data['subtitle'] = '';

		$data['kategori'] = $this->mod->get('kategori')->result_array();
		$data['satuan'] = $this->mod->get('satuan')->result_array();

		$check = $this->mod->getModalByNow();

		if ($check != null || $check != '') {
			# code...
			$data['modals'] = $check;
			$this->template->layouts($data);
		} else {
			# code...
			$data['modals'] = $check;
			$this->session->set_flashdata('modalMessage', 'input modal');
			$this->template->layouts($data);
		}
	}

	public function tampilListPembelian()
	{
		# code...
		// $data = [
		// 	'asdas' => 'asdasd'
		// ];
		// $_REQUEST
		$data['tmp_pembelian'] = $this->mod->get('barang_masuk_tmp', date('Y-m-d'), 'create_at')->result_array();
		// echo json_encode($data);
		$this->load->view('stokpembelianbarang/tampil', $data);
	}

	public function addTocart()
	{
		# code...


		// $data = [
		// 	'nota' => $this->input->post('nota'),
		// 	'tanggal' => $this->input->post('tanggal'),
		// 	'kode_barang' => $this->input->post('kode'),
		// 	'nama_barang' => $this->input->post('namaBarang'),
		// 	'stok' => $this->input->post('qty'),
		// 	'harga_jual' => $this->input->post('hargaJual'),
		// 	'harga_beli' => $this->input->post('hargaBeli')
		// ];

		// $this->cart->insert($data);
		// echo $this->show_cart();

		// die();


		// $data['kode_barang'] = $this->input->post('kode');

		// $data['check'] = $this->mod->get('barang', $data['kode_barang'], 'kode_barang')->row_array();


		// $data['count'] = '';
		// if (isset($data['check'])) {
		// 	# code...
		// 	$data['count'] = count($data['check']);
		// }

		// if ($data['count'] > 0) {


		// 	# code...
		// 	if ($data['check']['kode_barang'] == $data['kode_barang']) {

		// 		# code...
		// 		// (kondisi ? then : else)

		// 		$namaBarang = ($this->input->post('namaBarang') == $data['check']['nama_barang'] ? $data['check']['nama_barang'] : $this->input->post('namaBarang'));

		// 		$totalStok = ($this->input->post('qty') == $data['check']['stok'] ? $data['check']['stok'] : $data['check']['stok'] + $this->input->post('qty'));

		// 		$totalHBeli = $this->input->post('hargaBeli');

		// 		$totalHJual = $this->input->post('hargaJual');

		// 		// $id_satuan = ($this->input->post('namaBarang') == $data['check']['nama_barang'] ? $data['check']['nama_barang'] : $this->input->post('namaBarang'));

		// 		// $id_kategori = ($this->input->post('namaBarang') == $data['check']['nama_barang'] ? $data['check']['nama_barang'] : $this->input->post('namaBarang'));
		// 		// $data['a'] = $this->mod->get('barang', $data['kode_barang'], 'kode_barang')->row_array();

		// 		$data = [
		// 			'kode_barang' => $data['check']['kode_barang'],
		// 			'nama_barang' => $namaBarang,
		// 			'harga_jual' => $this->input->post('hargaJual'),
		// 			'harga_beli' => $this->input->post('hargaBeli'),
		// 			'keuntungan' => ($totalHJual - $totalHBeli),
		// 			'stok' => $totalStok,

		// 		];

		// 		$this->mod->update('barang', 'kode_barang', $data['kode_barang'], $data);
		// 	}
		// } else {
		// 	# code...
		$keuntungan = $this->input->post('hargaJual') - $this->input->post('hargaBeli');
		$insertData = [
			'nota' => $this->input->post('nota'),
			'tanggal' => $this->input->post('tanggal'),
			'kode_barang' => $this->input->post('kode'),
			'nama_barang' => $this->input->post('namaBarang'),
			'stok' => $this->input->post('qty'),
			'harga_jual' => $this->input->post('hargaJual'),
			'harga_beli' => $this->input->post('hargaBeli'),
			'keuntungan' => $keuntungan,
			'id_kategori' => $this->input->post('kategori'),
			'id_satuan' => $this->input->post('satuan'),
			'create_at' => date('Y-m-d')
		];

		$insert = $this->mod->insert('barang_masuk_tmp', $insertData);
		$data['msg'] = 'berhasil';
		echo json_encode($data);
	}

	public function store()
	{
		$insertCount = 0;
		$data = $this->mod->get_MultipleWhere('barang_masuk_tmp', "create_at = '" . date('Y-m-d') . "'")->result_array();
		$dataCount = count($data);

		for ($i = 0; $i < $dataCount; $i++) {
			unset($data[$i]['diskon']);

			$getBarang = $this->mod->get_MultipleWhere('barang', "kode_barang = '" . $data[$i]['kode_barang'] . "'")->result_array();

			if ($getBarang == null) {
				$newBarang = $data[$i];
				unset($newBarang['id_barang_masuk']);
				unset($newBarang['nota']);
				unset($newBarang['tanggal']);
				unset($newBarang['create_at']);
				$insertBarang = $this->mod->insert('barang', $newBarang);
			} else {
				if ($data[$i]['harga_beli'] != $getBarang[0]['harga_beli']) {
					$getBarang[0]['harga_beli'] = $data[$i]['harga_beli'];
				}
				if ($data[$i]['harga_jual'] != $getBarang[0]['harga_jual']) {
					$getBarang[0]['harga_jual'] = $data[$i]['harga_jual'];
				}
				if ($data[$i]['keuntungan'] != $getBarang[0]['keuntungan']) {
					$getBarang[0]['keuntungan'] = $data[$i]['keuntungan'];
				}
				$getBarang[0]['stok'] = intval($data[$i]['stok']) + intval($getBarang[0]['stok']);
				$updateBarang = $this->mod->update('barang', 'kode_barang', $getBarang[0]['kode_barang'], $getBarang[0]);
			}
			$id_barang_masuk = $data[$i]['id_barang_masuk'];
			unset($data[$i]['id_barang_masuk']);
			$insert = $this->mod->insert('barang_masuk', $data[$i]);
			$last_id = $this->mod->insert_id();

			$insertLog = [
				'kode_barang' => $data[$i]['kode_barang'],
				'id_barang_masuk' => $last_id,
				'tanggal' => $data[$i]['tanggal'],
				'stok' => $data[$i]['stok'],
				'keterangan' => 'Barang Masuk',
			];

			$insertLog = $this->mod->insert('log_stok', $insertLog);
			$insertCount++;
		}


		if ($insertCount == $dataCount) {
			# code...
			for ($j = 0; $j < $dataCount; $j++) {
				$delete = $this->mod->delete('barang_masuk_tmp', 'id_barang_masuk', $id_barang_masuk);
			}
			$data['msg'] = 'berhasil';
		} else {
			# code...
			$data['msg'] = 'gagal';
		}

		echo json_encode($data);
	}

	public function destroy($id)
	{
		# code...
		if ($id != '' || $id != null) {
			# code...
			$delete = $this->mod->delete('barang_masuk_tmp', 'id_barang_masuk', $id);
			if ($delete > 0) {
				# code...
				$data['msg'] = 'berhasil';
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
		}
		print json_encode($data);
	}
}

/* End of file Stokpembelianbarang.php */