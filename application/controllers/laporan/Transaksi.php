<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once(APPPATH . 'core/Mypdf.php');

class Transaksi extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        MY_Controller::is_logged_in();
    }

    public function class_data()
    {
        $data['listData'] = 'data-list" data-link="' . base_url() . "laporan/transaksi/getData" . '';
        $data['buttonCetak'] = '<button class="btn generatecode btn-info btn-cetak" type="submit" data-link="' . base_url() . "laporan/transaksi/getData" . '">Cari</button>';
        $data['buttonRestart'] = '<input class="btn btn-success" type="reset" value="Reset">';
        $data['listLaporan'] = 'data-list-laporan" data-link="' . base_url() . "laporan/transaksi/getData" . '';
        return $data;
    }

    public function pembelian()
    {
        $data = $this->class_data();
        $data['folder'] = 'laporan/transaksi/';
        $data['file']    = 'view';
        $data['page']     = 'pembelian';
        $data['title'] = 'Laporan';
        $data['sub_title'] = 'Transaksi Pembelian';
        $data['tipe'] = 'barang_masuk';
        $data['buttonLinktoCetak'] = '<button class="btn btn-primary btn-to-cetak" data-link="' . base_url() . "laporan/transaksi/getLaporan" . '" style="display: none;"><i class="fas fa-print"></i> Cetak</button>';
        $this->template->layouts($data);
    }

    public function penjualan()
    {
        $data = $this->class_data();
        $data['folder'] = 'laporan/transaksi/';
        $data['file']    = 'view';
        $data['page']     = 'penjualan';
        $data['title'] = 'Laporan';
        $data['sub_title'] = 'Transaksi Penjualan';
        $data['tipe'] = 'barang_keluar';
        $data['buttonLinktoCetak'] = '<button class="btn btn-primary btn-to-cetak" data-link="' . base_url() . "laporan/transaksi/getLaporan" . '" style="display: none;"><i class="fas fa-print"></i> Cetak</button>';
        $this->template->layouts($data);
    }

    public function stok()
    {
        $data = $this->class_data();
        $data['folder'] = 'laporan/transaksi/';
        $data['file']    = 'view-stok';
        $data['page']     = 'stok';
        $data['title'] = 'Laporan';
        $data['sub_title'] = 'Transaksi Stok';
        $data['subtitle'] = '';
        $data['tipe'] = 'log_stok';
        $data['buttonLinktoCetak'] = '<button class="btn btn-primary btn-to-cetak" data-link="' . base_url() . "laporan/transaksi/getLaporan" . '" style="display: none;"><i class="fas fa-print"></i> Cetak</button>';
        $this->template->layouts($data);
    }

    public function kasir()
    {
        $data = $this->class_data();
        $data['folder'] = 'laporan/transaksi/';
        $data['file']    = 'view-kasir';
        $data['page']     = 'kasir';
        $data['title'] = 'Laporan';
        $data['sub_title'] = 'Transaksi Kasir';
        $data['tipe'] = 'kasir';
        $data['buttonCetak'] = '<button class="btn generatecode btn-info btn-cetak-kasir" type="submit" data-link="' . base_url() . "laporan/transaksi/getDataKasir" . '">Cari</button>';
        $data['buttonLinktoCetak'] = '<button class="btn btn-primary btn-to-cetak-kasir" data-link="' . base_url() . "laporan/transaksi/getLaporanKasir" . '" style="display: none;"><i class="fas fa-print"></i> Cetak</button>';
        $this->template->layouts($data);
    }

    public function addColumnKasir($pdf)
    {
        $pdf->SetFont('', 'B', 9);
        $pdf->Cell(10, 8, "No", 1, 0, 'C');
        $pdf->Cell(30, 8, "Tipe", 1, 0, 'C');
        $pdf->Cell(35, 8, "Nama Barang", 1, 0, 'C');
        $pdf->Cell(35, 8, "Harga Satuan", 1, 0, 'C');
        $pdf->Cell(15, 8, "Diskon", 1, 0, 'C');
        $pdf->Cell(15, 8, "Qty", 1, 0, 'C');
        $pdf->Cell(25, 8, "Sub Total", 1, 0, 'C');
        $pdf->Cell(25, 8, "Saldo", 1, 1, 'C');
        $pdf->SetFont('', '', 9);
    }

    private function addRowKasir($pdf, $no, $value, $harga, $diskon, $sub_total, $saldo, $tipe)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(30, 8, $tipe, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['nama_barang'], 1, 0, 'C');
        $pdf->Cell(35, 8, "Rp. " . number_format($value['harga_jual'], 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(15, 8, $diskon . '%', 1, 0, 'C');
        $pdf->Cell(15, 8, $value['stok'], 1, 0, 'C');
        $pdf->Cell(25, 8, "Rp. " . number_format($sub_total, 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(25, 8, "Rp. " . number_format($saldo, 2, ',', '.'), 1, 1, 'C');
    }

    public function addColumnStok($pdf)
    {
        $pdf->Ln(10);
        $pdf->SetFont('', 'B', 9);
        $pdf->Cell(10, 8, "No", 1, 0, 'C');
        $pdf->Cell(50, 8, "Nama Barang", 1, 0, 'C');
        $pdf->Cell(40, 8, "Keterangan", 1, 0, 'C');
        $pdf->Cell(30, 8, "Tanggal", 1, 0, 'C');
        $pdf->Cell(25, 8, "Stok", 1, 0, 'C');
        $pdf->Cell(40, 8, "Sub Total", 1, 1, 'C');
        $pdf->SetFont('', '', 9);
    }

    private function addRowStok($pdf, $no, $value, $saldo)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(50, 8, $value['nama_barang'], 1, 0, 'C');
        $pdf->Cell(40, 8, $value['keterangan'], 1, 0, 'C');
        $pdf->Cell(30, 8, date('d-m-Y', strtotime($value['tanggal'])), 1, 0, 'C');
        $pdf->Cell(25, 8, $value['stok'], 1, 0, 'C');
        $pdf->Cell(40, 8, $saldo, 1, 1, 'C');
    }

    public function addColumn($pdf)
    {
        $pdf->Ln(10);
        $pdf->SetFont('', 'B', 9);
        $pdf->Cell(10, 8, "No", 1, 0, 'C');
        $pdf->Cell(35, 8, "Nama Barang", 1, 0, 'C');
        $pdf->Cell(20, 8, "Tanggal", 1, 0, 'C');
        $pdf->Cell(20, 8, "Harga Jual", 1, 0, 'C');
        $pdf->Cell(20, 8, "Diskon", 1, 0, 'C');
        $pdf->Cell(20, 8, "Jumlah", 1, 0, 'C');
        $pdf->Cell(30, 8, "Sub Total", 1, 0, 'C');
        $pdf->Cell(40, 8, "Saldo", 1, 1, 'C');
        $pdf->SetFont('', '', 9);
    }

    private function addRowPenjualan($pdf, $no, $value, $sub_total, $saldo)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['nama_barang'], 1, 0, '');
        $pdf->Cell(20, 8, date('d-m-Y', strtotime($value['tanggal'])), 1, 0, 'C');
        $pdf->Cell(20, 8, "Rp. " . number_format($value['harga_jual'], 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(20, 8, $value['diskon'] . '%', 1, 0, 'C');
        $pdf->Cell(20, 8, $value['stok'], 1, 0, 'C');
        $pdf->Cell(30, 8, "Rp. " . number_format($sub_total, 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(40, 8, "Rp. " . number_format($saldo, 2, ',', '.'), 1, 1, 'C');
    }

    private function addRowPembelian($pdf, $no, $value, $sub_total, $saldo)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['nama_barang'], 1, 0, '');
        $pdf->Cell(20, 8, date('d-m-Y', strtotime($value['tanggal'])), 1, 0, 'C');
        $pdf->Cell(20, 8, "Rp. " . number_format($value['harga_beli'], 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(20, 8, $value['diskon'] . '%', 1, 0, 'C');
        $pdf->Cell(20, 8, $value['stok'], 1, 0, 'C');
        $pdf->Cell(30, 8, "Rp. " . number_format($sub_total, 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(40, 8, "Rp. " . number_format($saldo, 2, ',', '.'), 1, 1, 'C');
    }

    public function addRowJumlah($pdf, $saldo)
    {
        $pdf->SetFont('', 'B', 9);
        $pdf->Cell(155, 8, 'Jumlah', 1, 0, 'C');
        $pdf->Cell(40, 8, "Rp. " . number_format($saldo, 2, ',', '.'), 1, 1, 'C');
    }
    public function addRowJumlahKasir($pdf, $saldo, $modal)
    {
        $total = $saldo - $modal;
        $pdf->SetFont('', 'B', 9);
        $pdf->Cell(165, 8, 'Total Saldo', 1, 0, 'C');
        $pdf->Cell(25, 8, "Rp. " . number_format($saldo, 2, ',', '.'), 1, 1, 'C');
        $pdf->Cell(165, 8, 'Total Modal', 1, 0, 'C');
        $pdf->Cell(25, 8, "Rp. " . number_format($modal, 2, ',', '.'), 1, 1, 'C');
        $pdf->Cell(165, 8, 'Total Laba-Rugi', 1, 0, 'C');
        $pdf->Cell(25, 8, "Rp. " . number_format($total, 2, ',', '.'), 1, 1, 'C');
    }

    public function getLaporan($tipe, $tanggal, $tanggal2)
    {
        $pdf = new \MYPDF();

        $pdf->AddPage();
        $pdf->setY(25);
        $pdf->SetAutoPageBreak(true, 30);
        $pdf->SetMargins('10', '30', '10', false);
        $pdf->SetTitle('Laporan Transaksi');

        if ($tipe == "barang_masuk") {
            $data = $this->mod->getLaporan('barang_masuk', $tanggal, $tanggal2)->result_array();
            $this->addColumn($pdf);
        } else if ($tipe == "barang_keluar") {
            $data = $this->mod->getLaporan('barang_keluar', $tanggal, $tanggal2)->result_array();
            // print $this->unit->run($data, 'is_array', 'tes tampil data laporan penjualan');
            // die();
            $this->addColumn($pdf);
        } else if ($tipe == "log_stok") {
            $data = [
                'where' => 'log_stok.tanggal BETWEEN "' . $tanggal . '" AND "' . $tanggal2 . '"',
                'order_by' => 'log_stok.id_log ASC, log_stok.tanggal ASC',
                'db' => 'barang',
                'db1' => 'barang_keluar',
                'join1' => 'barang_keluar.kode_barang = barang.kode_barang',
                'db2' => 'barang_masuk',
                'join2' => 'barang_masuk.kode_barang = barang.kode_barang',
                'db3' => 'log_stok',
                'join3' => 'log_stok.kode_barang = barang.kode_barang',
            ];
            $data = $this->mod->getLaporanJoin($data['db'], $data['where'], $data['order_by'], $data['db1'], $data['join1'], $data['db2'], $data['join2'], $data['db3'], $data['join3'])->result_array();

            $this->addColumnStok($pdf);
        }

        // row
        $saldo = 0;
        foreach ($data as $k => $value) {
            if ($tipe == 'log_stok') {
                if ($value['keterangan'] == 'Barang Keluar' || $value['keterangan'] == 'Retur Barang Masuk') {
                    $saldo -= $value['stok'];
                } else if ($value['keterangan'] == 'Barang Masuk') {
                    $saldo += $value['stok'];
                }
                $this->addRowStok($pdf, $k + 1, $value, $saldo);
            } else if ($tipe == 'barang_keluar') {
                $sub_total = ($value['harga_jual'] - ($value['harga_jual'] * ($value['diskon'] / 100))) * $value['stok'];
                $saldo += $sub_total;
                $this->addRowPenjualan($pdf, $k + 1, $value, $sub_total, $saldo);
            } else if ($tipe == 'barang_masuk') {
                $sub_total = ($value['harga_beli'] - ($value['harga_beli'] * ($value['diskon'] / 100))) * $value['stok'];
                $saldo += $sub_total;
                $this->addRowPembelian($pdf, $k + 1, $value, $sub_total, $saldo);
            }
        }

        $this->addRowJumlah($pdf, $saldo);

        $tanggal = date('d-m-Y');
        $pdf->Output('Laporan Order - ' . $tanggal . '.pdf');
    }

    public function getLaporanKasir($tipe, $tanggal, $tanggal2)
    {
        $pdf = new \MYPDF();

        $pdf->AddPage();
        $pdf->setY(25);
        $pdf->SetAutoPageBreak(true, 30);
        $pdf->SetMargins('10', '30', '10', false);
        $pdf->SetTitle('Laporan Transaksi');

        $data = $this->mod->getMultipleModal('modal', $tanggal, $tanggal2)->result_array();

        $db = [
            'order_by' => 'log_stok.id_log ASC, log_stok.tanggal ASC',
            'select' => 'barang.nama_barang, barang_keluar.harga_jual, barang_masuk.harga_beli, barang_keluar.diskon as diskon_jual, barang_masuk.diskon as diskon_beli, log_stok.keterangan, barang_keluar.tanggal as tanggal_jual, barang_masuk.tanggal as tanggal_beli, log_stok.stok',
            'db' => 'barang',
            'db1' => 'barang_keluar',
            'join1' => 'barang_keluar.kode_barang = barang.kode_barang',
            'db2' => 'barang_masuk',
            'join2' => 'barang_masuk.kode_barang = barang.kode_barang',
            'db3' => 'log_stok',
            'join3' => 'log_stok.kode_barang = barang.kode_barang',
        ];

        foreach ($data as $value) {
            $dateNow = date('Y-m-d', strtotime($value['create_at']));
            $pdf->Cell(40, 8, $dateNow, 1, 1, 'C');

            $db['where'] = '(barang_masuk.tanggal = "' . $dateNow . '") AND (barang_keluar.tanggal = "' . $dateNow . '") AND (log_stok.keterangan = "Barang Masuk" OR log_stok.keterangan = "Barang Keluar")';

            $getDb = $this->mod->getLaporanJoin($db['db'], $db['select'], $db['where'], $db['order_by'], $db['db1'], $db['join1'], $db['db2'], $db['join2'], $db['db3'], $db['join3'])->result_array();

            $tipe = '';
            $saldo = 0;
            if ($getDb == null) {
                $pdf->Cell(190, 8, 'Tidak Ada Data', 1, 1, 'C');
            } else {
                $this->addColumnKasir($pdf);
                foreach ($getDb as $k => $output) {
                    if ($output['keterangan'] == 'Barang Masuk') {
                        $harga = $output['harga_beli'];
                        $diskon = $output['diskon_beli'];
                        $sub_total = ($harga - ($harga * ($diskon / 100))) * $output['stok'];
                        $saldo -= $sub_total;
                        $tipe = "Pembelian";
                    } else if ($output['keterangan'] == 'Barang Keluar') {
                        $harga = $output['harga_jual'];
                        $diskon = $output['diskon_jual'];
                        $sub_total = ($harga - ($harga * ($diskon / 100))) * $output['stok'];
                        $saldo += $sub_total;
                        $tipe = "Penjualan";
                    }
                    $this->addRowKasir($pdf, $k + 1, $output, $harga, $diskon, $sub_total, $saldo, $tipe);
                }
                $this->addRowJumlahKasir($pdf, $saldo, $value['modal']);
            }
            // enter
            $pdf->Ln(10);
        }

        $tanggal = date('d-m-Y');
        $pdf->Output('Laporan Order - ' . $tanggal . '.pdf');
    }

    public function getData()
    {
        $table = $this->input->post('tipe');
        $tanggal = $this->input->post('tanggal');
        $tanggal2 = $this->input->post('tanggal2');

        $db = [
            'order_by' => 'log_stok.id_log ASC, log_stok.tanggal ASC',
            'db' => 'barang',
            'db1' => 'barang_keluar',
            'join1' => 'barang_keluar.kode_barang = barang.kode_barang',
            'db2' => 'barang_masuk',
            'join2' => 'barang_masuk.kode_barang = barang.kode_barang',
            'db3' => 'log_stok',
            'join3' => 'log_stok.kode_barang = barang.kode_barang',
        ];

        if ($tanggal == '' || $tanggal == null) {
            $data['msg']  = 'gagal';
            $data['msgvalue']  = 'Tanggal awal belum diatur';
            print json_encode($data);
        } else if ($tanggal2 == '' || $tanggal2 == null) {
            $data['msg']  = 'gagal';
            $data['msgvalue']  = 'Tanggal akhir belum diatur';
            print json_encode($data);
        } else if ($tanggal > $tanggal2) {
            $data['msg']  = 'gagal';
            $data['msgvalue']  = 'Tanggal awal harus lebih besar dari tanggal akhir';
            print json_encode($data);
        } else if ($table == 'log_stok') {
            $db['where'] = 'log_stok.tanggal BETWEEN "' . $tanggal . '" AND "' . $tanggal2 . '"';

            $data['getSearch'] = $this->mod->getLaporanJoin($db['db'], null, $db['where'], $db['order_by'], $db['db1'], $db['join1'], $db['db2'], $db['join2'], $db['db3'], $db['join3'])->result_array();
            $this->load->view('laporan/transaksi/tampil-stok', $data);
        } else if ($table == 'barang_keluar' || $table = 'barang_masuk') {
            $data['getSearch'] = $this->mod->getLaporan($table, $tanggal, $tanggal2)->result_array();
            $this->load->view('laporan/transaksi/tampil', $data);
        }
    }

    public function getDataKasir()
    {
        $tanggal = $this->input->post('tanggal');
        $tanggal2 = $this->input->post('tanggal2');


        $db = [
            'order_by' => 'log_stok.id_log ASC, log_stok.tanggal ASC',
            'db' => 'barang',
            'db1' => 'barang_keluar',
            'join1' => 'barang_keluar.kode_barang = barang.kode_barang',
            'db2' => 'barang_masuk',
            'join2' => 'barang_masuk.kode_barang = barang.kode_barang',
            'db3' => 'log_stok',
            'join3' => 'log_stok.kode_barang = barang.kode_barang',
        ];

        if (($tanggal == '' || $tanggal == null) && ($tanggal2 == '' || $tanggal2 == null)) {
            $data['msg']  = 'gagal';
            $data['msgvalue']  = 'Tanggal belum diatur';
            print json_encode($data);
        } else if ($tanggal2 == '' || $tanggal2 == null) {
            $db['where'] = '(barang_masuk.tanggal = "' . $tanggal . '" AND barang_keluar.tanggal = "' . $tanggal . '") AND (log_stok.keterangan = "Barang Masuk" OR log_stok.keterangan = "Barang Keluar")';
        } else if ($tanggal > $tanggal2) {
            $data['msg']  = 'gagal';
            $data['msgvalue']  = 'Tanggal awal harus lebih besar dari tanggal akhir';
            print json_encode($data);
        } else if ($tanggal != '' && $tanggal2 != '') {
            $db['where'] = '(barang_masuk.tanggal BETWEEN "' . $tanggal . '" AND "' . $tanggal2 . '") AND (barang_keluar.tanggal BETWEEN "' . $tanggal . '" AND "' . $tanggal2 . '") AND (log_stok.keterangan = "Barang Masuk" OR log_stok.keterangan = "Barang Keluar")';
        }

        if (!isset($data['msg'])) {
            $db['select'] = 'barang.nama_barang, barang_keluar.harga_jual, barang_masuk.harga_beli, barang_keluar.diskon as diskon_jual, barang_masuk.diskon as diskon_beli, log_stok.keterangan, barang_keluar.tanggal as tanggal_jual, barang_masuk.tanggal as tanggal_beli, log_stok.stok';

            $data['getSearch'] = $this->mod->getLaporanJoin($db['db'], $db['select'], $db['where'], $db['order_by'], $db['db1'], $db['join1'], $db['db2'], $db['join2'], $db['db3'], $db['join3'])->result_array();

            // print "<pre>";
            // var_dump($data);

            $this->load->view('laporan/transaksi/tampil-kasir', $data);
        }
    }
}

/* End of file Tpenjualan.php */
