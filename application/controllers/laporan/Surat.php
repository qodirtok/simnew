<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once(APPPATH . 'core/Mypdf.php');

class Surat extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        MY_Controller::is_logged_in();
    }

    public function class_data()
    {
        $data['listData'] = 'data-list" data-link="' . base_url() . "laporan/surat/getData" . '';
        $data['buttonCetak'] = '<button class="btn generatecode btn-info btn-cetak-surat" type="submit" data-link="' . base_url() . "laporan/surat/getData" . '">Cari</button>';
        $data['buttonRestart'] = '<input class="btn btn-success" type="reset" value="Reset">';
        $data['listLaporan'] = 'data-list-laporan" data-link="' . base_url() . "laporan/surat/getData" . '';
        return $data;
    }

    public function index()
    {
        $data = $this->class_data();
        $data['folder'] = 'laporan/surat/';
        $data['file']    = 'view';
        $data['page']     = 'lsurat';
        $data['title'] = 'Laporan';
        $data['sub_title'] = 'Surat';
        $data['buttonLinktoCetak'] = '<button class="btn btn-primary btn-to-cetak-surat" data-link="' . base_url() . "laporan/surat/getLaporan" . '" style="display: none;"><i class="fas fa-print"></i> Cetak</button>';
        $this->template->layouts($data);
    }

    public function addColumn($pdf)
    {
        $pdf->Ln(10);
        $pdf->SetFont('', 'B', 9);
        $pdf->Cell(10, 8, "No", 1, 0, 'C');
        $pdf->Cell(40, 8, "Nomor Surat", 1, 0, 'C');
        $pdf->Cell(40, 8, "Instansi", 1, 0, 'C');
        $pdf->Cell(50, 8, "Topik", 1, 0, 'C');
        $pdf->Cell(35, 8, "Tanggal", 1, 1, 'C');
        $pdf->SetFont('', '', 9);
    }

    private function addRow($pdf, $no, $value, $sub_total = null, $saldo = null)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(40, 8, $value['nomor_surat'], 1, 0, 'C');
        $pdf->Cell(40, 8, $value['instansi'], 1, 0, 'C');
        $pdf->Cell(50, 8, $value['topik'], 1, 0, 'C');
        $pdf->Cell(35, 8, date('d-m-Y', strtotime($value['tanggal'])), 1, 1, 'C');
    }

    public function addColumnCampur($pdf)
    {
        $pdf->SetFont('', 'B', 9);
        $pdf->Cell(10, 8, "No", 1, 0, 'C');
        $pdf->Cell(30, 8, "Tipe", 1, 0, 'C');
        $pdf->Cell(30, 8, "Nomor Surat", 1, 0, 'C');
        $pdf->Cell(30, 8, "Instansi", 1, 0, 'C');
        $pdf->Cell(40, 8, "Topik", 1, 0, 'C');
        $pdf->Cell(35, 8, "Tanggal", 1, 1, 'C');
        $pdf->SetFont('', '', 9);
    }

    private function addRowCampur($pdf, $no, $value, $ket_tipe)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(30, 8, $ket_tipe, 1, 0, 'C');
        $pdf->Cell(30, 8, $value['nomor_surat'], 1, 0, 'C');
        $pdf->Cell(30, 8, $value['instansi'], 1, 0, 'C');
        $pdf->Cell(40, 8, $value['topik'], 1, 0, 'C');
        $pdf->Cell(35, 8, date('d-m-Y', strtotime($value['tanggal'])), 1, 1, 'C');
    }

    public function addRowJumlah($pdf, $no)
    {
        $pdf->SetFont('', 'B', 11);
        $pdf->Cell(140, 8, 'Jumlah Surat', 1, 0, 'C');
        $pdf->Cell(35, 8, $no, 1, 1, 'C');
    }

    public function getLaporan($tipe, $tanggal, $tanggal2)
    {
        $pdf = new \MYPDF();

        $pdf->AddPage();
        $pdf->setY(25);
        $pdf->SetAutoPageBreak(true, 40);
        $pdf->SetMargins('10', '30', '10', false);
        $pdf->SetTitle('Laporan Surat');

        if ($tipe == "surat_masuk") {
            $data = $this->mod->getLaporan('surat', $tanggal, $tanggal2, 'type= "masuk"')->result_array();
            $pdf->SetFont('', 'B', 12);
            $pdf->cell(40, 8, 'Jenis: Surat Masuk', 0, 1, 'L');
            $this->addColumn($pdf);
        } else if ($tipe == "surat_keluar") {
            $data = $this->mod->getLaporan('surat', $tanggal, $tanggal2, 'type= "keluar"')->result_array();
            $pdf->SetFont('', 'B', 12);
            $pdf->cell(40, 8, 'Jenis: Surat Keluar', 0, 1, 'L');
            $this->addColumn($pdf);
        }

        // row
        if ($tipe == 'campur') {
            $tgl1 = new DateTime($tanggal);
            $tgl2 = new DateTime($tanggal2);
            $selisih_tanggal = $tgl2->diff($tgl1)->format("%a");;

            for ($i = 1; $i <= intval($selisih_tanggal); $i++) {
                $dateNow = date('Y-m-d', strtotime("+" . $i . " day", strtotime($tanggal)));
                $no = 1;

                $data = $this->mod->getLaporan('surat', $dateNow, $dateNow)->result_array();
                $pdf->Cell(40, 8, $dateNow, 1, 1, 'C');
                if ($data == null || $data == '') {
                    $pdf->Cell(175, 8, 'Tidak Ada Data', 1, 1, 'C');
                } else {
                    $this->addColumnCampur($pdf);

                    foreach ($data as $k => $value) {
                        if ($value['type'] == 'masuk') {
                            $ket_tipe = "Surat Masuk";
                        } elseif ($value['type'] == 'keluar') {
                            $ket_tipe = "Surat Keluar";
                        }
                        $this->addRowCampur($pdf, $no++, $value, $ket_tipe);
                    }
                    $this->addRowJumlah($pdf, $no - 1);
                }
                $pdf->Ln(10);
            }
        } else {
            $no = 1;
            foreach ($data as $value) {
                $this->addRow($pdf, $no++, $value);
            }
            $this->addRowJumlah($pdf, $no - 1);
        }



        $tanggal = date('d-m-Y');
        $pdf->Output('Laporan Order - ' . $tanggal . '.pdf');
    }

    public function getData()
    {
        $tipe = $this->input->post('tipe');
        $tanggal = $this->input->post('tanggal');
        $tanggal2 = $this->input->post('tanggal2');

        if ($tanggal == '' || $tanggal == null) {
            $data['msg']  = 'gagal';
            $data['msgvalue']  = 'Tanggal awal belum diatur';
            print json_encode($data);
        } else if ($tanggal2 == '' || $tanggal2 == null) {
            $data['msg']  = 'gagal';
            $data['msgvalue']  = 'Tanggal akhir belum diatur';
            print json_encode($data);
        } else if ($tanggal > $tanggal2) {
            $data['msg']  = 'gagal';
            $data['msgvalue']  = 'Tanggal awal harus lebih besar dari tanggal akhir';
            print json_encode($data);
        } else if ($tipe == '' || $tipe == null) {
            $data['msg']  = 'gagal';
            $data['msgvalue']  = 'Tipe belum dipilih';
            print json_encode($data);
        } else if ($tipe == 'surat_keluar') {
            $data['getSearch'] = $this->mod->getLaporan('surat', $tanggal, $tanggal2, 'type= "keluar"')->result_array();
        } else if ($tipe == 'surat_masuk') {
            $data['getSearch'] = $this->mod->getLaporan('surat', $tanggal, $tanggal2, 'type= "masuk"')->result_array();
        } else if ($tipe == 'campur') {
            $data['getSearch'] = $this->mod->getLaporan('surat', $tanggal, $tanggal2)->result_array();
        }

        $this->load->view('laporan/surat/tampil', $data);
    }
}

/* End of file Tpenjualan.php */
