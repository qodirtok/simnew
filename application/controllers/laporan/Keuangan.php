<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once(APPPATH . 'core/Mypdf.php');

class Keuangan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        MY_Controller::is_logged_in();
    }

    public function class_data()
    {
    }

    public function buku_besar()
    {
        $data = $this->class_data();
        $data['folder'] = 'laporan/keuangan/';
        $data['file']    = 'view';
        $data['page']     = 'buku_besar';
        $data['title'] = 'Laporan';
        $data['sub_title'] = 'Buku Besar';
        $data['tipe'] = 'buku_besar';
        $data['buttonLinktoCetak'] = '<button class="btn btn-primary btn-to-cetak" data-link="' . base_url() . "laporan/keuangan/getLaporan" . '" ><i class="fas fa-print"></i> Cetak</button>';
        $this->template->layouts($data);
    }

    public function neraca_saldo()
    {
        $data = $this->class_data();
        $data['folder'] = 'laporan/keuangan/';
        $data['file']    = 'view';
        $data['page']     = 'neraca_saldo';
        $data['title'] = 'Laporan';
        $data['sub_title'] = 'Neraca Saldo';
        $data['tipe'] = 'neraca_saldo';
        $data['buttonLinktoCetak'] = '<button class="btn btn-primary btn-to-cetak" data-link="' . base_url() . "laporan/keuangan/getLaporan" . '" ><i class="fas fa-print"></i> Cetak</button>';
        $this->template->layouts($data);
    }

    public function laba_rugi()
    {
        $data = $this->class_data();
        $data['folder'] = 'laporan/keuangan/';
        $data['file']    = 'view';
        $data['page']     = 'laba_rugi';
        $data['title'] = 'Laporan';
        $data['sub_title'] = 'Laba Rugi';
        $data['tipe'] = 'laba_rugi';
        $data['buttonLinktoCetak'] = '<button class="btn btn-primary btn-to-cetak" data-link="' . base_url() . "laporan/keuangan/getLaporan" . '" ><i class="fas fa-print"></i> Cetak</button>';
        $this->template->layouts($data);
    }

    public function neraca()
    {
        $data = $this->class_data();
        $data['folder'] = 'laporan/keuangan/';
        $data['file']    = 'view-neraca';
        $data['page']     = 'neraca';
        $data['title'] = 'Laporan';
        $data['sub_title'] = 'Neraca';
        $data['tipe'] = 'neraca';
        $data['buttonLinktoCetak'] = '<button class="btn btn-primary btn-to-cetak-neraca" data-link="' . base_url() . "laporan/keuangan/getLaporanNeraca" . '" ><i class="fas fa-print"></i> Cetak</button>';
        $this->template->layouts($data);
    }

    public function addColumnNeracaSaldo($pdf)
    {
        $pdf->Ln(10);
        $pdf->SetFont('', 'B', 9);
        $pdf->Cell(10, 8, "No", 1, 0, 'C');
        $pdf->Cell(35, 8, "Tanggal", 1, 0, 'C');
        $pdf->Cell(70, 8, "Keterangan", 1, 0, 'C');
        $pdf->Cell(40, 8, "Debet", 1, 0, 'C');
        $pdf->Cell(40, 8, "Kredit", 1, 1, 'C');
        $pdf->SetFont('', '', 8);
    }

    public function addRowKasNeracaSaldo($pdf, $no, $value)
    {
        if ($value['debet'] != 0) {
            $nominal = $value['debet'];
            $nominal1 = $value['debet'];
            $nominal2 = '0';
        } else if ($value['kredit'] != 0) {
            $nominal = $value['kredit'];
            $nominal1 = '0';
            $nominal2 = $value['kredit'];
        }

        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['tanggal'], 1, 0, 'C');
        $pdf->Cell(70, 8, $value['nama_akun'] . ' sebesar Rp. ' . number_format($nominal, 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(40, 8, 'Rp. ' . number_format($nominal1, 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(40, 8, 'Rp. ' . number_format($nominal2, 2, ',', '.'), 1, 1, 'C');
    }

    public function addRowKasNeraca($pdf, $value)
    {
        if ($value['debet'] != 0) {
            $nominal = $value['debet'];
        } else if ($value['kredit'] != 0) {
            $nominal = $value['kredit'];
        }

        $pdf->Cell(90, 8, $value['kode_akun'] . ' ' . $value['nama_akun'], 0, 0, 'C');
        $pdf->Cell(90, 8, 'Rp. ' . number_format($nominal, 2, ',', '.'), 0, 1, 'C');
    }

    public function addColumnKas($pdf)
    {
        $pdf->Ln(10);
        $pdf->SetFont('', 'B', 9);
        $pdf->Cell(10, 8, "No", 1, 0, 'C');
        $pdf->Cell(35, 8, "Tanggal", 1, 0, 'C');
        $pdf->Cell(70, 8, "Keterangan", 1, 0, 'C');
        $pdf->Cell(20, 8, "Debet", 1, 0, 'C');
        $pdf->Cell(20, 8, "Kredit", 1, 0, 'C');
        $pdf->Cell(40, 8, "Saldo", 1, 1, 'C');
        $pdf->SetFont('', '', 8);
    }

    private function addRowKasDebet($pdf, $no, $value, $sub_total = null, $saldo = null)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['tanggal'], 1, 0, 'C');
        $pdf->Cell(70, 8, $value['nama_akun'] . ' sebesar Rp. ' . number_format($value['debet'], 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(20, 8, number_format($sub_total, 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(20, 8, "0", 1, 0, 'C');
        $pdf->Cell(40, 8, number_format($saldo, 2, ',', '.'), 1, 1, 'C');
    }

    private function addRowKasKredit($pdf, $no, $value, $sub_total = null, $saldo = null)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['tanggal'], 1, 0, 'C');
        $pdf->Cell(70, 8, $value['nama_akun'] . ' sebesar Rp. ' . number_format($value['kredit'], 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(20, 8, "0", 1, 0, 'C');
        $pdf->Cell(20, 8, number_format($sub_total, 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(40, 8, number_format($saldo, 2, ',', '.'), 1, 1, 'C');
    }

    public function addColumn($pdf)
    {
        $pdf->Ln(10);
        $pdf->SetFont('', 'B', 9);
        $pdf->Cell(10, 8, "No", 1, 0, 'C');
        $pdf->Cell(35, 8, "Tanggal", 1, 0, 'C');
        $pdf->Cell(70, 8, "Keterangan", 1, 0, 'C');
        $pdf->Cell(20, 8, "Debet", 1, 0, 'C');
        $pdf->Cell(20, 8, "Kredit", 1, 0, 'C');
        $pdf->Cell(40, 8, "Saldo", 1, 1, 'C');
        $pdf->SetFont('', '', 8);
    }

    public function addColumnStok($pdf)
    {
        $pdf->Ln(10);
        $pdf->SetFont('', 'B', 9);
        $pdf->Cell(10, 8, "No", 1, 0, 'C');
        $pdf->Cell(35, 8, "Tanggal", 1, 0, 'C');
        $pdf->Cell(70, 8, "Keterangan", 1, 0, 'C');
        $pdf->Cell(20, 8, "Masuk", 1, 0, 'C');
        $pdf->Cell(20, 8, "Keluar", 1, 0, 'C');
        $pdf->Cell(40, 8, "Sub Total", 1, 1, 'C');
        $pdf->SetFont('', '', 8);
    }

    private function addRowDebet($pdf, $no, $value, $sub_total = null, $saldo = null)
    {
        if ($value['diskon'] > 0) {
            $keterangan = 'Penjualan ' . ' ' . $value['nama_barang'] . ' Rp. ' . number_format($value['harga_jual'], 2, ',', '.') . ', berjumlah ' . $value['stok'] . ' (Disc ' . $value['diskon'] . '%)';
        } else {
            $keterangan = 'Penjualan ' . ' ' . $value['nama_barang'] . ' Rp. ' . number_format($value['harga_jual'], 2, ',', '.') . ', berjumlah ' . $value['stok'];
        }
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['tanggal'], 1, 0, 'C');
        $pdf->Cell(70, 8, $keterangan, 1, 0, 'C');
        $pdf->Cell(20, 8, number_format($sub_total, 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(20, 8, "0", 1, 0, 'C');
        $pdf->Cell(40, 8, number_format($saldo, 2, ',', '.'), 1, 1, 'C');
    }


    private function addRowKredit($pdf, $no, $value, $sub_total = null, $saldo = null)
    {
        if ($value['diskon'] > 0) {
            $keterangan = 'Pembelian ' . ' ' . $value['nama_barang'] . ' Rp. ' . number_format($value['harga_beli'], 2, ',', '.') . ', berjumlah ' . $value['stok'] . ' (Disc ' . $value['diskon'] . '%)';
        } else {
            $keterangan = 'Pembelian ' . ' ' . $value['nama_barang'] . ' Rp. ' . number_format($value['harga_beli'], 2, ',', '.') . ', berjumlah ' . $value['stok'];
        }
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['tanggal'], 1, 0, 'C');
        $pdf->Cell(70, 8, $keterangan, 1, 0, 'C');
        $pdf->Cell(20, 8, "0", 1, 0, 'C');
        $pdf->Cell(20, 8, number_format($sub_total, 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(40, 8, number_format($saldo, 2, ',', '.'), 1, 1, 'C');
    }

    private function addRowModalKredit($pdf, $no, $value, $sub_total = null, $saldo = null)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['create_at'], 1, 0, 'C');
        $pdf->Cell(70, 8, "Memasukan Modal Sebesar Rp. " . number_format($value['modal'], 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(20, 8, number_format($sub_total, 2, ',', '.'), 1, 0, 'C');
        $pdf->Cell(20, 8, "0", 1, 0, 'C');
        $pdf->Cell(40, 8, number_format($saldo, 2, ',', '.'), 1, 1, 'C');
    }

    private function addRowStokMasuk($pdf, $no, $value, $sub_total = null, $saldo = null)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['tanggal'], 1, 0, 'C');
        $pdf->Cell(70, 8, 'Stok Masuk ' . ' ' . $value['stok'], 1, 0, 'C');
        $pdf->Cell(20, 8, $sub_total, 1, 0, 'C');
        $pdf->Cell(20, 8, "0", 1, 0, 'C');
        $pdf->Cell(40, 8, $saldo, 1, 1, 'C');
    }

    private function addRowStokKeluar($pdf, $no, $value, $sub_total = null, $saldo = null)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['tanggal'], 1, 0, 'C');
        $pdf->Cell(70, 8, 'Stok Keluar ' . ' ' . $value['stok'], 1, 0, 'C');
        $pdf->Cell(20, 8, "0", 1, 0, 'C');
        $pdf->Cell(20, 8, $sub_total, 1, 0, 'C');
        $pdf->Cell(40, 8, $saldo, 1, 1, 'C');
    }

    private function addRowStokRetur($pdf, $no, $value, $sub_total = null, $saldo = null, $tipe)
    {
        $pdf->Cell(10, 8, $no, 1, 0, 'C');
        $pdf->Cell(35, 8, $value['tanggal'], 1, 0, 'C');
        $pdf->Cell(70, 8, $tipe, 1, 0, 'C');
        if ($tipe == 'Retur Barang Masuk') {
            $pdf->Cell(20, 8, $sub_total, 1, 0, 'C');
            $pdf->Cell(20, 8, "0", 1, 0, 'C');
        } else if ($tipe == 'Retur Barang Keluar') {
            $pdf->Cell(20, 8, "0", 1, 0, 'C');
            $pdf->Cell(20, 8, $sub_total, 1, 0, 'C');
        }
        $pdf->Cell(40, 8, $saldo, 1, 1, 'C');
    }

    public function getLaporan($table, $tanggal, $tanggal2)
    {
        $pdf = new \MYPDF();

        $pdf->AddPage();
        $pdf->setY(25);
        $pdf->SetAutoPageBreak(true, 40);
        $pdf->SetMargins('10', '30', '10', false);
        $pdf->SetTitle('Laporan Keuangan');

        if ($table == 'buku_besar') {
            $modal = $this->mod->getMultipleModal('modal', $tanggal, $tanggal2)->result_array();
            $pembelian = $this->mod->getLaporan('barang_masuk', $tanggal, $tanggal2)->result_array();
            $penjualan = $this->mod->getLaporan('barang_keluar', $tanggal, $tanggal2)->result_array();
            $retur = $this->mod->getLaporan('retur', $tanggal, $tanggal2)->result_array();
            $kas_masuk = $this->mod->get_join('kas_masuk', 'tanggal BETWEEN "' . $tanggal . '" AND "' . $tanggal2 . '"',  'kas_masuk.tanggal ASC, kas_masuk.id_kas_masuk ASC', 'kategori_akun', 'kas_masuk.kode_akun = kategori_akun.kode_akun')->result_array();
            $kas_keluar = $this->mod->get_join('kas_keluar', 'tanggal BETWEEN "' . $tanggal . '" AND "' . $tanggal2 . '"',  'kas_keluar.tanggal ASC, kas_keluar.id_kas_keluar ASC', 'kategori_akun', 'kas_keluar.kode_akun = kategori_akun.kode_akun')->result_array();

            $saldo_modal  = 0;
            $pdf->setFont('', 'B', 12);
            $pdf->Cell(40, 8, 'Jenis: Modal', 0, 0, 'L');
            $this->addColumn($pdf);
            foreach ($modal as $k => $value) {
                $sub_total = $value['modal'];
                $saldo_modal += $sub_total;
                $this->addRowModalKredit($pdf, $k + 1, $value, $sub_total, $saldo_modal);
            }
            $pdf->Cell(155, 8, 'Total Modal', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_modal, 2, ',', '.'), 1, 1, 'C');

            $pdf->addPage();
            $saldo_pembelian  = 0;
            $pdf->setFont('', 'B', 12);
            $pdf->Cell(40, 8, 'Jenis: Pembelian', 0, 0, 'L');
            $this->addColumn($pdf);
            foreach ($pembelian as $k => $value) {
                $sub_total = ($value['harga_beli'] - ($value['harga_beli'] * ($value['diskon'] / 100))) * $value['stok'];
                $saldo_pembelian += $sub_total;
                $this->addRowKredit($pdf, $k + 1, $value, $sub_total, $saldo_pembelian);
            }
            $pdf->Cell(155, 8, 'Total Pembelian', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_pembelian, 2, ',', '.'), 1, 1, 'C');

            $pdf->addPage();
            $saldo_penjualan  = 0;
            $pdf->setFont('', 'B', 12);
            $pdf->Cell(40, 8, 'Jenis: Penjualan', 0, 0, 'L');
            $this->addColumn($pdf);
            foreach ($penjualan as $k => $value) {
                $sub_total = ($value['harga_jual'] - ($value['harga_jual'] * ($value['diskon'] / 100))) * $value['stok'];
                $saldo_penjualan += $sub_total;
                $this->addRowDebet($pdf, $k + 1, $value, $sub_total, $saldo_penjualan);
            }
            $pdf->Cell(155, 8, 'Total Penjualan', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_penjualan, 2, ',', '.'), 1, 1, 'C');

            $pdf->addPage();
            $saldo_kas_masuk  = 0;
            $pdf->setFont('', 'B', 12);
            $pdf->Cell(40, 8, 'Jenis: Kas Masuk', 0, 0, 'L');
            $this->addColumnKas($pdf);
            foreach ($kas_masuk as $k => $value) {
                $sub_total = $value['debet'];
                $saldo_kas_masuk += $sub_total;
                $this->AddRowKasDebet($pdf, $k + 1, $value, $sub_total, $saldo_kas_masuk);
            }
            $pdf->Cell(155, 8, 'Total Debet', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_kas_masuk, 2, ',', '.'), 1, 1, 'C');

            $pdf->addPage();
            $saldo_kas_keluar  = 0;
            $pdf->setFont('', 'B', 12);
            $pdf->Cell(40, 8, 'Jenis: Kas Keluar', 0, 0, 'L');
            $this->addColumnKas($pdf);
            foreach ($kas_keluar as $k => $value) {
                $sub_total = $value['kredit'];
                $saldo_kas_keluar += $sub_total;
                $this->AddRowKasKredit($pdf, $k + 1, $value, $sub_total, $saldo_kas_keluar);
            }
            $pdf->Cell(155, 8, 'Total Kredit', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_kas_keluar, 2, ',', '.'), 1, 1, 'C');

            $pdf->addPage();
            $stok_masuk  = 0;
            $pdf->setFont('', 'B', 12);
            $pdf->Cell(40, 8, 'Jenis: Stok Masuk', 0, 0, 'L');
            $this->addColumnStok($pdf);
            foreach ($pembelian as $k => $value) {
                $sub_total = $value['stok'];
                $stok_masuk += $sub_total;
                $this->addRowStokMasuk($pdf, $k + 1, $value, $sub_total, $stok_masuk);
            }
            $pdf->Cell(155, 8, 'Total Stok Masuk', 1, 0, 'C');
            $pdf->Cell(40, 8, $stok_masuk, 1, 1, 'C');

            $pdf->addPage();
            $stok_keluar  = 0;
            $pdf->setFont('', 'B', 12);
            $pdf->Cell(40, 8, 'Jenis: Stok Keluar', 0, 0, 'L');
            $this->addColumnStok($pdf);
            foreach ($penjualan as $k => $value) {
                $sub_total = $value['stok'];
                $stok_keluar += $sub_total;
                $this->addRowStokKeluar($pdf, $k + 1, $value, $sub_total, $stok_keluar);
            }
            $pdf->Cell(155, 8, 'Total Stok Keluar', 1, 0, 'C');
            $pdf->Cell(40, 8, $stok_keluar, 1, 1, 'C');

            $pdf->addPage();
            $stok_retur  = 0;
            $pdf->setFont('', 'B', 12);
            $pdf->Cell(40, 8, 'Jenis: Retur', 0, 0, 'L');
            $this->addColumnStok($pdf);
            foreach ($retur as $k => $value) {
                $sub_total = $value['stok'];
                if ($value['tipe_retur'] == 'pembelian') {
                    $stok_retur -= $sub_total;
                    $tipe = "Retur Barang Masuk";
                } elseif ($value['tipe_retur'] == 'penjualan') {
                    $tipe = "Retur Barang Keluar";
                }
                $this->addRowStokRetur($pdf, $k + 1, $value, $sub_total, $stok_retur, $tipe);
            }
            $pdf->Cell(155, 8, 'Total Retur', 1, 0, 'C');
            $pdf->Cell(40, 8, $stok_retur, 1, 1, 'C');

            $pdf->addPage();
            $total = ($stok_keluar - $stok_masuk) - $stok_masuk;
            $pdf->SetFont('', 'B', 11);
            $pdf->Cell(155, 8, 'Total Stok Masuk', 1, 0, 'C');
            $pdf->Cell(40, 8, $stok_masuk, 1, 1, 'C');
            $pdf->Cell(155, 8, 'Total Stok Keluar', 1, 0, 'C');
            $pdf->Cell(40, 8, $stok_keluar, 1, 1, 'C');
            $pdf->Cell(155, 8, 'Total Stok Retur', 1, 0, 'C');
            $pdf->Cell(40, 8, $stok_retur, 1, 1, 'C');

            $pdf->addPage();
            $total = ($saldo_penjualan + $saldo_kas_masuk) - ($saldo_kas_keluar + $saldo_pembelian) - $saldo_modal;
            $pdf->SetFont('', 'B', 11);
            $pdf->Cell(155, 8, 'Total Modal', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_modal, 2, ',', '.'), 1, 1, 'C');
            $pdf->Cell(155, 8, 'Total Pembelian', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_pembelian, 2, ',', '.'), 1, 1, 'C');
            $pdf->Cell(155, 8, 'Total Penjualan', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_penjualan, 2, ',', '.'), 1, 1, 'C');
            $pdf->Cell(155, 8, 'Total Kas Masuk', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_kas_masuk, 2, ',', '.'), 1, 1, 'C');
            $pdf->Cell(155, 8, 'Total Kas Keluar', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_kas_keluar, 2, ',', '.'), 1, 1, 'C');
            $pdf->Cell(155, 8, 'Total Laba-Rugi', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($total, 2, ',', '.'), 1, 1, 'C');
        } else if ($table == 'neraca_saldo') {
            $kas_masuk = $this->mod->get_join('kas_masuk', 'tanggal BETWEEN "' . $tanggal . '" AND "' . $tanggal2 . '"',  'kas_masuk.tanggal ASC, kas_masuk.id_kas_masuk ASC', 'kategori_akun', 'kas_masuk.kode_akun = kategori_akun.kode_akun')->result_array();
            $kas_keluar = $this->mod->get_join('kas_keluar', 'tanggal BETWEEN "' . $tanggal . '" AND "' . $tanggal2 . '"',  'kas_keluar.tanggal ASC, kas_keluar.id_kas_keluar ASC', 'kategori_akun', 'kas_keluar.kode_akun = kategori_akun.kode_akun')->result_array();
            // column
            $this->addColumnNeracaSaldo($pdf);

            //row
            $saldo_kas_masuk  = 0;
            foreach ($kas_masuk as $k => $value) {
                $sub_total = $value['debet'];
                $saldo_kas_masuk += $sub_total;
                $this->AddRowKasNeracaSaldo($pdf, $k + 1, $value);
            }

            $saldo_kas_keluar  = 0;
            foreach ($kas_keluar as $k => $value) {
                $sub_total = $value['kredit'];
                $saldo_kas_keluar += $sub_total;
                $this->AddRowKasNeracaSaldo($pdf, $k + 1, $value);
            }

            $pdf->Cell(115, 8, 'Total ', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_kas_masuk, 2, ',', '.'), 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($saldo_kas_keluar, 2, ',', '.'), 1, 1, 'C');

            $total = $saldo_kas_masuk - $saldo_kas_keluar;
            $pdf->Cell(190, 8, 'Total Selisih Balance Rp. '  . number_format($total, 2, ',', '.'), 0, 1, 'C');
        } else if ($table == 'laba_rugi') {
            $pembelian = $this->mod->sum('barang_masuk', '(harga_beli - (harga_beli * (diskon / 100))) * stok', 'tanggal BETWEEN "' . $tanggal . '" AND "' . $tanggal2 . '"');
            $penjualan = $this->mod->sum('barang_keluar', '(harga_jual - (harga_jual * (diskon / 100))) * stok', 'tanggal BETWEEN "' . $tanggal . '" AND "' . $tanggal2 . '"');
            // print $this->unit->run($modal, 'is_array', 'tes tampil data modal untuk laporan bukubesar');

            // column
            $pdf->Ln(10);
            $pdf->SetFont('', 'B', 9);
            $pdf->Cell(25, 8, "No", 1, 0, 'C');
            $pdf->Cell(70, 8, "Keterangan", 1, 0, 'C');
            $pdf->Cell(40, 8, "Debet", 1, 0, 'C');
            $pdf->Cell(40, 8, "Kredit", 1, 1, 'C');
            $pdf->SetFont('', '', 8);
            //row
            $total = $penjualan['total'] - $pembelian['total'];
            $pdf->Cell(25, 8, "1", 1, 0, 'C');
            $pdf->Cell(70, 8, "Pembelian", 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. 0', 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($pembelian['total'], 2, ',', '.'), 1, 1, 'C');

            $pdf->Cell(25, 8, "2", 1, 0, 'C');
            $pdf->Cell(70, 8, "Penjualan", 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($penjualan['total'], 2, ',', '.'), 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. 0', 1, 1, 'C');

            $pdf->Cell(95, 8, "Total", 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($penjualan['total'], 2, ',', '.'), 1, 0, 'C');
            $pdf->Cell(40, 8, 'Rp. '  . number_format($pembelian['total'], 2, ',', '.'), 1, 1, 'C');
            $pdf->Cell(175, 8, 'Total Selisih Balance Rp. '  . number_format($total, 2, ',', '.'), 0, 1, 'C');
        }


        $tanggal = date('Y-m-d');
        $pdf->Output('Laporan  - ' . $tanggal . '.pdf');
    }

    public function getLaporanNeraca($table, $tanggal)
    {
        $pdf = new \MYPDF();

        $pdf->AddPage();
        $pdf->setY(25);
        $pdf->SetAutoPageBreak(true, 40);
        $pdf->SetMargins('10', '30', '10', false);
        $pdf->SetTitle('Laporan Keuangan');

        $kas_masuk = $this->mod->get_join('kas_masuk', 'tanggal <=  "' . $tanggal . '"', 'kas_masuk.tanggal ASC, kas_masuk.id_kas_masuk ASC', 'kategori_akun', 'kas_masuk.kode_akun = kategori_akun.kode_akun')->result_array();
        $kas_keluar = $this->mod->get_join('kas_keluar', 'tanggal <=  "' . $tanggal . '"', 'kas_keluar.tanggal ASC, kas_keluar.id_kas_keluar ASC', 'kategori_akun', 'kas_keluar.kode_akun = kategori_akun.kode_akun')->result_array();
        // print $this->unit->run($pembelian, 'is_array', 'tes tampil data pembelian untuk laporan laba rugi ');
        // print $this->unit->run($pembelian, 'is_array', 'tes tampil data pembelian untuk laporan laba rugi ');
        // print $this->unit->run($retur, 'is_array', 'tes tampil data retur barang untuk laporan bukubesar');
        // print $this->unit->run($kas_masuk, 'is_array', 'tes tampil data kas masuk untuk laporan neraca saldo');
        // print $this->unit->run($kas_keluar, 'is_array', 'tes tampil data kas keluar untuk laporan neraca saldo');
        // die();
        //row
        $pdf->SetFont('', 'B', 11);
        $pdf->Cell(40, 8, 'Aktiva ', 0, 1, 'C');
        $pdf->Cell(90, 8, 'Jenis Akun', 0, 0, 'C');
        $pdf->Cell(90, 8, 'Nominal', 0, 1, 'C');
        $pdf->SetFont('', '', 9);
        $saldo_kas_masuk  = 0;
        foreach ($kas_masuk as $k => $value) {
            $sub_total = $value['debet'];
            $saldo_kas_masuk += $sub_total;
            $this->AddRowKasNeraca($pdf, $value);
        }

        $pdf->SetFont('', 'B', 11);
        $pdf->Cell(40, 8, 'Pasiva ', 0, 1, 'C');
        $pdf->Cell(90, 8, 'Jenis Akun', 0, 0, 'C');
        $pdf->Cell(90, 8, 'Nominal', 0, 1, 'C');
        $pdf->SetFont('', '', 9);
        $saldo_kas_keluar  = 0;
        foreach ($kas_keluar as $k => $value) {
            $sub_total = $value['kredit'];
            $saldo_kas_keluar += $sub_total;
            $this->AddRowKasNeraca($pdf, $value);
        }

        $total = $saldo_kas_masuk - $saldo_kas_keluar;

        $pdf->SetFont('', 'B', 11);
        $pdf->Cell(180, 8, 'Total Selisih Balance Rp. '  . number_format($total, 2, ',', '.'), 0, 1, 'C');

        $tanggal = date('Y-m-d');
        $pdf->Output('Laporan  - ' . $tanggal . '.pdf');
    }
}

/* End of file Tpenjualan.php */
