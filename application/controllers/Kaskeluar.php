<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kaskeluar extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        MY_Controller::is_logged_in();
    }

    public function getModal()
    {
        $get = $this->mod->getModalByNow();

        if ($get == null || $get == '') {
            $data['modal'] = 'Rp.' . number_format(0);
        } else {
            $data['modal'] = 'Rp.' . number_format($get->modal);
        }

        print json_encode($data);
    }

    public function class_data()
    {
        # code...
        $data['listData'] = 'data-list" data-link="' . base_url() . "kaskeluar/tampil" . '';
        $data['buttonSave'] = '<button class="btn generatecode btn-info btn-save" type="submit" data-link="' . base_url() . "kaskeluar/store" . '" data-method="tambah" value="Simpan">Simpan</button> ';
        $data['buttonRestart'] = '<input class="btn btn-success" type="reset" value="Reset">';
        $data['buttonUpdate'] = '<button class="btn generatecode btn-info btn-update" type="submit" data-link="' . base_url() . "kaskeluar/update" . '" data-aksi="update" value="Simpan">Simpan</button> ';
        return $data;
    }


    public function index()
    {
        $data = Self::class_data();

        $data['folder'] = 'kaskeluar';
        $data['file']    = 'view';
        $data['page']     = 'kaskeluar';
        $data['title'] = 'Kas Keluar';
        $data['subtitle'] = '';
        $data['kode_akun'] = $this->mod->get_select('kategori_akun', '*', 'jenis = "KREDIT"')->result_array();

        $check = $this->mod->getModalByNow();

        if ($check != null || $check != '') {
            # code...
            $data['modals'] = $check;
            $this->template->layouts($data);
        } else {
            # code...
            $data['modals'] = $check;
            $this->session->set_flashdata('modalMessage', 'input modal');
            $this->template->layouts($data);
        }
    }

    public function tampil()
    {
        # code...
        $dateRange = [
            'tanggal >=' => date("Y-m-d"),
            'tanggal <=' => date("Y-m-d", strtotime("+30 days"))
        ];
        $data['kaskeluar'] = $this->mod->get_join('kas_keluar', $dateRange,  'kas_keluar.tanggal', 'kategori_akun', 'kas_keluar.kode_akun = kategori_akun.kode_akun')->result_array();
        $this->load->view('kaskeluar/tampil', $data);
    }

    public function show($id)
    {
        # code...
        $data = $this->class_data();

        // $id = $this->input->get('id');
        if ($id != '' || $id != null) {
            # code...
            $data['show'] = $this->mod->get_join('kas_keluar', 'id_kas_keluar = ' . $id,  'kas_keluar.tanggal', 'kategori_akun', 'kas_keluar.kode_akun = kategori_akun.kode_akun')->result_array();

            if ($data['show'] > 0) {
                # code...
                $data['title']    = 'Detail Kas';
                $data['body'] = '
				<form class="form-update">
                <input type="hidden" name="id" value="' . $data['show'][0]['id_kas_keluar'] . '">
                            <div class="form-group">
                                <label>Jenis Akun</label>
                                <select name="kode_akun" class="form-control select2" disabled>
                                    <option value="' . $data['show'][0]['kode_akun'] . '" selected >' . $data['show'][0]['nama_akun'] . '</option>
                                </select>
                            </div>
                            <input type="hidden" name="kode_akun" value="' . $data['show'][0]['kode_akun'] . '">
							<div class="form-group">
                                <label for="keterangan">Keterangan</label>
                                <textarea class=" riset form-control" name="keterangan">' . $data['show'][0]['keterangan'] . '</textarea>
							</div>
							<div class="form-group">
								<label for="kredit">Debet</label>
								<input type="text" class=" form-control change" value="' . $data['show'][0]['kredit'] . '" name="kredit" >
							</div>
							<div class="form-group text-right">
								' . $data['buttonUpdate'] . '
								' . $data['buttonRestart'] . '
							</div>
                </form>
				';
                $data['modal'] = $this->modal->formModal($data['title'], $data['body'], $data['buttonSave'], $data['buttonRestart']);
            }
        }

        print json_encode($data);
    }

    public function store()
    {
        # code...
        $insert_data = [
            'kode_akun' => $this->input->post('kode_akun', true),
            'keterangan' => $this->input->post('keterangan', true),
            'kredit' => $this->input->post('jumlah', true),
            'tanggal' => date('Y-m-d')
        ];

        $insert = $this->mod->insert('kas_keluar', $insert_data);


        if ($insert > 0) {
            $data['msg'] = "berhasil";
        } else {
            $data['msg'] = "gagal";
        }
        print json_encode($data);
    }

    public function update()
    {
        # code...
        $id = $this->input->post('id');

        if ($id != null || $id != '') {
            $update_data = [
                'kode_akun' => $this->input->post('kode_akun'),
                'keterangan' => $this->input->post('keterangan'),
                'kredit' => $this->input->post('kredit'),
            ];

            $update = $this->mod->update('kas_keluar', 'id_kas_keluar', $id, $update_data);

            if ($update > 0) {
                # code...
                $data['msg'] = 'berhasil';
            } else {
                # code...
                $data['msg'] = 'gagal';
            }
        } else {
            # code...
            $data['msg'] = 'gagal insert';
        }

        print json_encode($data);
    }

    public function destroy($id)
    {
        if ($id != '' || $id != null) {
            $this->mod->delete('kas_keluar', 'id_kas_keluar', $id);
            $data['msg'] = "berhasil";
        } else {
            $data['msg'] = "gagal";
        }

        // var_dump($id);
        print json_encode($data);
    }
}

/* End of file User.php */
/* Location: ./application/controllers/User.php */
