<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Satuan extends MY_Controller
{


	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		MY_Controller::is_logged_in();
	}

	public function getModal()
	{
		# code...
		$get = $this->mod->getModalByNow();

		if ($get == null || $get == '') {
			# code...
			$data['modal'] = 'Rp.' . number_format(0);
		} else {
			# code...
			$data['modal'] = 'Rp.' . number_format($get->modal);
		}

		// var_dump(json_encode($get));
		// die();
		print json_encode($data);
	}

	public function class_data()
	{
		# code...
		$data['listData'] = 'data-list" data-link="' . base_url() . "satuan/tampil" . '';
		$data['buttonSave'] = '<button class="btn generatecode btn-info btn-save" type="submit" data-link="' . base_url() . "satuan/store" . '" data-method="tambah" value="Simpan">Simpan</button> ';
		$data['buttonUpdate'] = '<button class="btn generatecode btn-info btn-update" type="submit" data-link="' . base_url() . "satuan/update" . '" data-aksi="update" value="Simpan">Simpan</button> ';
		$data['buttonRestart'] = '<input class="btn btn-success" type="reset" value="Reset">';
		return $data;
	}

	public function index()
	{
		$data = $this->class_data();

		$data['folder'] = 'master/satuan';
		$data['file']	= 'view';
		$data['page'] 	= 'satuan';
		$data['title'] = 'Satuan';
		$data['subtitle'] = '';
		$check = $this->mod->getModalByNow();

		if ($check != null || $check != '' || $this->session->userdata('role') === 'Administrator') {
			# code...
			$data['modals'] = $check;
			$this->template->layouts($data);
		} else {
			# code...
			$data['modals'] = $check;
			$this->session->set_flashdata('modalMessage', 'input modal');
			$this->template->layouts($data);
		}
	}

	public function tampil()
	{
		# code...
		$data['satuan'] = $this->mod->get('satuan')->result_array();
		$this->load->view('master/satuan/list_satuan', $data);

		print json_encode('succeess');
	}

	public function store()
	{
		# code...
		$this->form_validation->set_rules('name', 'Nama Satuan', 'trim|required');


		if ($this->form_validation->run() == TRUE) {
			# code...
			$data = [
				'nama_satuan' => $this->input->post('name', true)
			];
			$insert = $this->mod->insert('satuan', $data);

			if ($insert > 0) {
				# code...
				$data['msg'] = 'berhasil';
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
		}
		print json_encode($data);
	}

	function show($id)
	{
		# code...
		$data = $this->class_data();

		// $id = $this->input->get('id');
		if ($id != '' || $id != null) {
			# code...
			$data['show'] = $this->mod->get('satuan', $id, 'id_satuan')->result_array();

			if ($data['show'] > 0) {
				# code...
				$data['title']	= 'Detail Satuan';
				$data['body'] = '
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					
					<li class="nav-item">
						<a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Update</a>
					</li>
			
				</ul>
				<div class="tab-content" id="myTabContent">
					</div>
					<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					<form class="form-update">
					<input type="hidden" name="id" value="' . $data['show'][0]['id_satuan'] . '">
								<div class="form-group">
									<label for="nama_satuan">Nama Satuan</label>
									<input type="text" class=" form-control change" value="' . $data['show'][0]['nama_satuan'] . '" name="name" >
								</div>
								
								
								<div class="form-group text-right">
									' . $data['buttonUpdate'] . '
									' . $data['buttonRestart'] . '
								</div>
							</form>
					</div>
			
				</div>';
				$data['modal'] = $this->modal->formModal($data['title'], $data['body'], $data['buttonSave'], $data['buttonRestart']);
			}
		}

		// var_dump($data['show']);
		print json_encode($data);
	}

	public function update()
	{
		# code...
		$this->form_validation->set_rules('name', 'Nama satuan', 'trim|required');
		$id = $this->input->post('id');
		if ($id != null || $id != '') {
			# code...
			if ($this->form_validation->run() == TRUE) {
				# code...
				$data = [
					'nama_satuan' => $this->input->post('name', true)
				];
				$update = $this->mod->update('satuan', 'id_satuan', $id, $data);

				if ($update > 0) {
					# code...
					$data['msg'] = 'berhasil';
				}
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
		} else {
			# code...
			$data['msg'] = 'gagal';
		}

		print json_encode($data);
	}


	public function destroy($id)
	{
		# code...
		if ($id != '' || $id != null) {
			# code...
			$delete = $this->mod->delete('satuan', 'id_satuan', $id);
			if ($delete > 0) {
				# code...
				$data['msg'] = 'berhasil';
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
		}
		print json_encode($data);
	}
}

/* End of file Satuan.php */
