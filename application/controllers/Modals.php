<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Modals extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		MY_Controller::is_logged_in();
	}


	public function class_data()
	{
		# code...

		$data['listData'] = 'data-list" data-link="' . base_url() . "modals/tampil" . '';
		$data['buttonRestart'] = '<input class="btn btn-success" type="reset" value="Reset">';
		$data['buttonUpdate'] = '<button class="btn generatecode btn-info btn-update" type="submit" data-link="' . base_url() . "modals/update" . '" data-aksi="update" value="Simpan">Simpan</button> ';
		$data['buttonSave'] = '<button class="btn generatecode btn-info btn-save" type="submit" data-link="' . base_url() . "modal/store" . '" data-method="tambah" value="Simpan">Simpan</button> ';
		return $data;
	}

	public function index()
	{
		$data = $this->class_data();

		$data['folder'] = 'modal';
		$data['file']	= 'view';
		$data['page'] 	= 'modal';
		$data['title'] = 'Modal';
		$data['subtitle'] = '';

		$this->template->layouts($data);
	}

	public function tampil()
	{
		# code...
		$data['modal'] = $this->mod->get('modal')->result_array();

		$this->load->view('modal/list_modal', $data);


		print json_encode('succses');
	}

	public function store()
	{
		# code...


		if ($_REQUEST['modal'] != null) {
			# code...
			$data = [
				'modal' => $_REQUEST['modal']
			];
			$insert = $this->mod->insert('modal', $data);

			if ($insert > 0) {
				# code...
				$msg = ['msg' => 'berhasil'];
			} else {
				# code...
				$msg = ['msg' => 'gagal'];
			}
		} else {
			# code...
			$msg = ['msg' => 'gagal'];
		}


		echo json_encode($msg);
	}

	public function show($id)
	{
		# code...
		$data = $this->class_data();

		// $id = $this->input->get('id');
		if ($id != '' || $id != null) {
			# code...
			$data['show'] = $this->mod->get('modal', $id, 'id_modal')->result_array();

			if ($data['show'] > 0) {
				# code...
				$data['title']	= 'Detail Modal';
				$data['body'] = '
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					
					<li class="nav-item">
						<a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Update</a>
					</li>
			
				</ul>
				<div class="tab-content" id="myTabContent">
					</div>
					<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					<form class="form-update">
					<input type="hidden" name="id" value="' . $data['show'][0]['id_modal'] . '">
								<div class="form-group">
									<label for="Kode_User">Modal</label>
									<input type="number" class="rupiah form-control change" value="' . $data['show'][0]['modal'] . '" name="name" >
								</div>
								
								
								<div class="form-group text-right">
									' . $data['buttonUpdate'] . '
									' . $data['buttonRestart'] . '
								</div>
							</form>
					</div>
			
				</div>';
				$data['modal'] = $this->modal->formModal($data['title'], $data['body'], $data['buttonSave'], $data['buttonRestart']);
			}
		}
		// var_dump($data);
		echo json_encode($data);
	}

	public function update()
	{
		# code...
		$this->form_validation->set_rules('name', 'Modal', 'trim|required');
		$id = $this->input->post('id');

		if ($id != null || $id != '') {
			# code...
			if ($this->form_validation->run() == TRUE) {
				# code...
				$data = [
					'id_modal' => $this->input->post('id'),
					'modal' => $this->input->post('name'),
					'update_at' => date('Y/m/d')
				];
				$update = $this->mod->update('modal', 'id_modal', $id, $data);

				if ($update > 0) {
					# code...
					$data['msg'] = 'berhasil';
				}
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
		} else {
			# code...
			$data['msg'] = 'gagal';
		}

		echo json_encode($data);
	}

	public function destroy($id)
	{
		# code...
	}
}

/* End of file modal.php */
