<?php

defined('BASEPATH') or exit('No direct script access allowed');

class barang extends MY_Controller
{


	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		MY_Controller::is_logged_in();
	}

	public function getModal()
	{
		# code...
		$get = $this->mod->getModalByNow();

		if ($get == null || $get == '') {
			# code...
			$data['modal'] = 'Rp.' . number_format(0);
		} else {
			# code...
			$data['modal'] = 'Rp.' . number_format($get->modal);
		}

		// var_dump(json_encode($get));
		// die();
		print json_encode($data);
	}

	public function class_data()
	{
		# code...

		$data = [
			'db' => 'barang',
			'id' => null,
			'atributwhere' => null,
			'db1' => 'kategori',
			'join1' => 'kategori.id_kategori = barang.id_kategori',
			'db2' => 'satuan',
			'join2' => 'satuan.id_satuan = barang.id_satuan',
		];

		$data['listData'] = 'data-list" data-link="' . base_url() . "barang/tampil" . '';
		$data['buttonSave'] = '<button class="btn generatecode btn-info btn-save" type="submit" data-link="' . base_url() . "barang/store" . '" data-method="tambah" value="Simpan">Simpan</button> ';
		$data['buttonRestart'] = '<input class="btn btn-success" type="reset" value="Reset">';
		$data['buttonLinktoFormAdd'] = '<button class="btn btn-primary klik-menu" data-link="' . base_url() . "barang/add" . '"><i class="fas fa-plus"></i> Tambah Barang</button>';
		$data['buttonUpdate'] = '<button class="btn generatecode btn-info btn-update" type="submit" data-link="' . base_url() . "barang/update" . '" data-aksi="update" value="Simpan">Simpan</button> ';
		return $data;
	}

	public function index()
	{
		$data = $this->class_data(); // -> jangan dirubah

		$data['folder'] = 'master/barang';
		$data['file']	= 'view';
		$data['page'] 	= 'barang';
		$data['title'] = 'Barang';
		$data['subtitle'] = '';
		$check = $this->mod->getModalByNow();

		if ($check != null || $check != '' || $this->session->userdata('role') === 'Administrator') {
			# code...
			$data['modals'] = $check;
			$this->template->layouts($data);
		} else {
			# code...
			$data['modals'] = $check;
			$this->session->set_flashdata('modalMessage', 'input modal');
			$this->template->layouts($data);
		}
	}

	public function tampil()
	{
		# code...
		$data = $this->class_data(); // -> jangan dirubah

		$data['barang'] = $this->mod->get($data['db'], $data['id'], $data['atributwhere'], $data['db1'], $data['join1'], $data['db2'], $data['join2'], 'barang.')->result_array();

		$this->load->view('master/barang/list_barang', $data);
	}

	public function store()
	{
		# code...
		$this->form_validation->set_rules('kd', 'Kode barang / Barcode', 'trim|required');
		$this->form_validation->set_rules('name', 'Nama barang', 'trim|required');
		$this->form_validation->set_rules('hrg_beli', 'Harga beli', 'trim|required');
		$this->form_validation->set_rules('hrg_jual', 'Harga jual', 'trim|required');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required');
		$this->form_validation->set_rules('satuan', 'Satuan', 'trim|required');


		if ($this->form_validation->run() == TRUE) {
			# code...
			$hrgbeli = str_replace('.', '', $this->input->post('hrg_beli', true));
			$hrgjual = str_replace('.', '', $this->input->post('hrg_jual', true));

			$keuntungan = ($hrgjual - $hrgbeli);

			$data = [
				'kode_barang' => $this->input->post('kd', true),
				'nama_barang' => $this->input->post('name', true),
				'harga_beli' => $hrgbeli,
				'harga_jual' => $hrgjual,
				'keuntungan' => $keuntungan,
				'id_kategori' => $this->input->post('kategori', true),
				'id_satuan' => $this->input->post('satuan', true),
				'stok' => 0,
			];
			if ($data['id_kategori'] > 0 || $data['id_satuan'] > 0) {
				# code...
				$insert = $this->mod->insert('barang', $data);

				if ($insert > 0) {
					# code...
					$data['msg'] = 'berhasil';
				} else {
					# code...
					$data['msg'] = 'gagal';
				}
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
		} else {
			# code...
			$data['msg'] = 'gagal';
		}
		print json_encode($data);
	}

	public function add()
	{
		# code...
		$data = $this->class_data(); // -> jangan dirubah

		$data['kategori'] = $this->mod->get('kategori')->result_array();
		$data['satuan'] = $this->mod->get('satuan')->result_array();

		$data['folder'] = 'master/barang';
		$data['file']	= 'form_tambah';
		$data['page'] 	= 'barang';
		$data['title'] = 'Barang';
		$data['subtitle'] = '';
		// $this->load->view('master/barang/form_tambah', $data);
		$this->template->layouts($data);
	}


	public function kategori()
	{
		# code...
	}

	public function show($id)
	{
		# code...
		$data = $this->class_data();

		// $id = $this->input->get('id');
		if ($id != '' || $id != null) {
			# code...
			$data['kategori'] = $this->mod->get('kategori')->result_array();
			$data['satuan'] = $this->mod->get('satuan')->result_array();
			$data['show'] = $this->mod->get($data['db'], $id, 'id_barang', $data['db1'], $data['join1'], $data['db2'], $data['join2'], 'barang.')->result_array();

			if ($data['show'][0]['stok'] < 6) {
				# code...
				$notif = '<span class="badge badge-danger"> hampir habis </span>';
			} else {
				# code...
				$notif = '';
			}
			if ($data['show'] > 0) {
				# code...
				$data['title']	= 'Detail barang';
				foreach ($data['kategori'] as $key => $value) {
					# code...
					$ktg = [];
					if ($data['show'][0]['id_kategori'] ==  $value['id_kategori']) {
						# code...
						$ktg[] = '<option value="' . $value['id_kategori'] . '" selected>' . $value['nama_kategori'] . '</option>';
					} else {
						# code...
						$ktg[] = '<option value="' . $value['id_kategori'] . '">' . $value['nama_kategori'] . '</option>';
					}
					$kategori[] = $ktg;
				}
				$output = [
					'data' => $kategori
				];
				for ($i = 0; $i < count($output['data']); $i++) {
					# code...
					// var_dump($i);
					$kktg = [];
					$kktg = $output['data'][$i][0];
					$outt[] = $kktg;
				}

				foreach ($data['satuan'] as $key => $value) {
					# code...
					$stn = [];
					if ($data['show'][0]['id_satuan'] ==  $value['id_satuan']) {
						# code...
						$stn[] = '<option value="' . $value['id_satuan'] . '" selected>' . $value['nama_satuan'] . '</option>';
					} else {
						# code...
						$stn[] = '<option value="' . $value['id_satuan'] . '">' . $value['nama_satuan'] . '</option>';
					}
					$satuan[] = $stn;
				}
				$output = [
					'satuan' => $satuan
				];
				for ($i = 0; $i < count($output['satuan']); $i++) {
					# code...
					// var_dump($i);
					$stuan = [];
					$stuan = $output['satuan'][$i][0];
					$satuanout[] = $stuan;
				}
				$data['body'] = '
				<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
				<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Detail</a>
				</li>
				<li class="nav-item">
				<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Update</a>
				</li>

				</ul>
				<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
				<table class="table table-striped">

				<tbody>
				<tr>
				<th scope="row">Kode barang <br> (Barcode)</th>
				<td>' . $data['show'][0]['kode_barang'] . '</td>
				</tr>
				<tr>
				<th scope="row">Nama barang</th>
				<td>' . $data['show'][0]['nama_barang'] . '</td>
				</tr>
				<tr>
				<th scope="row">Kategori barang</th>
				<td>' . $data['show'][0]['nama_kategori'] . '</td>
				</tr>
				<tr>
				<th scope="row">Satuan</th>
				<td>' . $data['show'][0]['nama_satuan'] . '</td>
				</tr>
				<tr>
				<th scope="row">Stok</th>
				<td>' . $data['show'][0]['stok'] .  ' ' . $notif . ' </td>
				</tr>
				<tr>
				<th scope="row">Harga beli</th>
				<td>' . $data['show'][0]['harga_beli'] . '</td>
				</tr>
				<tr>
				<th scope="row">Harja jual</th>
				<td>' . $data['show'][0]['harga_jual'] . '</td>
				</tr>
				<tr>
				<th scope="row">Harga jual - harga beli</th>
				<td>' . $data['show'][0]['keuntungan'] . '</td>
				</tr>
				</tbody>
				</table>
				</div>
				<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
				<form class="form-update">
				<input type="hidden" name="id" value="' . $data['show'][0]['id_barang'] . '">
				<div class="form-group">
				<label for="Kode_User">Kode Barang</label>
				<input type="text" class=" form-control change" value="' . $data['show'][0]['kode_barang'] . '" name="kode" disabled>
				</div>

				<div class="form-group">
				<label for="Kode_User">Nama barang</label>
				<input type="text" class=" form-control change" value="' . $data['show'][0]['nama_barang'] . '" name="name"  >
				</div>
				<div class="form-group">
				<label>Kategori</label>
				<select name="kategori" class="form-control select2">


				' . implode($outt) . '
				</select>
				</div>
				<div class="form-group">
				<label>satuan</label>
				<select name="satuan" class="form-control select2">
				' . implode($satuanout) . '

				</select>
				</div>

				<div class="form-group">
				<label>Harga beli</label>
				<div class="input-group">
				<div class="input-group-prepend">
				<div class="input-group-text">
				Rp
				</div>
				</div>
				<input type="text" value="' . $data['show'][0]['harga_beli'] . '"  name="hrg_beli" class="form-control rupiah">
				</div>
				</div>
				<div class="form-group">
				<label>Harga Jual</label>
				<div class="input-group">
				<div class="input-group-prepend">
				<div class="input-group-text">
				Rp
				</div>
				</div>
				<input type="text" value="' . $data['show'][0]['harga_jual'] . '"  name="hrg_jual" class="form-control rupiah">
				</div>
				</div>
				<div class="form-group text-right">
				' . $data['buttonUpdate'] . '
				</div>
				</form>
				</div>

				</div>

				';
				$data['modal'] = $this->modal->formModal($data['title'], $data['body'], $data['buttonUpdate'], $data['buttonRestart']);
			}
		}

		// var_dump($data['body']);
		print json_encode($data);
	}

	public function update()
	{
		# code...
		$this->form_validation->set_rules('name', 'Nama barang', 'trim');
		$id = $this->input->post('id');
		if ($id != null || $id != '') {
			# code...
			if ($this->form_validation->run() == TRUE) {
				# code...

				$keuntungan = ($this->input->post('hrg_jual', true) - $this->input->post('hrg_beli', true));

				$data = [
					'kode_barang' => $this->input->post('kode', true),
					'nama_barang' => $this->input->post('name', true),
					'id_kategori' => $this->input->post('kategori', true),
					'id_satuan' => $this->input->post('satuan', true),
					'stok' => $this->input->post('stok', true),
					'harga_beli' => $this->input->post('hrg_beli', true),
					'harga_jual' => $this->input->post('hrg_jual', true),
					'keuntungan' => $keuntungan,
				];
				$update = $this->mod->update('barang', 'id_barang', $id, $data);

				if ($update > 0) {
					# code...
					$data['msg'] = 'berhasil';
				} else {
					# code...
					$data['msg'] = 'gagal masuk db';
				}
			} else {
				# code...
				$data['msg'] = 'gagal insert';
			}
		} else {
			# code...
			$data['msg'] = 'gagal insert id';
		}

		// var_dump($id);
		print json_encode($data);
	}

	public function destroy($id)
	{
		# code...
		if ($id != '' || $id != null) {
			# code...
			$delete = $this->mod->delete('barang', 'id_barang', $id);
			if ($delete > 0) {
				# code...
				$data['msg'] = 'berhasil';
			} else {
				# code...
				$data['msg'] = 'gagal';
			}
		}
		print json_encode($data);
	}
}

/* End of file barang.php */
