<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Retur extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		MY_Controller::is_logged_in();
	}

	public function getModal()
	{
		# code...
		$get = $this->mod->getModalByNow();

		if ($get == null || $get == '') {
			# code...
			$data['modal'] = 'Rp.' . number_format(0);
		} else {
			# code...
			$data['modal'] = 'Rp.' . number_format($get->modal);
		}

		// var_dump(json_encode($get));
		// die();
		print json_encode($data);
	}

	public function class_data()
	{
		$data['folder'] = 'retur';
		$data['file']	= 'view.php';
		// $data['listData'] = 'data-list" data-link="' . base_url() . "retur/tampilListRetur" . '';
		$data['listSearch'] = 'data-list-search" data-link="' . base_url() . "retur/tampilListRetur" . '';
		$data['listDataHistory'] = 'data-list" data-link="' . base_url() . "retur/tampilHistoryRetur" . '';
		$data['buttonSave'] = '<button class="btn generatecode btn-info btn-save" type="submit" data-link="' . base_url() . "retur/store" . '" data-method="tambah" value="Simpan">Simpan</button> ';
		$data['buttonUpdate'] = '<button class="btn generatecode btn-info btn-update" type="submit" data-link="' . base_url() . "retur/update" . '" data-aksi="update" value="Simpan">Simpan</button> ';
		$data['buttonRestart'] = '<input class="btn btn-success" type="reset" value="Reset">';
		return $data;
	}

	public function index()
	{
		$data = $this->class_data();

		$data['folder'] = 'retur';
		$data['file']	= 'view';
		$data['page'] 	= 'retur';
		$data['title'] = 'Retur';
		$data['subtitle'] = '';

		$check = $this->mod->getModalByNow();

		if ($check != null || $check != '') {
			# code...
			$data['modals'] = $check;
			$this->template->layouts($data);
		} else {
			# code...
			$data['modals'] = $check;
			$this->session->set_flashdata('modalMessage', 'input modal');
			$this->template->layouts($data);
		}
	}

	public function tampilListRetur()
	{
		$tipe = $this->input->post('tipe');
		$data = [];

		if (isset($tipe)) {
			$nota = $this->input->post('id');
			if ($tipe == "pembelian") {
				$table = 'barang_masuk';
				$data['getSearch'] = $this->mod->get_Multiplewhere($table, "nota = '" . $nota . "'")->result_array();
			} else if ($tipe == "penjualan") {
				$table = 'barang_keluar';
				$data['getSearch'] = $this->mod->get_Multiplewhere($table, "nota = '" . $nota . "'")->result_array();
			}

			if (isset($data['getSearch'])) {
				$dataCount = count($data['getSearch']);
				for ($i = 0; $i < $dataCount; $i++) {
					$data['getSearch'][$i]['stok'] = $this->log_stok($table, $data['getSearch'][$i]);
				}
			}
		}
		$this->load->view('retur/tampil', $data);
	}

	public function log_stok($table, $data)
	{
		$stok = 0;
		$log_stok = $this->mod->get_select('log_stok', 'id_' . $table . ', id_retur, stok', 'id_' . $table . ' = ' . $data['id_' . $table])->result_array();
		$dataCount = count($log_stok);
		for ($i = 0; $i < $dataCount; $i++) {
			if ($log_stok[$i]['id_retur'] != null) {
				$stok += $log_stok[$i]['stok'];
			} else {
				$stok = 0;
			}
		}
		return $data['stok'] -= $stok;
	}

	public function history()
	{
		$data = $this->class_data();

		$data['folder'] = 'retur/history';
		$data['file']	= 'view';
		$data['page'] 	= 'retur/history';
		$data['title'] = 'History Retur';
		$data['subtitle'] = '';

		$check = $this->mod->getModalByNow();

		if ($check != null || $check != '') {
			# code...
			$data['modals'] = $check;
			$this->template->layouts($data);
		} else {
			# code...
			$data['modals'] = $check;
			$this->session->set_flashdata('modalMessage', 'input modal');
			$this->template->layouts($data);
		}
	}

	public function tampilHistoryRetur()
	{
		$data['getSearch'] = $this->mod->get('retur')->result_array();
		$this->load->view('retur/history/tampil', $data);
	}

	public function show($table, $id)
	{
		# code...
		$data = $this->class_data();

		// $id = $this->input->get('id');
		if ($id != '' || $id != null) {
			# code...

			if ($table == 'barang_keluar') {
				$data['show'] = $this->mod->get('barang_keluar', $id, 'id_barang_keluar')->result_array();
			} else if ($table == 'barang_masuk') {
				$data['show'] = $this->mod->get('barang_masuk', $id, 'id_barang_masuk')->result_array();
			}

			$nama_supplier = $this->mod->get_select('retur', 'nama_supplier', 'nota = "' . $data['show'][0]['nota'] . '"')->row_array();

			if (isset($nama_supplier)) {
				$nama_supplier['nama_supplier'] = 'value="' . $nama_supplier['nama_supplier'] . '" readonly';
			} else {
				$nama_supplier['nama_supplier'] = '';
			}

			if ($data['show'] > 0) {
				# code...
				$data['stok'] = $this->log_stok($table, $data['show'][0]);
				$data['title']	= 'Detail Barang';
				$data['body'] = '
				<form class="form">
				<input type="hidden" name="id" value="' . $id . '" >
				<input type="hidden" name="nota" >
				<input type="hidden" name="tipe_retur" >
				<input type="hidden" name="stok" value="' . $data['stok'] . '" >
							<div class="form-group">
								<label for="Kode_User">Kode Barang</label>
								<input type="text" class=" form-control change" readonly value="' . $data['show'][0]['kode_barang'] . '" name="kode_barang">
							</div>
							<div class="form-group">
								<label for="Kode_User">Nama Barang</label>
								<input type="text" class=" form-control change" readonly value="' . $data['show'][0]['nama_barang'] . '" name="nama_barang" >
							</div>
							<div class="form-group">
								<label for="Kode_User">Harga Jual</label>
								<input type="text" class=" form-control change" readonly value="' . $data['show'][0]['harga_jual'] . '" name="harga_jual" >
							</div>
							<div class="form-group">
								<label for="Kode_User">Harga Beli</label>
								<input type="text" class=" form-control change" readonly value="' . $data['show'][0]['harga_beli'] . '" name="harga_beli" >
							</div>
							<div class="form-group">
								<label for="Kode_User">Nama Supplier</label>
								<input type="text" class=" form-control change" name="nama_supplier" ' . $nama_supplier['nama_supplier'] . '>
							</div>
							<div class="form-group">
								<label for="Kode_User">Qty</label>
								<input type="text" class=" form-control change" name="qty" >
							</div>
							<div class="form-group">
							<label>Status Retur</label>
							<select name="status_retur" id="status_retur" class="form-control select2">
								<option active hidden selected>Status Retur</option>
								<option value="ya">Yes</option>
								<option value="tidak">Tidak</option>
							</select>
						</div>
							<div class="form-group text-right">
								' . $data['buttonSave'] . '
								' . $data['buttonRestart'] . '
							</div>
						</form>';
				$data['modal'] = $this->modal->formModal($data['title'], $data['body'], $data['buttonSave'], $data['buttonRestart']);
			}
		}

		print json_encode($data);
	}

	public function show_history($id)
	{
		# code...
		$data = $this->class_data();

		// $id = $this->input->get('id');
		if ($id != '' || $id != null) {
			# code...
			$data['show'] = $this->mod->get('retur', $id, 'id_retur')->result_array();

			if ($data['show'] > 0) {
				# code...
				$data['title']	= 'Detail Barang';
				$data['body'] = '
							<input type="hidden" name="nota" value="' . $data['show'][0]['nota'] . '" readonly >
							<input type="hidden" name="tipe_retur" value="' . $data['show'][0]['tipe_retur'] . '" readonly >
							<div class="form-group">
								<label for="Kode_User">Kode Barang</label>
								<input type="text" class=" form-control change" readonly value="' . $data['show'][0]['kode_barang'] . '" name="kode_barang">
							</div>
							<div class="form-group">
								<label for="Kode_User">Nama Barang</label>
								<input type="text" class=" form-control change" readonly value="' . $data['show'][0]['nama_barang'] . '" name="nama_barang" >
							</div>
							<div class="form-group">
								<label for="Kode_User">Harga Jual</label>
								<input type="text" class=" form-control change" readonly value="' . $data['show'][0]['harga_jual'] . '" name="harga_jual" >
							</div>
							<div class="form-group">
								<label for="Kode_User">Nama Supplier</label>
								<input type="text" class=" form-control change" name="nama_supplier" value="' . $data['show'][0]['nama_supplier'] . '" readonly>
							</div>
							<div class="form-group">
								<label for="Kode_User">Qty</label>
								<input type="text" class=" form-control change" name="qty" value="' . $data['show'][0]['stok'] . '" readonly>
							</div>
							';
				$data['modal'] = $this->modal->formModal($data['title'], $data['body'], "", "");
			}
		}

		print json_encode($data);
	}

	public function store()
	{
		$status_retur = $this->input->post('status_retur');
		// $status_retur = "ya";

		if ($status_retur == "ya") {
			$id = $this->input->post('id');
			$nota = $this->input->post('nota');
			$tipe_retur = $this->input->post('tipe_retur');
			$nama_supplier = $this->input->post('nama_supplier');
			$qty = $this->input->post('qty');
			$stok = $this->input->post('stok');

			// $tipe_retur = "penjualan";
			// $nota = "N2020200001";
			// $nama_supplier = "Budi";
			// $qty = 1;
			$sisaStok = $stok - $qty;
			if ($sisaStok >= 0) {
				if ($tipe_retur == "pembelian") {
					$namaTable = 'Barang Masuk';
					$table = 'barang_masuk';
					$getBarang = $this->mod->get($table, $nota, 'nota')->row_array();
				} else if ($tipe_retur == "penjualan") {
					$namaTable = 'Barang Keluar';
					$table = 'barang_keluar';
					$getBarang = $this->mod->get($table, $nota, 'nota')->row_array();
				}


				$sub_total = ($getBarang['harga_jual'] - ($getBarang['harga_jual'] * $getBarang['diskon'] / 100)) * $qty;
				$insertData = [
					'nota' => $nota,
					'kode_barang' => $getBarang['kode_barang'],
					'nama_barang' => $getBarang['nama_barang'],
					'stok' => $qty,
					'harga_jual' => $getBarang['harga_jual'],
					'diskon' => $getBarang['diskon'],
					'sub_total' => $sub_total,
					'tanggal' => $getBarang['tanggal'],
					'nama_supplier' => $nama_supplier,
					'tipe_retur' => $tipe_retur,
					'create_at' => date('Y-m-d')
				];

				$insert = $this->mod->insert('retur', $insertData);



				$last_id = $this->mod->insert_id();

				$insertLog = [
					'kode_barang' => $getBarang['kode_barang'],
					'id_' . $table => $id,
					'id_retur' => $last_id,
					'tanggal' => date('Y-m-d'),
					'stok' => $qty,
					'keterangan' => 'Retur ' . $namaTable,
				];

				$insertLog = $this->mod->insert('log_stok', $insertLog);



				if ($insert > 0 && $insertLog > 0) {
					$data['msg'] = 'berhasil';
					$data['msgvalue'] = 'Data Retur Berhasil Ditambahkan';
				} else {
					$data['msg'] = 'gagal';
					$data['msgvalue'] = 'Data Retur Gagal Ditambahkan';
				}
			} else {
				$data['msg'] = 'gagal';
				$data['msgvalue'] = 'Qty melebihi jumlah pembelian asli';
			}
		}

		echo json_encode($data);
	}
}

/* End of file Retur.php */
