<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends MY_Controller

{

	public function
	default()
	{
		# code...
		$data['title'] 		= 	'Login - Sikopma';
		$data['subtitle']	=	null;

		return $data;
	}

	public function index()
	{
		$data = Self::default();
		if ($this->session->userdata('role') == null) {

			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run() == TRUE) {

				$data = [
					'username' => $this->input->post('username'),
					'password' => $this->input->post('password')
				];

				$this->load->view('auth/login', $data);
				$this->_validation($data);
			} else {

				$this->session->set_flashdata('error_msg', validation_errors());
				$this->load->view('auth/login', $data);
			}
		} else {

			switch ($this->session->userdata('role')) {
				case 'Administrator':

					redirect(base_url() . "dashboard");
					break;
				case 'User':

					redirect(base_url() . "tpenjualan");
					break;
				default:
					# code...
					break;
			}
		}
	}

	public function _validation($data)
	{
		$test = false;

		$username = $data['username'];
		$password = $data['password'];
		$checkRows = $this->db->get_where('user', ['username' => $username], 1)->row_array();

		if ($checkRows) {

			if (password_verify($password, $checkRows['password'])) {

				$test = true;

				$newdata = [
					'kd_user' => $checkRows['kd_user'],
					'nama' => $checkRows['nama_user'],
					'username' => $checkRows['username'],
					'role' => $checkRows['group_user'],
				];
				$this->session->set_userdata($newdata);


				switch ($checkRows['group_user']) {
					case 'Administrator':

						$this->session->set_flashdata('message', 'login');
						redirect(base_url() . "dashboard");
						break;
					case 'User':
						$check = $this->mod->getModalByNow();

						if ($check != null || $check != '') {

							$this->session->set_flashdata('message', 'login');
							redirect(base_url() . "tpenjualan");
						} else {

							$this->session->set_flashdata('modalMessage', 'input modal');
							$this->session->set_flashdata('message', 'login');

							redirect(base_url() . "tpenjualan");
						}

						break;
					default:
						# code...

						break;
				}
			} else {
				# code...
				$this->session->set_flashdata('message', 're-login');
				redirect(base_url());
			}
		} else {
			# code...
			$this->session->set_flashdata('message', 're-login');
			redirect(base_url());
		}

		// print $this->unit->run($test, true, 'tes Login');
	}


	public function logout()
	{
		# code...
		$this->session->set_flashdata('message', 'logout');
		$this->session->sess_destroy();
		redirect(base_url());
	}
}

/* End of file Auth.php */
