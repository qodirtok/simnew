<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stokbarang extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		MY_Controller::is_logged_in();
	}
	public function getModal()
	{
		# code...
		$get = $this->mod->getModalByNow();

		if ($get == null || $get == '') {
			# code...
			$data['modal'] = 'Rp.' . number_format(0);
		} else {
			# code...
			$data['modal'] = 'Rp.' . number_format($get->modal);
		}

		// var_dump(json_encode($get));
		// die();
		print json_encode($data);
	}

	public function class_data()
	{
		# code...
		$data = [
			'db' => 'barang',
			'id' => null,
			'atributwhere' => null,
			'db1' => 'kategori',
			'join1' => 'kategori.id_kategori = barang.id_kategori',
			'db2' => 'satuan',
			'join2' => 'satuan.id_satuan = barang.id_satuan',
		];
		$data['listData'] = 'data-list" data-link="' . base_url() . "stokbarang/tampil" . '';
		$data['buttonSave'] = '<button class="btn generatecode btn-info btn-save" type="submit" data-link="' . base_url() . "stokbarang/store" . '" data-method="tambah" value="Simpan">Simpan</button> ';
		$data['buttonRestart'] = '<input class="btn btn-success" type="reset" value="Reset">';
		$data['buttonLinktoFormAdd'] = '<button class="btn btn-primary klik-menu" data-link="' . base_url() . "stokpembelianbarang" . '"><i class="fas fa-plus"></i> Stok Barang</button>';
		$data['buttonUpdate'] = '<button class="btn generatecode btn-info btn-update" type="submit" data-link="' . base_url() . "stokbarang/update" . '" data-aksi="update" value="Simpan">Simpan</button> ';
		return $data;
	}

	public function index()
	{
		$data = $this->class_data();

		$data['folder'] = 'stokbarang';
		$data['file']	= 'view';
		$data['page'] 	= 'stokbarang';
		$data['title'] = 'Stok barang';
		$data['subtitle'] = '';
		$check = $this->mod->getModalByNow();

		if ($check != null || $check != '') {
			# code...
			$data['modals'] = $check;
			$this->template->layouts($data);
		} else {
			# code...
			$data['modals'] = $check;
			$this->session->set_flashdata('modalMessage', 'input modal');
			$this->template->layouts($data);
		}
	}

	public function tampil()
	{
		# code...
		$data = $this->class_data();
		$data['barang'] = $this->mod->get($data['db'], $data['id'], $data['atributwhere'], $data['db1'], $data['join1'], $data['db2'], $data['join2'])->result_array();
		$this->load->view('stokbarang/list_barang', $data);
	}

	public function show($id)
	{
		# code...
		$data = $this->class_data();

		if ($id != '' || $id != null) {
			# code...
			$data['kategori'] = $this->mod->get('kategori')->result_array();
			$data['satuan'] = $this->mod->get('satuan')->result_array();
			$data['show'] = $this->mod->get($data['db'], $id, 'id_barang', $data['db1'], $data['join1'], $data['db2'], $data['join2'])->result_array();

			if ($data['show'][0]['stok'] < 6) {
				# code...
				$notif = '<span class="badge badge-danger"> hampir habis </span>';
			} else {
				# code...
				$notif = '';
			}
			if ($data['show'] > 0) {
				# code...
				$data['title']	= 'Detail barang';
				$data['body'] = '
				<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
				<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Detail</a>
				</li>


				</ul>
				<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
				<table class="table table-striped">

				<tbody>
				<tr>
				<th scope="row">Kode barang <br> (Barcode)</th>
				<td>' . $data['show'][0]['kode_barang'] . '</td>
				</tr>
				<tr>
				<th scope="row">Nama barang</th>
				<td>' . $data['show'][0]['nama_barang'] . '</td>
				</tr>
				<tr>
				<th scope="row">Kategori barang</th>
				<td>' . $data['show'][0]['nama_kategori'] . '</td>
				</tr>
				<tr>
				<th scope="row">Satuan</th>
				<td>' . $data['show'][0]['nama_satuan'] . '</td>
				</tr>
				<tr>
				<th scope="row">Stok</th>
				<td>' . $data['show'][0]['stok'] .  ' ' . $notif . ' </td>
				</tr>
				<tr>
				<th scope="row">Harga beli</th>
				<td>' . $data['show'][0]['harga_beli'] . '</td>
				</tr>
				<tr>
				<th scope="row">Harja jual</th>
				<td>' . $data['show'][0]['harga_jual'] . '</td>
				</tr>
				<tr>
				<th scope="row">Harga jual - harga beli</th>
				<td>' . $data['show'][0]['keuntungan'] . '</td>
				</tr>
				</tbody>
				</table>
				</div>


				</div>

				';
				$data['modal'] = $this->modal->formModal($data['title'], $data['body'], $data['buttonUpdate'], $data['buttonRestart']);
			}
		}

		print json_encode($data);
	}

	public function search()
	{
		# code...
		// $request = $
	}

	public function show_cart()
	{
		# code...
		$data = '';
		$no = 0;
		foreach ($this->cart->contents() as $value) {
			# code...
			$no++;
			$data .= $value['nota'];
		}
		$data .= $this->cart->total();

		return $data;
	}

	public function add()
	{
		# code...
		$data['file'] = 'tambah_stok';
		$data['folder'] = 'stokbarang';
		$data['page'] = 'stokbarang';
		$data['title'] = '';
		$this->template->layouts($data);
	}

	public function update()
	{
		# code...
		$id = $this->input->post('input');
	}
}

/* End of file Stokbarang.php */
