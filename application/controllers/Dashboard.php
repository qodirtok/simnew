<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		MY_Controller::is_logged_in();
	}


	// public function _data()
	// {
	// 	# code...
	// 	$data = MY_Controller::default_data(MY_Controller::class_data('Dashboard', 'view_1'), 'Dashboard Umum', null);

	// 	return $data;
	// }

	public function class_data()
	{
		# code...
	}

	public function countAll()
	{
		# code...
		$data['user'] = $this->mod->count_all('user');
		$data['barang'] = $this->mod->count_all('barang');
		$data['stok_barang'] = $this->mod->count_stok('barang', 'sum(stok) as total');
		$data['kategori'] = $this->mod->count_all('kategori');
		$data['satuan'] = $this->mod->count_all('satuan');
		$data['surat'] = $this->mod->count_all('surat');
		$data['surat_masuk'] = $this->mod->count_Surat("'masuk'");
		$data['surat_keluar'] = $this->mod->count_Surat("'keluar'");
		$data['modal'] = $this->mod->getModalByNow();
		// var_dump($data);
		// die;
		return $data;
	}

	public function index()
	{
		# code...
		$data = Self::class_data();
		$data = $this->countAll();
		MY_Controller::checkAdmin();
		$data['folder'] = 'dashboard';
		$data['file']	= 'view_1.php';
		$data['page'] 	= 'dashboard umum';
		$data['title'] = 'Dashboard';
		$data['subtitle'] = '';

		$this->template->layouts($data);
	}

	public function dashboardpenjualan()
	{

		$data = Self::class_data();
		$data = $this->countAll();

		$data['file']	= 'view_2';
		$data['folder'] = 'dashboard';
		$data['title'] 	= 'Dashboard Penjualan';
		$data['page'] 	= 'Dashboard Penjualan';
		$data['chart'] = 'data-list" data-link="' . base_url() . "dashboard/chart" . '';
		$data['chart_bulanan'] = 'data-list-bulanan" data-link="' . base_url() . "dashboard/chartBulanan" . '';

		$pembelian = $this->mod->sum('barang_masuk', '(harga_beli - (harga_beli * (diskon / 100))) * stok', 'tanggal = "' . date('Y-m-d') . '"');

		$penjualan = $this->mod->sum('barang_keluar', '(harga_jual - (harga_jual * (diskon / 100))) * stok', 'tanggal = "' . date('Y-m-d') . '"');

		$penjualan_untung = $this->mod->sum('barang_keluar', '(harga_beli - (harga_beli * (diskon / 100))) * stok', 'tanggal = "' . date('Y-m-d') . '"');

		if (!isset($pembelian)) {
			$pembelian = 0;
		}

		if (!isset($penjualan)) {
			$penjualan = 0;
		}

		if (!isset($penjualan_untung)) {
			$penjualan_untung = 0;
		}

		$kas_masuk = $this->mod->get_join('kas_masuk', 'tanggal =  "' .  date('Y-m-d') . '"', 'kas_masuk.tanggal ASC, kas_masuk.id_kas_masuk ASC', 'kategori_akun', 'kas_masuk.kode_akun = kategori_akun.kode_akun')->result_array();
		$data['kas_masuk'] = 0;
		foreach ($kas_masuk as $k => $output) {
			$nominal = $output['debet'];
			$data['kas_masuk'] += $nominal;
		}

		$kas_keluar = $this->mod->get_join('kas_keluar', 'tanggal =  "' .  date('Y-m-d') . '"', 'kas_keluar.tanggal ASC, kas_keluar.id_kas_keluar ASC', 'kategori_akun', 'kas_keluar.kode_akun = kategori_akun.kode_akun')->result_array();
		$data['kas_keluar'] = 0;
		foreach ($kas_keluar as $k => $output) {
			$nominal = $output['kredit'];
			$data['kas_keluar'] += $nominal;
		}

		$data['pengeluaran'] = intval($pembelian['total']) + $data['kas_keluar'];
		$data['pendapatan'] = intval($penjualan['total']) + $data['kas_masuk'];
		$data['keuntungan'] = intval($penjualan['total']) - intval($penjualan_untung['total']);
		// $data['keuntungan'] = ($data['saldo_jual'] - $data['saldo_jualbeli']) - $data['saldo_beli'];
		// print $this->unit->run($data, 'is_array', 'tes tampil data dashboard');
		$this->template->layouts($data);
	}

	// public function chart()
	// {
	// 	$db = [
	// 		'order_by' => 'log_stok.id_log ASC, log_stok.tanggal ASC',
	// 		'select' => 'barang.nama_barang, barang_keluar.harga_jual, barang_masuk.harga_beli, barang_keluar.diskon as diskon_jual, barang_masuk.diskon as diskon_beli, log_stok.keterangan, barang_keluar.tanggal as tanggal_jual, barang_masuk.tanggal as tanggal_beli, log_stok.stok',
	// 		'db' => 'barang',
	// 		'db1' => 'barang_keluar',
	// 		'join1' => 'barang_keluar.kode_barang = barang.kode_barang',
	// 		'db2' => 'barang_masuk',
	// 		'join2' => 'barang_masuk.kode_barang = barang.kode_barang',
	// 		'db3' => 'log_stok',
	// 		'join3' => 'log_stok.kode_barang = barang.kode_barang',
	// 	];

	// 	$tanggal_awal = date('Y-m-d');
	// 	$tujuh_hari_lalu = date('Y-m-d', strtotime('-7 days', strtotime($tanggal_awal)));
	// 	for ($i = 1; $i <= 7; $i++) {
	// 		$data['saldo_beli'] = 0;
	// 		$data['saldo_jual'] = 0;

	// 		$dateNow = date('Y-m-d', strtotime('+' . $i . ' days', strtotime($tujuh_hari_lalu)));

	// 		$db['where'] = '(log_stok.tanggal = "' . $dateNow . '") AND (log_stok.keterangan = "Barang Masuk" OR log_stok.keterangan = "Barang Keluar")';
	// 		$getDb = $this->mod->getLaporanJoin($db['db'], $db['select'], $db['where'], $db['order_by'], $db['db1'], $db['join1'], $db['db2'], $db['join2'], $db['db3'], $db['join3'])->result_array();
	// 		foreach ($getDb as $output) {
	// 			if ($output['keterangan'] == 'Barang Masuk') {
	// 				$harga = $output['harga_beli'];
	// 				$diskon = $output['diskon_beli'];
	// 				$sub_total = ($harga - ($harga * ($diskon / 100))) * $output['stok'];
	// 				$data['saldo_beli'] += $sub_total;
	// 			} elseif ($output['keterangan'] == 'Barang Keluar') {
	// 				$harga = $output['harga_jual'];
	// 				$diskon = $output['diskon_jual'];
	// 				$sub_total = ($harga - ($harga * ($diskon / 100))) * $output['stok'];
	// 				$data['saldo_jual'] += $sub_total;
	// 			}
	// 		}
	// 		$data['keuntungan' . $i] = $data['saldo_jual'] - $data['saldo_beli'];
	// 	}

	// 	$this->load->view('dashboard/chart', $data);
	// 	$this->load->view('_partials/07_js_chart', $data);
	// }

	public function chart()
	{
		// $pembelian_harian = $this->mod->sum_week('barang_masuk', '(harga_beli - (harga_beli * (diskon / 100))) * stok', 'tanggal <= DATE_SUB(now(), INTERVAL 7 DAY)')->result_array();

		// $penjualan_harian = $this->mod->sum_week('barang_keluar', '(harga_jual - (harga_jual * (diskon / 100))) * stok', 'tanggal <= DATE_SUB(now(), INTERVAL 7 DAY)')->result_array();

		// $data_count = count($pembelian_harian);

		// for ($i = 0; $i < 7; $i++) {
		// 	for ($j = 1; $j <= 31; $j++) {
		// 		if (isset($pembelian_harian[$i])) {
		// 			if (date('d', strtotime($pembelian_harian[$i]['day'])) == $j) {
		// 				$date = strtotime($pembelian_harian[$i]['day']);
		// 				$data['bulan'][$i] = date('m', $date);
		// 				$data['tahun'][$i] = date('Y', $date);
		// 				$data['hari'][$i] = date('d', strtotime($pembelian_harian[0]['day']));
		// 				$data['keuntungan_hari'][$i] = $penjualan_harian[$i]['total'] - $pembelian_harian[$i]['total'];
		// 			}
		// 		} else {

		// 			$date = strtotime($pembelian_harian[$data_count - 1]['day']);
		// 			$data['bulan'][$i] = date('m', $date);
		// 			$data['tahun'][$i] = date('Y', $date);
		// 			$data['hari'][$i] = date('d', $date) - $i;
		// 			$data['keuntungan_hari'][$i] = 0;
		// 		}
		// 	}
		// }

		// for ($i = 0; $i < 7; $i++) {
		// 	for ($j = 0; $j < 7; $j++) {
		// 		if ($data['hari'][$i] < $data['hari'][$j]) {
		// 			$tmp = $data['hari'][$j];
		// 			$data['hari'][$j] = $data['hari'][$i];
		// 			$data['hari'][$i] = $tmp;

		// 			$tmp2 = $data['keuntungan_hari'][$j];
		// 			$data['keuntungan_hari'][$j] = $data['keuntungan_hari'][$i];
		// 			$data['keuntungan_hari'][$i] = $tmp2;
		// 		}
		// 	}
		// }

		// for ($i = 0; $i < 7; $i++) {
		// 	for ($j = 0; $j < 7; $j++) {
		// 		if ($data['hari'][$i] < $data['hari'][$j]) {
		// 			$tmp = $data['hari'][$j];
		// 			$data['hari'][$j] = $data['hari'][$i];
		// 			$data['hari'][$i] = $tmp;

		// 			$tmp2 = $data['keuntungan_hari'][$j];
		// 			$data['keuntungan_hari'][$j] = $data['keuntungan_hari'][$i];
		// 			$data['keuntungan_hari'][$i] = $tmp2;
		// 		}
		// 	}
		// }

		// for ($i = 0; $i < 7; $i++) {
		// 	$data['hari'][$i] = $data['tahun'][$i] . '-' . $data['bulan'][$i] . '-' . $data['hari'][$i];
		// 	$data['hari'][$i] = date('l', strtotime($data['hari'][$i]));
		// 	unset($data['bulan'][$i]);
		// 	unset($data['tahun'][$i]);
		// }

		$this->load->view('dashboard/chart');
	}

	public function chartBulanan()
	{
		// 	$pembelian_bulanan = $this->mod->sum_month('barang_masuk', '(harga_beli - (harga_beli * (diskon / 100))) * stok', 'tanggal >= DATE_SUB(now(), INTERVAL 6 MONTH)')->result_array();

		// 	$penjualan_bulanan = $this->mod->sum_month('barang_keluar', '(harga_jual - (harga_jual * (diskon / 100))) * stok', 'tanggal >= DATE_SUB(now(), INTERVAL 6 MONTH)')->result_array();

		// 	$data_count = count($pembelian_bulanan);

		// 	for ($i = 0; $i < 6; $i++) {
		// 		for ($j = 1; $j <= 12; $j++) {
		// 			if (isset($pembelian_bulanan[$i])) {
		// 				if ($pembelian_bulanan[$i]['month'] == $j) {
		// 					$data['label_tahunan'][$i] = $pembelian_bulanan[$i]['year'];
		// 					$data['label_bulanan'][$i] = $pembelian_bulanan[$i]['month'];
		// 					$data['keuntungan_bulanan'][$i] = $penjualan_bulanan[$i]['total'] - $pembelian_bulanan[$i]['total'];
		// 				}
		// 			} else {
		// 				$data['label_tahunan'][$i] = 0;
		// 				$data['label_bulanan'][$i] = $pembelian_bulanan[$data_count - 1]['month'] - $i;
		// 				$data['keuntungan_bulanan'][$i] = 0;
		// 			}
		// 		}
		// 	}

		// 	for ($i = 0; $i < 6; $i++) {
		// 		for ($j = 0; $j < 6; $j++) {
		// 			if ($data['label_bulanan'][$i] < $data['label_bulanan'][$j]) {
		// 				$tmp = $data['label_bulanan'][$j];
		// 				$data['label_bulanan'][$j] = $data['label_bulanan'][$i];
		// 				$data['label_bulanan'][$i] = $tmp;

		// 				$tmp2 = $data['keuntungan_bulanan'][$j];
		// 				$data['keuntungan_bulanan'][$j] = $data['keuntungan_bulanan'][$i];
		// 				$data['keuntungan_bulanan'][$i] = $tmp2;
		// 			}
		// 		}
		// 	}

		// 	for ($i = 0; $i < 6; $i++) {
		// 		if ($data['label_bulanan'][$i] == 1) {
		// 			$data['label_bulanan'][$i] = 'Januari';
		// 		} else if ($data['label_bulanan'][$i] == 2) {
		// 			$data['label_bulanan'][$i] = 'Februari';
		// 		} else if ($data['label_bulanan'][$i] == 3) {
		// 			$data['label_bulanan'][$i] = 'Maret';
		// 		} else if ($data['label_bulanan'][$i] == 4) {
		// 			$data['label_bulanan'][$i] = 'April';
		// 		} else if ($data['label_bulanan'][$i] == 5) {
		// 			$data['label_bulanan'][$i] = 'Mei';
		// 		} else if ($data['label_bulanan'][$i] == 6) {
		// 			$data['label_bulanan'][$i] = 'Juni';
		// 		} else if ($data['label_bulanan'][$i] == 7) {
		// 			$data['label_bulanan'][$i] = 'Juli';
		// 		} else if ($data['label_bulanan'][$i] == 8) {
		// 			$data['label_bulanan'][$i] = 'Agustus';
		// 		} else if ($data['label_bulanan'][$i] == 9) {
		// 			$data['label_bulanan'][$i] = 'September';
		// 		} else if ($data['label_bulanan'][$i] == 10) {
		// 			$data['label_bulanan'][$i] = 'Oktober';
		// 		} else if ($data['label_bulanan'][$i] == 11) {
		// 			$data['label_bulanan'][$i] = 'November';
		// 		} else if ($data['label_bulanan'][$i] == 12) {
		// 			$data['label_bulanan'][$i] = 'Desember';
		// 		}
		// 	}

		$this->load->view('dashboard/chart_bulanan');
	}
}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
