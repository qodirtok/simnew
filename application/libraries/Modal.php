<?php

class Modal
{
	protected $_CI;


	public function __construct()
	{
		//Do your magic here
		$this->_CI = get_instance();
	}

	public function formModal($title = null, $body = null, $buttonsave = null, $buttonreset = null)
	{
		# code...
		$data['modal'] = '<div class="modal tampil" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">' . $title . '</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					' . $body . '
				</div>
				
			</div>
		</div>
	</div>';

		return $data;
	}
}
