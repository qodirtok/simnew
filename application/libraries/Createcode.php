<?php

class Createcode
{
	protected $_CI;


	public function __construct()
	{
		//Do your magic here
		$this->_CI = get_instance();
	}

	public function takedb($db = null, $attr = null)
	{
		# code...
		$this->_CI->db->SELECT('RIGHT(' . $attr . ',4) AS code', FALSE)
			->FROM($db)
			->ORDER_BY($attr, 'DESC')
			->LIMIT(1);

		$query = $this->_CI->db->get();
		return $query;
	}

	public function Gcode($db = null, $attr = null)
	{
		# code...
		$takeDb = $this->takedb($db, $attr);

		if ($takeDb->num_rows() <> 0) {
			# code...
			$data = $takeDb->row();
			$code = intval($data->code) + 1;
		} else {
			# code...
			$code = 1;
		}

		if ($db == 'user') {
			$huruf = 'SKPM-';
		} else if ($db == 'barang_keluar') {
			$huruf = 'N';
		}

		$codeMax = str_pad($code, 4, "0", STR_PAD_LEFT); // angka 4 untuk mengatur banyak digit
		$codeResult = $huruf . date("Y") . date("d") . $codeMax;

		return $codeResult;
	}
}
