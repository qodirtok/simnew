<?php

class Template
{
	protected $_CI;


	public function __construct()
	{
		$this->CI = get_instance();
	}

	public function layouts($data)
	{
		# code...
		if ($data['folder'] != null || $data['file'] != null) {
			# code...
			$this->CI->load->view('_partials/01_head', $data);
			// if ($data['page'] === 'penjualan' || $data['page'] === 'stokbarang') {
			// 	# code...
			// 	$this->CI->load->view('_partials/02_top_navbar', $data);
			// 	$this->CI->load->view('_partials/03_top_sidebar', $data);
			// } else {
			// 	# code...
			$this->CI->load->view('_partials/02_navbar', $data);

			switch ($this->CI->session->userdata('role')) {
				case 'Administrator':
					# code...
					$this->CI->load->view('_partials/03_sidebar', $data);
					break;

				case 'User':
					$this->CI->load->view('_partials/03_user_sidebar', $data);
					# code...
					break;
			}

			// }
			$this->CI->load->view($data['folder'] . '/' . $data['file'], $data);
			$this->CI->load->view('_partials/05_footer', $data);
			$this->CI->load->view('_partials/06_js_load', $data);
			// $this->CI->load->view('_partials/07_js_chart', $data);
			$this->CI->load->view('_partials/07_js', $data);
		} else {
			$this->CI->load->view('error/404_found', $data);
		}
	}
}
