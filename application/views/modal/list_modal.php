<?php
$n = 1;
if (!empty($modal)) {
    # code...
    foreach ($modal as $key => $value) :
?>
        <tr>
            <td class="align-middle">
                <?php print $n; ?>
            </td>
            <td class="align-middle">
                <?php print 'Rp.' . number_format($value['modal']); ?>
            </td>
            <td class="align-middle">
                <?php print date('d-m-Y', strtotime($value['create_at'])); ?>
            </td>
            <td class="align-middle">
                <?php print (isset($value['update_at'])) ? date('d-m-Y', strtotime($value['update_at'])) : null; ?>
            </td>
            <td class="align-middle">
                <center>
                    <?php if (strtotime($value['create_at']) <= strtotime(date('Y-m-d'))) { ?>
                        <div class="buttons">
                            -
                        </div>
                    <?php } else { ?>
                        <div class="buttons">
                            <button class="btn btn-sm btn-icon btn-warning btndetail open-modal" data-link="<?php print base_url() . "modals/show/" . $value['id_modal']; ?>"><i class=" fas fa-eye"></i></button>
                        </div>
                    <?php } ?>
                </center>
            </td>
        </tr>

<?php $n++;
    endforeach;
}
?>