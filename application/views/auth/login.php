<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<title><?php print $title; ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php base_url() ?>assets/img/logo/icon/logos.ico" />

	<!-- General CSS Files -->
	<link rel="stylesheet" href="<?php print base_url(); ?>assets/modules/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php print base_url(); ?>assets/modules/fontawesome/css/all.min.css">

	<!-- CSS Libraries -->
	<link rel="stylesheet" href="<?php print base_url(); ?>assets/modules/bootstrap-social/bootstrap-social.css">
	<link rel="stylesheet" href="<?php print base_url(); ?>assets/modules/izitoast/css/iziToast.min.css">

	<!-- Template CSS -->
	<link rel="stylesheet" href="<?php print base_url(); ?>assets/css/style.css">
	<link rel="stylesheet" href="<?php print base_url(); ?>assets/css/components.css">
	<!-- Start GA -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-94034622-3');
	</script>
	<!-- /END GA -->
</head>

<body>
	<div id="app">
		<section class="section">
			<div class="container mt-5">
				<div class="row">
					<div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
						<div class="login-brand">
							<img src="<?php print base_url(); ?>assets/img/logo/logos.png" alt="logo" width="100" class="shadow-light">
						</div>

						<div class="card card-primary">
							<div class="card-header">
								<h4>Login</h4>
							</div>
							<div class="card-body">
								<?php if ($this->session->flashdata('message')) { ?>
									<div class="flash-data" data-flashdata="<?php print $this->session->flashdata('message'); ?>"></div>
								<?php } ?>
								<form method="POST" action="<?php print base_url() . "auth"; ?>" class="needs-validation" novalidate="">
									<div class="form-group">
										<label for="username">Username</label>
										<input id="username" type="text" class="form-control" name="username" tabindex="1">
										<div class="">
											<?php print form_error('username', '<small class="text-danger ">', '</small>'); ?>
										</div>
									</div>

									<div class="form-group">
										<div class="d-block">
											<label for="password" class="control-label">Password</label>
										</div>
										<input id="password" type="password" class="form-control" name="password" tabindex="2">
										<div class="">
											<?php print form_error('password', '<small class="text-danger ">', '</small>'); ?>
										</div>
									</div>

									<div class="form-group">
										<input href="<?php print base_url(); ?>dashboard" type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
									</div>
								</form>
							</div>
						</div>

						<div class="simple-footer">
							Copyright &copy; <?php print date('Y'); ?> <div class="bullet"></div> Sistem Informasi Koperasi Mahasiswa <div class="bullet"></div>Development by : qodirtok@gmail.com - Design by Stisla

						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<!-- General JS Scripts -->
	<script src="<?php print base_url(); ?>assets/modules/jquery.min.js"></script>
	<script src="<?php print base_url(); ?>assets/modules/popper.js"></script>
	<script src="<?php print base_url(); ?>assets/modules/tooltip.js"></script>
	<script src="<?php print base_url(); ?>assets/modules/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php print base_url(); ?>assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
	<script src="<?php print base_url(); ?>assets/modules/moment.min.js"></script>
	<script src="<?php print base_url(); ?>assets/js/stisla.js"></script>
	<script src="<?php print base_url(); ?>assets/modules/izitoast/js/iziToast.min.js"></script>

	<!-- JS Libraies -->

	<!-- Page Specific JS File -->

	<!-- Template JS File -->
	<script src="<?php print base_url(); ?>assets/js/scripts.js"></script>
	<script src="<?php print base_url(); ?>assets/js/custom.js"></script>

	<script>
		(function(document, window, $) {
			'use strict'
			$(document).ready(function() {
				const flashdata = $('.flash-data').attr('data-flashdata');
				switch (flashdata) {
					case 're-login':
						iziToast.error({
							title: 'Failed',
							message: 'Username/Password yang anda masukkan salah!!',
							position: 'topRight',
						});
						break;

					case 'login':
						iziToast.success({
							title: 'Success',
							message: 'Login berhasil',
							position: 'topRight',
						});
						break;
				}
			})
		})
		(document, window, jQuery);

		// const flashdata = $('.flash-data').data('message');
		// if (flashdata) {
		// console.log('asdasd');
		// }
	</script>

</body>

</html>