<?php
$pembelian_bulanan = $this->mod->sum_month('barang_masuk', '(harga_beli - (harga_beli * (diskon / 100))) * stok', 'tanggal >= DATE_SUB(now(), INTERVAL 6 MONTH)')->result_array();

$penjualan_bulanan = $this->mod->sum_month('barang_keluar', '(harga_jual - (harga_jual * (diskon / 100))) * stok', 'tanggal >= DATE_SUB(now(), INTERVAL 6 MONTH)')->result_array();
$data_count = count($pembelian_bulanan);

$keuntungan = [];
$k = 0;
for ($i = 0; $i < $data_count; $i++) {
    $keuntungan[$k] = $penjualan_bulanan[$k]['total'] - $pembelian_bulanan[$k]['total'];
    $k++;
}
?>

<div class="card-body" id="chart_month" style="display: none;">
    <canvas id="myChart2" class="chartjs-render-monitor" height="336" width="555" style="display: block; width: 555px; height: 336px;"></canvas>
</div>

<script type="text/javascript">
    var ctx2 = document.getElementById("myChart2").getContext('2d');
    var myChart2 = new Chart(ctx2, {
        type: 'line',
        data: {
            labels: [<?php foreach ($pembelian_bulanan as $value) {
                            if ($value['month'] == 1) {
                                $value['month'] = 'Januari';
                            } else if ($value['month'] == 2) {
                                $value['month'] = 'Februari';
                            } else if ($value['month'] == 3) {
                                $value['month'] = 'Maret';
                            } else if ($value['month'] == 4) {
                                $value['month'] = 'April';
                            } else if ($value['month'] == 5) {
                                $value['month'] = 'Mei';
                            } else if ($value['month'] == 6) {
                                $value['month'] = 'Juni';
                            } else if ($value['month'] == 7) {
                                $value['month'] = 'Juli';
                            } else if ($value['month'] == 8) {
                                $value['month'] = 'Agustus';
                            } else if ($value['month'] == 9) {
                                $value['month'] = 'September';
                            } else if ($value['month'] == 10) {
                                $value['month'] = 'Oktober';
                            } else if ($value['month'] == 11) {
                                $value['month'] = 'November';
                            } else if ($value['month'] == 12) {
                                $value['month'] = 'Desember';
                            }
                            echo "'" . $value['month'] . " " . $value['year'] . "',";
                        } ?>],
            datasets: [{
                label: 'Statistics',
                data: [<?php foreach ($keuntungan as $u) {
                            echo $u . ",";
                        } ?>],
                borderWidth: 2,
                backgroundColor: '#6777ef',
                borderColor: '#6777ef',
                borderWidth: 2.5,
                pointBackgroundColor: '#ffffff',
                pointRadius: 4
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 50000
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: true
                    },
                    gridLines: {
                        display: true
                    }
                }]
            },
        }
    });
</script>