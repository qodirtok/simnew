<?php
$pembelian_harian = $this->mod->sum_week('barang_masuk', '(harga_beli - (harga_beli * (diskon / 100))) * stok', 'tanggal <= DATE_SUB(now(), INTERVAL 7 DAY)')->result_array();

$penjualan_harian = $this->mod->sum_week('barang_keluar', '(harga_jual - (harga_jual * (diskon / 100))) * stok', 'tanggal <= DATE_SUB(now(), INTERVAL 7 DAY)')->result_array();

$data_count = count($pembelian_harian);

$keuntungan = [];
$k = 0;
for ($i = 0; $i < $data_count; $i++) {
    $keuntungan[$k] = $penjualan_harian[$k]['total'] - $pembelian_harian[$k]['total'];
    $k++;
}
?>

<div class="card-body" id="chart_week">
    <canvas id="myChart" class="chartjs-render-monitor" height="336" width="555" style="display: block; width: 555px; height: 336px;"></canvas>
</div>

<script type="text/javascript">
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [<?php foreach ($pembelian_harian as $value) {
                            echo "'" . $value['day'] . "',";
                        } ?>],
            datasets: [{
                label: 'Statistics',
                data: [<?php foreach ($keuntungan as $u) {
                            echo $u . ",";
                        } ?>],
                borderWidth: 2,
                backgroundColor: '#6777ef',
                borderColor: '#6777ef',
                borderWidth: 2.5,
                pointBackgroundColor: '#ffffff',
                pointRadius: 4
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 20000
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: true
                    },
                    gridLines: {
                        display: true
                    }
                }]
            },
        }
    });
</script>