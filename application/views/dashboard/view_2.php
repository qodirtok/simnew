<div class="main-content">
	<section class="section">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="card card-statistic-2">
					<div class="card-chart">
						<canvas id="balance-chart" height="80"></canvas>
					</div>
					<div class="card-icon shadow-primary bg-primary">
						<i class="fas fa-dollar-sign"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Modal Hari ini</h4>
						</div>
						<div class="card-body">
							<?php if (isset($modal->modal)) {
								print 'Rp. ' . number_format($modal->modal, 2, ',', '.');
							} else {
								print 'Rp. ' . number_format('0', 2, ',', '.');
							} ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="card card-statistic-2">
					<div class="card-chart">
						<canvas id="balance-chart" height="80"></canvas>
					</div>
					<div class="card-icon shadow-primary bg-primary">
						<i class="fas fa-dollar-sign"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Barang terjual hari ini</h4>
						</div>
						<div class="card-body">
							<?php print "Rp. " . number_format($pendapatan, 2, ',', '.') ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="card card-statistic-2">
					<div class="card-chart">
						<canvas id="sales-chart" height="80"></canvas>
					</div>
					<div class="card-icon shadow-primary bg-primary">
						<i class="fas fa-shopping-bag"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Pengeluaran hari ini</h4>
						</div>
						<div class="card-body">
							<?php print "Rp. " . number_format($pengeluaran, 2, ',', '.') ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="card card-statistic-2">
					<div class="card-chart">
						<canvas id="sales-chart" height="80"></canvas>
					</div>
					<div class="card-icon shadow-primary bg-primary">
						<i class="fas fa-shopping-bag"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Keuntungan hari ini</h4>
						</div>
						<div class="card-body">
							<?php print "Rp. " . number_format($keuntungan, 2, ',', '.') ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="card card-statistic-2">
					<div class="card-chart">
						<canvas id="sales-chart" height="80"></canvas>
					</div>
					<div class="card-icon shadow-primary bg-primary">
						<i class="fas fa-shopping-bag"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Pendatapan hari ini</h4>
						</div>
						<div class="card-body">
							<?php print "Rp. " . number_format($pendapatan, 2, ',', '.') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">

			<div class="col-lg-3 col-md-6 col-sm-6 col-12">
				<div class="card card-statistic-1">
					<div class="card-icon bg-danger">
						<i class="far fa-newspaper"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Total barang</h4>
						</div>
						<div class="card-body">
							<?php print $barang; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6 col-12">
				<div class="card card-statistic-1">
					<div class="card-icon bg-danger">
						<i class="far fa-newspaper"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Total Stok Barang</h4>
						</div>
						<div class="card-body">
							<?php if ($stok_barang['total'] === '' || $stok_barang['total'] === null || $stok_barang['total'] < 0) {
								print '0';
							} else {
								# code...
								print $stok_barang['total'];
							} ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6 col-12">
				<div class="card card-statistic-1">
					<div class="card-icon bg-danger">
						<i class="far fa-newspaper"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Total kategori</h4>
						</div>
						<div class="card-body">
							<?php print $kategori; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6 col-12">
				<div class="card card-statistic-1">
					<div class="card-icon bg-danger">
						<i class="far fa-newspaper"></i>
					</div>
					<div class="card-wrap">
						<div class="card-header">
							<h4>Total satuan</h4>
						</div>
						<div class="card-body">
							<?php print $satuan; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-1">
				<div class="card">
					<div class="form-group">
						<div class="row card-header">
							<div class="col">
								<h2 class="section-title">Graph Keuntungan</h2>
							</div>
							<div class="col-3">
								<select name="selectChart" id="selectChart" class="form-control select2">
									<option selected disabled>Pilih</option>
									<option value="chart_week">Per Minggu</option>
									<option value="chart_month">Per Bulan</option>
								</select>
							</div>
						</div>
						<div class="<?php print $chart ?>"></div>
						<div class="<?php print $chart_bulanan ?>"></div>
					</div>
				</div>
			</div>
	</section>
</div>