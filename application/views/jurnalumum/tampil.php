<?php
$n = 1;
$saldo_debet = 0;
$saldo_kredit = 0;
if (!empty($jurnalumum)) {
	# code...
	foreach ($jurnalumum as $key => $value) :
		$saldo_debet += $value['debet'];
		$saldo_kredit += $value['kredit'];
?>
		<tr>
			<td class="align-middle">
				<?php print $n; ?>
			</td>
			<td class="align-middle">
				<?php print $value['nama_akun']; ?>
			</td>
			<td class="align-middle">
				<?php print $value['keterangan']; ?>
			</td>
			<td class="align-middle">
				<?php print "Rp. " . number_format($value['debet'], 2, ',', '.'); ?>
			</td>
			<td class="align-middle">
				<?php print "Rp. " . number_format($value['kredit'], 2, ',', '.'); ?>
			</td>
			<td class="align-middle">
				<?php print date('Y-m-d', strtotime($value['create_at'])); ?>
			</td>
			<td class="align-middle">
				<div class="buttons">
					<button class="btn btn-sm btn-icon btn-warning btndetail open-modal" data-link="<?php print base_url() . "jurnalumum/show/" . $value['id_jurnal_umum']; ?>"><i class=" fas fa-eye"></i></button>
					<button class="btn btn-sm btn-icon btn-danger btndelete" data-link="<?php print base_url() . "jurnalumum/destroy/" . $value['id_jurnal_umum']; ?>"><i class="fas fa-trash"></i></button>
				</div>
			</td>
		</tr>

	<?php $n++;
	endforeach;
	?>
	<!-- <tr>
		<td colspan="3 align-middle text-center">
			<strong>Jumlah</strong>
		</td>
		<td class="align-middle">
			<strong><?php print "Rp. " . number_format($saldo_debet, 2, ',', '.') ?></strong>
		</td>
		<td class="align-middle">
			<strong><?php print "Rp. " . number_format($saldo_kredit, 2, ',', '.') ?></strong>
		</td>
	</tr> -->
<?php
}

?>