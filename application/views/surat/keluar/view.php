<div class="main-content page">
    <section class="section">

        <div class="section-header">
            <h1><?php print $title; ?></h1>
        </div>

        <div class="section-body">

            <h2 class="section-title">Data <?php print $title; ?></h2>
            <p class="section-lead">
                Kelola Data <?php print $title; ?>.
            </p>

            <div class="row">

                <div class="col-lg-12 col-md-12 col-12 col-sm-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Kelola Data <?php print $title; ?></h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <?php print $buttonLinktoSuratKeluar; ?>
                            </div>
                            <div class="table-responsive ">
                                <table class="table table-striped" id="list-Table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Nomor surat</th>
                                            <th>Nama Instansi</th>
                                            <th>Topik surat</th>
                                            <th>Tanggal kluar</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="<?php print $listData; ?>">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
</div>


<!-- //MODALL -->
<div class="tampil-modal"></div>