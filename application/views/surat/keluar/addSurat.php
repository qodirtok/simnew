<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php print $title; ?></h1>
        </div>

        <div class="section-body">

            <h2 class="section-title">Data <?php print $title; ?></h2>
            <p class="section-lead">
                Kelola Data <?php print $title; ?>.
            </p>

            <form class="form">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Tambah Surat Keluar</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                                        <div class="form-group kodeuser">
                                            <label for="nomor_surat">Nomor Surat</label>
                                            <input type="text" class=" form-control change" name="nomor_surat">
                                        </div>
                                        <div class="form-group kodeuser">
                                            <label for="nomor_surat">Nama Instansi</label>
                                            <input type="text" class=" form-control change" name="instansi">
                                        </div>
                                        <div class="form-group kodeuser">
                                            <label for="Kode_User">Topik surat</label>
                                            <textarea name="topik" id="" class=" form-control change"></textarea>
                                        </div>
                                        <div class="form-group kodeuser">
                                            <label for="Kode_User">Tanggal keluar surat</label>
                                            <input type="date" class=" form-control change" name="tanggal">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group float-right">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Upload Scan surat</h4>
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                                        <div class="form-group kodeuser">
                                            <label for="Kode_User"></label>
                                            <input type="file" class=" form-control change" id="scan" name="scan">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group float-right">
                                    <?php print $buttonSave; ?>
                                    <?php print $buttonRestart; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </section>
</div>


<!-- //MODALL -->
<div class="tampil-modal"></div>