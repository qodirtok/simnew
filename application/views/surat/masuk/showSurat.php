<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php print $title; ?></h1>
        </div>

        <div class="section-body">

            <h2 class="section-title">Data <?php print $title; ?></h2>
            <p class="section-lead">
                Kelola Data <?php print $title; ?>.
            </p>

            <form class="form">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Edit Surat Masuk</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                                        <input type="hidden" name="id" value="<?php print $surat[0]['id_surat']; ?>">
                                        <div class="form-group">
                                            <label for="nomor_surat">Nomor Surat</label>
                                            <input type="text" class=" form-control change" name="nomor_surat" value="<?php print $surat[0]['nomor_surat']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nomor_surat">Nama Instansi</label>
                                            <input type="text" class=" form-control change" name="instansi" value="<?php print $surat[0]['instansi']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="Kode_User">Topik surat</label>
                                            <textarea name="topik" id="" class=" form-control change"><?php print $surat[0]['topik']; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="Kode_User">Tanggal masuk surat</label>
                                            <input type="date" class=" form-control change" name="tanggal" value="<?php print $surat[0]['tanggal']; ?>" id="kode">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group float-right">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Upload Scan surat</h4>
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-3 col-md-12 col-12 col-sm-12">
                                        <div class="form-group gallery gallery-md">
                                            <div class="gallery-item" data-image="<?php print base_url() . '/assets/img/uploads/surat/masuk/' . $surat[0]['scan']; ?>" data-title="<?php print $surat[0]['scan']; ?>"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="Kode_User"></label>
                                            <input type="file" class=" form-control change" id="scan" name="scan" id="kode">
                                            <input type="hidden" name="old_scan" value="<?php print $surat[0]['scan']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group float-right">
                                    <?php print $buttonUpdate; ?>
                                    <?php print $buttonRestart; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </section>
</div>


<!-- //MODALL -->
<div class="tampil-modal"></div>