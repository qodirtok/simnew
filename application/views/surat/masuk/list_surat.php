<?php
$n = 1;
if (!empty($surat)) {
    # code...
    foreach ($surat as $key => $value) :
?>
        <tr>
            <td class="align-middle">
                <?php print $n; ?>
            </td>
            <td class="align-middle">
                <?php print $value['nomor_surat']; ?>
            </td>
            <td class="align-middle">
                <?php print $value['instansi']; ?>
            </td>
            <td class="align-middle">
                <?php print $value['topik']; ?>
            </td>
            <td class="align-middle">
                <?php print $value['tanggal']; ?>
            </td>
            <td class="align-middle">
                <div class="buttons">
                    <button class="btn btn-sm btn-icon btn-warning btndetail klik-menu" data-link="<?php print base_url() . "surat/show/" . $value['id_surat']; ?>"><i class=" fas fa-eye"></i></button>
                    <button class="btn btn-sm btn-icon btn-danger btndelete" data-link="<?php print base_url() . "surat/destroy/" . $value['id_surat']; ?>"><i class="fas fa-trash"></i></button>
                </div>
            </td>
        </tr>

<?php $n++;
    endforeach;
}
?>