<?php
$n = 1;
$saldo_kredit = 0;
if (!empty($kaskeluar)) {
	# code...
	foreach ($kaskeluar as $key => $value) :
		$saldo_kredit += $value['kredit'];
?>
		<tr>
			<td class="align-middle">
				<?php print $n; ?>
			</td>
			<td class="align-middle">
				<?php print $value['nama_akun']; ?>
			</td>
			<td class="align-middle">
				<?php print $value['keterangan']; ?>
			</td>
			<td class="align-middle">
				<?php print "Rp. " . number_format($value['debet'], 2, ',', '.'); ?>
			</td>
			<td class="align-middle">
				<?php print "Rp. " . number_format($value['kredit'], 2, ',', '.'); ?>
			</td>
			<td class="align-middle">
				<?php print date('Y-m-d', strtotime($value['tanggal'])); ?>
			</td>
			<td class="align-middle">
				<div class="buttons">
					<button class="btn btn-sm btn-icon btn-warning btndetail open-modal" data-link="<?php print base_url() . "kaskeluar/show/" . $value['id_kas_keluar']; ?>"><i class=" fas fa-eye"></i></button>
					<button class="btn btn-sm btn-icon btn-danger btndelete" data-link="<?php print base_url() . "kaskeluar/destroy/" . $value['id_kas_keluar']; ?>"><i class="fas fa-trash"></i></button>
				</div>
			</td>
		</tr>

	<?php $n++;
	endforeach;
	?>
	<!-- <tr>
		<td colspan="4">
			<strong>Jumlah</strong>
		</td>
		<td class="align-middle">
			<strong><?php print "Rp. " . number_format($saldo_kredit, 2, ',', '.') ?></strong>
		</td>
		<td></td>
		<td></td>
	</tr> -->
<?php
}

?>