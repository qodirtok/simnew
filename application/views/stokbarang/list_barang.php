<?php
$n = 1;
if (!empty($barang)) {
	# code...
	foreach ($barang as $key => $value) :
?>
		<tr>
			<td class="align-middle">
				<?php print $n; ?>
			</td>
			<td class="align-middle">
				<?php print $value['kode_barang']; ?>
			</td>
			<td class="align-middle">
				<?php print $value['nama_barang']; ?>
			</td>
			<td class="align-middle">
				<?php print 'Rp. ' . number_format($value['harga_jual']); ?>
			</td>
			<td class="align-middle">
				<?php if ($value['stok'] <= 0) {
					# code...
					print '<span class="badge badge-danger">Stok Habis</span>';
				} else {
					# code...
					if ($value['stok'] < 6) {
						# code...
						print '<span class="badge badge-warning">Stok hampir habis</span>';
					} else {
						# code...
						print $value['stok'];
					}
				}
				?>
			</td>
			<td class="align-middle">
				<div class="buttons">
					<button class="btn btn-sm btn-icon btn-warning  open-modal" data-link="<?php print base_url() . "stokbarang/show/" . $value['id_barang']; ?>"><i class=" fas fa-eye"></i></button>
					<!-- <button class="btn btn-sm btn-icon btn-danger btndelete" data-link="<?php print base_url() . "stokbarang/destroy/" . $value['id_barang']; ?>"><i class="fas fa-trash"></i></button> -->
				</div>
			</td>
		</tr>

<?php $n++;
	endforeach;
}
?>
