			<div class="main-sidebar sidebar-style-2">
				<aside id="sidebar-wrapper">
					<div class="sidebar-brand">
						<a href="<?php print base_url() . 'dashboard'; ?>">Sikopma</a>
					</div>
					<div class="sidebar-brand sidebar-brand-sm">
						<a href="<?php print base_url() . 'dashboard'; ?>">Sm</a>
					</div>
					<ul class="sidebar-menu">
						<li class="menu-header">Dashboard</li>

						<?php
						if ($this->session->userdata('role') === 'Administrator') {
							# code...
						?>
							<li class="dropdown <?php if ($page === 'dashboard umum' || $page === 'Dashboard Penjualan') {
													# code...
													print 'active';
												} ?>">
								<a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
								<ul class="dropdown-menu">
									<li class=<?php if ($page === 'dashboard umum') {
													# code...
													print 'active';
												} ?>><a class="page nav-link klik-menu" href="#" data-link="<?php print base_url() . 'dashboard'; ?>">Dashboard Umum</a></li>
									<li class=<?php if ($page === 'Dashboard Penjualan') {
													# code...
													print 'active';
												} ?>><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . 'dashboard/dashboardpenjualan' ?>">Dashboard Penjualan</a></li>
								</ul>
							</li>
						<?php } else { ?>

							<li class="<?php if ($page === 'Dashboard Penjualan') {
											# code...
											print 'active';
										} ?>"><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . 'dashboard/dashboardpenjualan' ?>"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
						<?php }
						?>

						<?php if ($this->session->userdata('role') === 'Administrator') {
							# code...
						?>
							<!-- //dokumen// -->
							<li class="menu-header">Dokumen</li>

							<li class="dropdown <?php if ($page === 'surat' || $page === 'surat_keluar') {
													# code...
													print 'active';
												} ?>">
								<a href="#" class="nav-link has-dropdown"><i class="fas fa-database"></i><span>Dokumen</span></a>
								<ul class="dropdown-menu">
									<li class=<?php if ($page === 'surat') {
													# code...
													print 'active';
												} ?>><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "surat"; ?>">Surat masuk</a></li>
									<li class=<?php if ($page === 'surat_keluar') {
													# code...
													print 'active';
												} ?>><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "surat_keluar"; ?>">Surat keluar</a></li>
								</ul>
							</li>

							<!-- <li class="<?php if ($page === 'surat') {
												print 'active';
											} ?>"><a class="nav-link klik-menu" data-link="<?php print base_url() . "surat"; ?>" href="#"><i class="fas fa-file"></i> <span>dokumen surat</span></a></li> -->

							<!-- //closedokumen -->

						<?php } ?>

						<!-- //Masterdata -->
						<li class="menu-header">Master</li>
						<li class="dropdown <?php if ($page === 'barang' || $page === 'kategori' || $page === 'satuan') {
												# code...
												print 'active';
											} ?>">
							<a href="#" class="nav-link has-dropdown"><i class="fas fa-database"></i><span>Master barang</span></a>
							<ul class="dropdown-menu">
								<li class=<?php if ($page === 'barang') {
												# code...
												print 'active';
											} ?>><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "barang"; ?>">Barang</a></li>
								<li class=<?php if ($page === 'kategori') {
												# code...
												print 'active';
											} ?>><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "kategori"; ?>">Kategori</a></li>
								<li class=<?php if ($page === 'satuan') {
												# code...
												print 'active';
											} ?>><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "satuan"; ?>">Satuan</a></li>
							</ul>
						</li>
						<!-- //closemasterdata -->

						<!-- //Masterdata -->
						<li class="menu-header">Laporan</li>

						<?php if ($this->session->userdata('role') === 'Administrator') {
							# code...
						?>
							<!-- //Laporan Dokumen -->
							<li class="dropdown <?php if ($page === 'lsurat') {
													# code...
													print 'active';
												} ?>">
								<a href="#" class="nav-link has-dropdown"><i class="fas fa-file-pdf"></i><span>Laporan Dokumen</span></a>
								<ul class="dropdown-menu">
									<li class=<?php if ($page === 'lsurat') {
													# code...
													print 'active';
												} ?>><a class="page nav-link klik-menu" data-link="<?php print base_url() . "laporan/surat"; ?>" href="#">Laporan Surat</a></li>
								</ul>
							</li>
							<!-- //Close Laporan dokumen -->
						<?php } ?>
						<!-- //Laporan peminjaman -->
						<!-- <li class="dropdown <?php if ($page === 'lpeminjaman') {
														# code...
														print 'active';
													} ?>">
							<a href="#" class="nav-link has-dropdown"><i class="fas fa-file-pdf"></i><span>Laporan Peminjaman</span></a>
							<ul class="dropdown-menu">
								<li class=<?php if ($page === 'lpeminjaman') {
												# code...
												print 'active';
											} ?>><a class="page nav-link" data-link="<?php print base_url() . "lpeminjaman"; ?>" href="#">Laporan peminjaman</a></li>
							</ul>
						</li> -->
						<!-- //Close Laporan peminjaman -->

						<!-- //Laporan Transaksi -->
						<li class="dropdown <?php if ($page === 'kasir' || $page === 'penjualan' || $page === 'pembelian' || $page === 'stok') {
												# code...
												print 'active';
											} ?>">
							<a href="#" class="nav-link has-dropdown"><i class="fas fa-clipboard"></i><span>Laporan Transaksi</span></a>
							<ul class="dropdown-menu">
								<li class=<?php if ($page === 'kasir') {
												# code...
												print 'active';
											} ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/transaksi/kasir"; ?>" href="#">Laporan kasir</a></li>
								<li class=<?php if ($page === 'penjualan') {
												# code...
												print 'active';
											} ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/transaksi/penjualan"; ?>" href="#">Laporan penjualan</a></li>
								<li class=<?php if ($page === 'pembelian') {
												# code...
												print 'active';
											} ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/transaksi/pembelian"; ?>" href="#">Laporan pembelian</a></li>
								<li class=<?php if ($page === 'stok') {
												# code...
												print 'active';
											} ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/transaksi/stok"; ?>" href="#">Laporan Stok</a></li>
							</ul>
						</li>
						<!-- //CloseLaporan transaksi -->

						<!-- //laporan keuangan -->
						<li class="dropdown <?php if ($page === 'lkeuangan' || $page === 'buku_besar' || $page === 'laba_rugi' || $page === 'neraca_saldo' || $page === 'neraca') {
												# code...
												print 'active';
											} ?>">
							<a href="#" class="nav-link has-dropdown"><i class="fas fa-file-invoice-dollar"></i><span>Laporan Keuangan</span></a>
							<ul class="dropdown-menu">
								<li class=<?php if ($page === 'buku_besar') {
												# code...
												print 'active';
											} ?>><a class="page nav-link klik-menu" data-link="<?php print base_url() . "laporan/keuangan/buku_besar"; ?>" href="#">Buku besar</a></li>
								<li class=<?php if ($page === 'laba_rugi') {
												# code...
												print 'active';
											} ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/keuangan/laba_rugi"; ?>" href="#">Laba rugi</a></li>
								<li class=<?php if ($page === 'neraca_saldo') {
												# code...
												print 'active';
											} ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/keuangan/neraca_saldo"; ?>" href="#">Neraca saldo</a></li>
								<li class=<?php if ($page === 'neraca') {
												# code...
												print 'active';
											} ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/keuangan/neraca"; ?>" href="#">Neraca</a></li>
							</ul>
						</li>
						<!-- //closelaporankeuangan -->

						<?php if ($this->session->userdata('role') === 'Administrator') {
							# code...
						?>
							<li class="menu-header">Admin</li>
							<li class="<?php if ($page === 'user') {
											print 'active';
										} ?>"><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . 'user/'; ?>"><i class="fas fa-users"></i> <span>User</span></a></li>
						<?php } ?>

						<div class="mt-4 mb-4 p-3 hide-sidebar-mini">
							<a href="#" class="klik-menu btn btn-primary btn-lg btn-block btn-icon-split" data-link="<?php print base_url() . 'auth/logout'; ?>">
								<i class="fas fa-sign-out-alt"></i> Logout
							</a>
						</div>
				</aside>
			</div>