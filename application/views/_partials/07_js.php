<script type="text/javascript">
	// All JavaScript Custom
	(function(document, window, $) {
		'use strict'
		// var Site = window.Site;
		var MyTable;
		var tableScroll;
		$(document).ready(function() {
			// Site.run();
			// console.log(Site);

			const flashLogin = $('.flashLogin').attr('data-flashLogin');
			const flashModal = $('.flashModal').attr('data-flashModal');

			if (flashModal) {
				if (flashModal == 'input modal') {
					$('.toDisable').prop('disabled', true)
					cekModal()
				} else {
					$('.modalForm').hide();
					$('.toDisable').removeAttr('disabled', true)
				}
			}


			function cekModal() {
				swal({
					icon: 'warning',
					text: 'Silahkan input modal untuk bisa melanjutkan transaksi',
					dangerMode: true,
					closeOnClickOutside: false,
					closeOnEsc: false,
					content: {
						element: 'input',
						attributes: {
							placeholder: '',
							type: 'number',
						},
					},
				}).then((data) => {
					if (data) {
						$.ajax({
								type: "POST",
								url: "<?php print base_url() . 'modals/store'; ?>",
								data: {
									modal: data
								},
							})
							.done(function(response) {
								var json = $.parseJSON(response)
								if (json.msg == 'berhasil') {
									$('.modalForm').hide();
									$('.toDisable').removeAttr('disabled', true)
								} else {
									$('.toDisable').prop('disabled', true)
								}
							})
					} else {
						var link = '<?php print base_url() . 'tpenjualan' ?>'

						cekModal();
					}
				});
			}


			if (flashLogin) {
				iziToast.success({
					title: 'Success',
					message: 'Login Berhasil',
					position: 'topRight',
				});
			}

			const getModal = $('.getModal')
			if (getModal) {
				setInterval(function() {
					getModal.html(getModal.attr('data-modal'));
				}, 1000)
			}

			MyTable = $('#list-Table').dataTable({
				"paging": true,
				"lengthChange": true,
				"searching": true,
				"ordering": true,
				"info": true,
				"autoWidth": true,
				"responsive": {
					details: false
				}
			});

			tableScroll = $('#tabel-keranjang').dataTable({
				"scrollY": "300px",
				"scrollCollapse": true,
				"paging": false,
				"searching": true,
				"ordering": true,
				"autoWidth": true,
				"info": true,
				"responsive": true
			});

			tampilData();
			tampilDataChartBulanan();

			var link = '<?php print $this->uri->segment(1); ?>';

			switch (link) {
				case 'user':
					setInterval(function() {
						generatecode();
					}, 1000)
					break;
				case 'tpenjualan':
					setInterval(function() {
						refreshModal();
					}, 1000)
					break;
				case 'stokpembelianbarang':
					setInterval(function() {
						refreshModal();
					}, 1000)
					break;
				case 'retur':
					setInterval(function() {
						refreshModal();
					}, 1000)
					break;
				case 'satuan':
					setInterval(function() {
						refreshModal();
					}, 1000)
					break;
				case 'kategori':
					setInterval(function() {
						refreshModal();
					}, 1000)
					break;
				case 'stokbarang':
					setInterval(function() {
						refreshModal();
					}, 1000)
					break;
			}



			function refresh() {
				// MyTable.ajax.reload(null, false);
				MyTable = $('#list-Table').dataTable();
			}


			function tampilData() {
				var link = $('.data-list').attr('data-link');
				$.get(link, function(data) {
					MyTable.fnDestroy();
					$('.data-list').html(data);
					refresh();
				})
			}

			function tampilDataChartBulanan() {
				var link = $('.data-list-bulanan').attr('data-link');
				$.get(link, function(data) {
					MyTable.fnDestroy();
					$('.data-list-bulanan').html(data);
					refresh();
				})
			}


			function generatecode() {
				var link = '<?php print base_url() . "user/getKode" ?>';
				$.get(link, function(data) {
					var out = jQuery.parseJSON(data);
					var a = document.getElementById("kode");

					a.value = out.kode;

				})
			}

			function refreshModal() {
				var link = $('.getModal').attr('data-link');
				$.get(link, function(data) {
					var out = jQuery.parseJSON(data);
					var a = $('.getModal');
					// if (out.modal === null || out.modal === '') {
					// 	a.html(0);
					// }
					a.html(out.modal);
				})
			}

			$(document).on('click', '.link-to-form', function() {
				var link = $(this).attr('data-link');

				window.location.href = link;
				return false;
			});

			// input to rupiah
			$(function() {
				$('.rupiah').priceFormat({
					prefix: '',
					centsLimit: 0,
					thousandsSeparator: '.',
				});
			});
			// close input to rupiah

			$(document).on('click', '.btn-addCart', function() {
				var link = $(this).attr('data-link')
				var data = new FormData(this.form)

				$.ajax({
						type: "POST",
						url: link,
						data: data,
						async: true,
						processData: false,
						contentType: false,
						cache: false,
					})
					.done(function(response) {
						var json = $.parseJSON(response)
						switch (json.msg) {
							case 'berhasil':
								tampilData();
								$('.formCart')[0].reset();
								$('#subtotal_input').val(json.sub_total);
								$('#total_input').val(json.sub_total);
								iziToast.success({
									title: 'Success',
									message: 'Data ' + json.msg + ' dimasukan ke keranjang belanja',
									position: 'topRight',
								});
								break;
							case 'gagal':
								tampilData();
								iziToast.error({
									title: 'Failed',
									message: 'Data ' + json.msg + ' disimpan/field tidak sesuai. ' + json.msgvalue,
									position: 'topRight',
								});
								break;
						}
					})
				return false
			})

			// btn-save
			$(document).on('click', '.btn-save', function(e) {
				$('.btn-save').text('proses...');
				$('.btn-save').attr('disabled', true);

				var link = $(this).attr('data-link');
				// var form = $('.form').serialize();
				var data = new FormData(this.form);

				$.ajax({
						url: link,
						type: 'POST',
						async: true,
						processData: false,
						contentType: false,
						cache: false,
						data: data,
					})
					.done(function(data) {
						console.log(data);
						var out = jQuery.parseJSON(data);
						switch (out.msg) {
							case "gagal":
								console.log(out.msg);
								tampilData();
								iziToast.error({
									title: 'Failed',
									message: 'Data ' + out.msg + ' disimpan/field tidak sesuai. ' + out.msgvalue + '.',
									position: 'topRight',
								});
								break;
							case "berhasil":
								console.log(out.msg);
								$('.form')[0].reset();

								if ($('#nota_input').length) {
									$('#nota_input').val(out.nota);
								}
								if ($('.tampil').length) {
									$('.tampil').modal('hide');
								}
								if ($('.formCart').length) {
									$('.formCart')[0].reset();
								}

								tampilData();
								iziToast.success({
									title: 'Success',
									message: 'Data ' + out.msg + ' disimpan',
									position: 'topRight',
								});
								var page = '<?php print $this->uri->segment(1); ?>';
								if (page === 'user') {

									$(document).on('click', '.generatecode', function(e) {
										generatecode();
										e.preventDefault();
										return false;
									})
								}
								break;
						}
						$('.btn-save').text('Simpan');
						$('.btn-save').attr('disabled', false);
						tampilData();
					})

				e.preventDefault();
				return false;
			});

			$(document).on('click', '.btn-save-jual', function(e) {
				$('.btn-save-jual').text('proses...');
				$('.btn-save-jual').attr('disabled', true);

				var link = $(this).attr('data-link');
				// var form = $('.form').serialize();
				var data = new FormData(this.form);
				var nota = $("input[name='nota']").val();

				$.ajax({
						url: link,
						type: 'POST',
						async: true,
						processData: false,
						contentType: false,
						cache: false,
						data: data,
					})
					.done(function(data) {
						var out = jQuery.parseJSON(data);
						switch (out.msg) {
							case "gagal":
								tampilData();
								iziToast.error({
									title: 'Failed',
									message: 'Data ' + out.msg + ' disimpan/field tidak sesuai. ' + out.msgvalue + '.',
									position: 'topRight',
								});
								break;
							case "berhasil":
								swal("Apakah Ingin Print Struk Belanja?", {
										buttons: {
											cancel: "Tidak",
											catch: {
												text: "Iya",
												value: "yes",
											},
										},
									})
									.then((value) => {
										switch (value) {
											case "yes":
												window.open("<?php print base_url(); ?>tpenjualan/struk/" + nota, '_blank');
												window.focus();
												break;
											default:
												$('#subtotal_input').val('0');
												$('#total_input').val('0');
												$('.form')[0].reset();
												$('.formCart')[0].reset();
												$('#nota_input').val(out.nota);
												tampilData();
												iziToast.success({
													title: 'Success',
													message: 'Data ' + out.msg + ' disimpan',
													position: 'topRight',
												});
												var page = '<?php print $this->uri->segment(1); ?>';
												if (page === 'user') {

													$(document).on('click', '.generatecode', function(e) {
														generatecode();
														e.preventDefault();
														return false;
													})
												}
												break;
										}
									});
						}
						$('.btn-save-jual').text('Simpan');
						$('.btn-save-jual').attr('disabled', false);
						tampilData();
					})

				e.preventDefault();
				return false;
			});

			//btn-update
			$(document).on('click', '.btn-update', function(e) {
				$('.btn-update').text('proses...');
				$('.btn-update').attr('disabled', true);

				var link = $(this).attr('data-link');
				// var form = $('.form-update').serialize();
				var data = new FormData(this.form);
				// console.log(data);
				$.ajax({
						url: link,
						type: 'POST',
						async: true,
						processData: false,
						contentType: false,
						cache: false,
						data: data,
					})
					.done(function(data) {
						var out = jQuery.parseJSON(data);
						switch (out.msg) {
							case "gagal":
								tampilData();
								iziToast.error({
									title: 'Failed',
									message: 'Data ' + out.msg + ' disimpan/field tidak sesuai',
									position: 'topRight',
								});
								break;
							case "berhasil":
								// $('.form')[0].reset();
								tampilData();
								$('.tampil').modal('hide');
								$('.tampil-modal').modal('hide');
								iziToast.success({
									title: 'Success',
									message: 'Data ' + out.msg + ' disimpan',
									position: 'topRight',
								});
								var page = '<?php print $this->uri->segment(1); ?>';
								if (page === 'user') {
									// console.log(link);
									// $('.riset').val('');
									$(document).on('click', '.generatecode', function(e) {
										e.preventDefault();
										return false;
									})
								}
								break;
						}
						$('.btn-update').text('Simpan');
						$('.btn-update').attr('disabled', false);
						tampilData();
					})

				e.preventDefault();
				return false;
			});

			//btn-delete
			$(document).on('click', '.btndelete', function(e) {
				var link = $(this).attr('data-link');

				swal({
						title: 'Peringatan',
						text: 'apakah yakin ingin dihapus ?',
						icon: 'warning',
						buttons: {
							confirm: {
								text: 'Ya',
								color: '#FB160A',
								value: true,
								visible: true,
								closeModal: true
							},
							cancel: {
								text: "Tidak",
								value: null,
								visible: true,
								closeModal: true
							}
						},
						dangerMode: true,
					})
					.then((willDelete) => {
						if (willDelete) {
							$.ajax({
									url: link,
									type: 'POST',
									async: true,
								})
								.done(function(data) {
									var out = jQuery.parseJSON(data);
									tampilData();
									switch (out.msg) {
										case 'berhasil':
											tampilData();
											$('#subtotal_input').val(out.sub_total);
											$('#total_input').val(out.sub_total);
											iziToast.success({
												title: 'Success',
												message: 'Data ' + out.msg + ' dihapus',
												position: 'topRight',
											})
											break;

										default:
											tampilData();
											iziToast.error({
												title: 'Failed',
												message: 'Data ' + out.msg + ' dihapus',
												position: 'topRight',
											});
											break;
									}
								})
						}
					})
				e.preventDefault();
				return false;
			});

			//btn-modal
			$(document).on('click', '.open-modal', function() {
				var link = $(this).attr('data-link');
				var nota = $("input[name='nota']").val();
				var tipe = $("input[name='tipe']:checked").val();
				$.ajax({
						type: 'POST',
						url: link,
						async: true,
					})
					.done(function(data) {
						var out = jQuery.parseJSON(data);
						$('.tampil-modal').html(out.modal.modal);
						$('.tampil').modal('show');
						$("input:hidden[name='nota']").val(nota);
						$("input:hidden[name='tipe_retur']").val(tipe);
						// console.log(out.modal.modal);
					})
			})
			//close btn-mdal

			// href sidebar
			$(document).on('click', '.klik-menu', function() {
				var link = $(this).attr('data-link');
				window.location.href = link;
				return false;
			});

			$(document).on('input', '#search_func', function() {
				var link = $('.data-list-search').attr('data-link');
				var id = {
					id: $(this).val(),
				};
				$.ajax({
					type: "POST",
					url: link,
					data: id,
					success: function(msg) {
						$('.data-list-search').html(msg);
						refresh();
					}
				});
			});

			$(document).on('click', '.btn-cetak', function(e) {
				$('.btn-cetak').text('Proses...');
				$('.btn-cetak').attr('disabled', true);

				var link = $(this).attr('data-link');
				// var form = $('.form').serialize();
				var data = new FormData(this.form);
				var tanggal = $('input[name=tanggal]').val();
				var tanggal2 = $('input[name=tanggal2]').val();

				$.ajax({
						url: link,
						type: 'POST',
						async: true,
						processData: false,
						contentType: false,
						cache: false,
						data: data,
					})
					.done(function(data) {
						if (tanggal == '' || tanggal2 == '' || (tanggal > tanggal2)) {
							var json = $.parseJSON(data)
							switch (json.msg) {
								case 'gagal':
									iziToast.error({
										title: 'Failed',
										message: 'Data gagal dicari. ' + json.msgvalue,
										position: 'topRight',
									});
									break;
							}
						} else {
							$('.btn-to-cetak').show();
							$('.data-list-laporan').html(data);
						}

						var page = '<?php print $this->uri->segment(1); ?>';
						if (page === 'user') {

							$(document).on('click', '.generatecode', function(e) {
								generatecode();
								e.preventDefault();
								return false;
							})

						}

						$('.btn-cetak').text('Cari');
						$('.btn-cetak').attr('disabled', false);
					})

				e.preventDefault();
				return false;
			});

			$(document).on('click', '.btn-cetak-kasir', function(e) {
				$('.btn-cetak-kasir').text('Proses...');
				$('.btn-cetak-kasir').attr('disabled', true);

				var link = $(this).attr('data-link');
				// var form = $('.form').serialize();
				var data = new FormData(this.form);
				var tanggal = $('input[name=tanggal]').val();
				var tanggal2 = $('input[name=tanggal2]').val();

				$.ajax({
						url: link,
						type: 'POST',
						async: true,
						processData: false,
						contentType: false,
						cache: false,
						data: data,
					})
					.done(function(data) {
						if ((tanggal == '' && tanggal2 == '')) {
							console.log(data);
							var json = $.parseJSON(data)
							switch (json.msg) {
								case 'gagal':
									iziToast.error({
										title: 'Failed',
										message: 'Data gagal dicari. ' + json.msgvalue,
										position: 'topRight',
									});
									break;
							}
						} else if ((tanggal2 != '')) {
							if (tanggal > tanggal2) {
								console.log(data);
								var json = $.parseJSON(data)
								switch (json.msg) {
									case 'gagal':
										iziToast.error({
											title: 'Failed',
											message: 'Data gagal dicari. ' + json.msgvalue,
											position: 'topRight',
										});
										break;
								}
							} else {
								$('.btn-to-cetak-kasir').show();
								$('.data-list-laporan').html(data);
							}
						} else {
							$('.btn-to-cetak-kasir').show();
							$('.data-list-laporan').html(data);
						}

						var page = '<?php print $this->uri->segment(1); ?>';
						if (page === 'user') {

							$(document).on('click', '.generatecode', function(e) {
								generatecode();
								e.preventDefault();
								return false;
							})

						}

						$('.btn-cetak-kasir').text('Cari');
						$('.btn-cetak-kasir').attr('disabled', false);
					})

				e.preventDefault();
				return false;
			});

			$(document).on('click', '.btn-to-cetak', function(e) {
				var link = $(this).attr('data-link');
				var tipe = $('input[name=tipe]').val();
				var tanggal = $('input[name=tanggal]').val();
				var tanggal2 = $('input[name=tanggal2]').val();

				if (tanggal == '' || tanggal2 == '' || (tanggal > tanggal2)) {
					iziToast.error({
						title: 'Failed',
						message: 'Data gagal dicari. Input tanggal tidak valid',
						position: 'topRight',
					});
				} else {
					window.open(link + '/' + tipe + '/' + tanggal + '/' + tanggal2, '_blank');
					window.focus();
				}
			});

			$(document).on('click', '.btn-to-cetak-kasir', function(e) {
				var link = $(this).attr('data-link');
				var tipe = $('input[name=tipe]').val();
				var tanggal = $('input[name=tanggal]').val();
				var tanggal2 = $('input[name=tanggal2]').val();

				if ((tanggal == '' && tanggal2 == '')) {
					console.log(data);
					var json = $.parseJSON(data)
					switch (json.msg) {
						case 'gagal':
							iziToast.error({
								title: 'Failed',
								message: 'Data gagal dicari. ' + json.msgvalue,
								position: 'topRight',
							});
							break;
					}
				} else if ((tanggal2 != '')) {
					if (tanggal > tanggal2) {
						console.log(data);
						var json = $.parseJSON(data)
						switch (json.msg) {
							case 'gagal':
								iziToast.error({
									title: 'Failed',
									message: 'Data gagal dicari. ' + json.msgvalue,
									position: 'topRight',
								});
								break;
						}
					} else {
						window.open(link + '/' + tipe + '/' + tanggal + '/' + tanggal2, '_blank');
						window.focus();
					}
				} else {
					if (tanggal2 == '') {
						window.open(link + '/' + tipe + '/' + tanggal + '/' + tanggal, '_blank');
						window.focus();
					} else {
						window.open(link + '/' + tipe + '/' + tanggal + '/' + tanggal2, '_blank');
						window.focus();
					}
				}
			});

			$(document).on('click', '.btn-to-cetak-neraca', function(e) {
				var link = $(this).attr('data-link');
				var tipe = $('input[name=tipe]').val();
				var tanggal = $('input[name=tanggal]').val();
				var tanggal2 = $('input[name=tanggal2]').val();

				if (tanggal == '' || (tanggal > tanggal2)) {
					iziToast.error({
						title: 'Failed',
						message: 'Data gagal dicari. Input tanggal tidak valid',
						position: 'topRight',
					});
				} else {
					window.open(link + '/' + tipe + '/' + tanggal, '_blank');
					window.focus();
				}
			});

			$(document).on('click', '.btn-cetak-surat', function(e) {
				$('.btn-cetak').text('Proses...');
				$('.btn-cetak').attr('disabled', true);

				var link = $(this).attr('data-link');
				// var form = $('.form').serialize();
				var data = new FormData(this.form);
				var tipe = $('select[name=tipe] option').filter(':selected').val()
				var tanggal = $('input[name=tanggal]').val();
				var tanggal2 = $('input[name=tanggal2]').val();

				$.ajax({
						url: link,
						type: 'POST',
						async: true,
						processData: false,
						contentType: false,
						cache: false,
						data: data,
					})
					.done(function(data) {
						if (tanggal == '' || tanggal2 == '' || (tanggal > tanggal2) || tipe == '') {
							var json = $.parseJSON(data)
							switch (json.msg) {
								case 'gagal':
									iziToast.error({
										title: 'Failed',
										message: 'Data gagal dicari. ' + json.msgvalue,
										position: 'topRight',
									});
									break;
							}
						} else {
							$('.btn-to-cetak-surat').show();
							$('.data-list-laporan').html(data);
						}

						var page = '<?php print $this->uri->segment(1); ?>';
						if (page === 'user') {

							$(document).on('click', '.generatecode', function(e) {
								generatecode();
								e.preventDefault();
								return false;
							})

						}

						$('.btn-cetak').text('Cari');
						$('.btn-cetak').attr('disabled', false);
					})

				e.preventDefault();
				return false;
			});

			$(document).on('click', '.btn-to-cetak-surat', function(e) {
				var link = $(this).attr('data-link');
				var tipe = $('select[name=tipe] option').filter(':selected').val()
				var tanggal = $('input[name=tanggal]').val();
				var tanggal2 = $('input[name=tanggal2]').val();
				if (tanggal == '' || tanggal2 == '' | (tanggal > tanggal2) || tipe == 'Pilih Tipe') {
					iziToast.error({
						title: 'Failed',
						message: 'Data gagal dicari. Input tanggal atau Tipe tidak valid',
						position: 'topRight',
					});

				} else {
					window.open(link + '/' + tipe + '/' + tanggal + '/' + tanggal2, '_blank');
					window.focus();
				}

			});

			$(document).on('input', '#search_func_retur', function() {
				var link = $('.data-list-search').attr('data-link');
				var id = {
					id: $(this).val(),
					tipe: $("input[name='tipe']:checked").val(),
				};
				$.ajax({
					type: "POST",
					url: link,
					data: id,
					success: function(msg) {
						$('.data-list-search').html(msg);
						refresh();
					}
				});
			});

			$(document).on('input', '#diskon_func', function() {
				var input = {
					subtotal: $('#subtotal_input').val(),
					diskon: $(this).val() / 100,
				}
				$('#total_input').val(input.subtotal - (input.subtotal * input.diskon));
				refresh();
			});

			$(document).on('input', '#bayar_func', function() {
				var input = {
					total: $('#total_input').val(),
					bayar: $(this).val(),
				}
				$('#kembalian_input').val(input.bayar - input.total);
				refresh();
			});

			$("#selectChart").on('change', function() {
				if ($(this).val() == 'chart_week') {
					$("#chart_week").show();
					$("#chart_month").hide();
				} else if ($(this).val() == 'chart_month') {
					$("#chart_week").hide();
					$("#chart_month").show();
				}
			});

			//close js
		});
	})
	(document, window, jQuery);
</script>

</body>

</html>