<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="<?php print base_url() . 'dashboard'; ?>">Sikopma</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?php print base_url() . 'dashboard'; ?>">Sm</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>

            <li class="<?php if ($page === 'Dashboard Penjualan') {
                            # code...
                            print 'active';
                        } ?>"><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . 'dashboard/dashboardpenjualan' ?>"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>

            <!-- //modal// -->
            <li class="menu-header">Modal</li>
            <li class="<?php if ($page === 'modal') {
                            print 'active';
                        } ?>"><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "modals"; ?>"><i class="fas fa-hand-holding-usd"></i> <span>Modal</span></a></li>
            <!-- //closemodal -->

            <!-- //Transaksi// -->
            <li class="menu-header">Transaksi</li>
            <li class="<?php if ($page === 'tpenjualan') {
                            print 'active';
                        } ?>"><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "tpenjualan"; ?>"><i class="fas fa-money-bill-wave"></i> <span>Penjualan</span></a></li>
            <!-- //closeTransaksi -->



            <!-- //Stok pembelian barang// -->
            <li class="menu-header">Pembelian barang</li>
            <li class="<?php if ($page === 'stokpembelianbarang') {
                            print 'active';
                        } ?>"><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "stokpembelianbarang"; ?>"><i class="fas fa-hand-holding-usd"></i> <span>Pembelian barang</span></a></li>
            <!-- //closeStok pembelian barang -->

            <!-- //retur// -->
            <li class="menu-header">Retur</li>
            <li class="<?php if ($page === 'retur') {
                            print 'active';
                        } ?>"><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "retur"; ?>"><i class="fas fa-hand-holding-usd"></i> <span>Retur</span></a></li>
            <!-- //closeretur -->

            <!-- //Stok barang// -->
            <li class="menu-header">Stok barang</li>
            <li class="<?php if ($page === 'stokbarang') {
                            print 'active';
                        } ?>"><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "stokbarang"; ?>"><i class="fas fa-hand-holding-usd"></i> <span>Stok barang</span></a></li>
            <!-- //closeStok barang -->

            <!-- // kas menu -->
            <li class="menu-header">Keuangan</li>
            <li class="dropdown <?php if ($page === 'kasmasuk' || $page === 'kaskeluar' || $page === 'jurnalumum') {
                                    # code...
                                    print 'active';
                                } ?>">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-file-invoice-dollar"></i><span>Kas</span></a>
                <ul class="dropdown-menu">
                    <li class=<?php if ($page === 'kasmasuk') {
                                    # code...
                                    print 'active';
                                } ?>><a class="page nav-link klik-menu" data-link="<?php print base_url() . "kasmasuk"; ?>" href="#">Kas Masuk</a></li>
                    <li class=<?php if ($page === 'kaskeluar') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "kaskeluar"; ?>" href="#">Kas Keluar</a></li>
                    <li class=<?php if ($page === 'jurnalumum') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "jurnalumum"; ?>" href="#">Jurnal Umum</a></li>
                </ul>
            </li>
            <!-- //close kas menu -->

            <!-- //Masterdata -->
            <li class="menu-header">Master</li>
            <li class="dropdown <?php if ($page === 'barang' || $page === 'kategori' || $page === 'satuan') {
                                    # code...
                                    print 'active';
                                } ?>">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-database"></i><span>Master barang</span></a>
                <ul class="dropdown-menu">
                    <li class=<?php if ($page === 'barang') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "barang"; ?>">Barang</a></li>
                    <li class=<?php if ($page === 'kategori') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "kategori"; ?>">Kategori</a></li>
                    <li class=<?php if ($page === 'satuan') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" href="#" data-link="<?php print base_url() . "satuan"; ?>">Satuan</a></li>
                </ul>
            </li>
            <!-- //closemasterdata -->

            <!-- //Masterdata -->
            <li class="menu-header">Laporan</li>
            <!-- //Laporan peminjaman -->
            <!-- <li class="dropdown <?php if ($page === 'lpeminjaman') {
                                            # code...
                                            print 'active';
                                        } ?>">
							<a href="#" class="nav-link has-dropdown"><i class="fas fa-file-pdf"></i><span>Laporan Peminjaman</span></a>
							<ul class="dropdown-menu">
								<li class=<?php if ($page === 'lpeminjaman') {
                                                # code...
                                                print 'active';
                                            } ?>><a class="page nav-link" data-link="<?php print base_url() . "lpeminjaman"; ?>" href="#">Laporan peminjaman</a></li>
							</ul>
						</li> -->
            <!-- //Close Laporan peminjaman -->

            <!-- //Laporan Transaksi -->
            <li class="dropdown <?php if ($page === 'kasir' || $page === 'penjualan' || $page === 'pembelian' || $page === 'stok') {
                                    # code...
                                    print 'active';
                                } ?>">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-clipboard"></i><span>Laporan Transaksi</span></a>
                <ul class="dropdown-menu">
                    <li class=<?php if ($page === 'kasir') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/transaksi/kasir"; ?>" href="#">Laporan kasir</a></li>
                    <li class=<?php if ($page === 'penjualan') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/transaksi/penjualan"; ?>" href="#">Laporan penjualan</a></li>
                    <li class=<?php if ($page === 'pembelian') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/transaksi/pembelian"; ?>" href="#">Laporan pembelian</a></li>
                    <li class=<?php if ($page === 'stok') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/transaksi/stok"; ?>" href="#">Laporan Stok</a></li>
                </ul>
            </li>
            <!-- //CloseLaporan transaksi -->

            <!-- //laporan keuangan -->
            <li class="dropdown <?php if ($page === 'lkeuangan' || $page === 'buku_besar' || $page === 'laba_rugi' || $page === 'neraca_saldo' || $page === 'neraca') {
                                    # code...
                                    print 'active';
                                } ?>">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-file-invoice-dollar"></i><span>Laporan Keuangan</span></a>
                <ul class="dropdown-menu">
                    <li class=<?php if ($page === 'buku_besar') {
                                    # code...
                                    print 'active';
                                } ?>><a class="page nav-link klik-menu" data-link="<?php print base_url() . "laporan/keuangan/buku_besar"; ?>" href="#">Buku besar</a></li>
                    <li class=<?php if ($page === 'laba_rugi') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/keuangan/laba_rugi"; ?>" href="#">Laba rugi</a></li>
                    <li class=<?php if ($page === 'neraca_saldo') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/keuangan/neraca_saldo"; ?>" href="#">Neraca saldo</a></li>
                    <li class=<?php if ($page === 'neraca') {
                                    # code...
                                    print 'active';
                                } ?>><a class="nav-link klik-menu" data-link="<?php print base_url() . "laporan/keuangan/neraca"; ?>" href="#">Neraca</a></li>
                </ul>
            </li>
            <!-- //closelaporankeuangan -->


            <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                <a href="#" class="klik-menu btn btn-primary btn-lg btn-block btn-icon-split" data-link="<?php print base_url() . 'auth/logout'; ?>">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>
            </div>
    </aside>
</div>