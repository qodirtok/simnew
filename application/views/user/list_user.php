<?php
$n = 1;
if (!empty($user)) {
	# code...
	foreach ($user as $key => $value) :
?>
		<tr>
			<td class="align-middle">
				<?php print $n; ?>
			</td>
			<td class="align-middle">
				<?php print $value['kd_user']; ?>
			</td>
			<td class="align-middle">
				<?php print $value['nama_user']; ?>
			</td>
			<td class="align-middle">
				<?php print $value['username']; ?>
			</td>
			<td class="align-middle">
				<?php print $value['group_user']; ?>
			</td>
			<td class="align-middle">
				<div class="buttons">
					<button class="btn btn-sm btn-icon btn-warning btndetail open-modal" data-link="<?php print base_url() . "user/show/" . $value['id_user']; ?>"><i class=" fas fa-eye"></i></button>
					<button class="btn btn-sm btn-icon btn-danger btndelete" data-link="<?php print base_url() . "user/destroy/" . $value['id_user']; ?>"><i class="fas fa-trash"></i></button>
				</div>
			</td>
		</tr>

<?php $n++;
	endforeach;
}
?>