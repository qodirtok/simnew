<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1><?php print $title; ?></h1>
		</div>

		<div class="section-body">

			<h2 class="section-title">Data User</h2>
			<p class="section-lead">
				Kelola Data User.
			</p>

			<div class="row">
				<div class="col-lg-4 col-md-12 col-12 col-sm-12">
					<div class="card">
						<div class="card-header">
							<h4>Tambah User</h4>
						</div>
						<div class="card-body">
							<form class="form">
								<div class="form-group kodeuser">
									<label for="Kode_User">Kode User</label>
									<input type="text" class=" form-control change" name="kode" id="kode" readonly>
								</div>
								<div class="form-group">
									<label for="Nama">Nama</label>
									<input type="text" class=" riset form-control" name="name" placeholder="">
									<div>
										<?php print form_error('name', '<small class="text-danger">', '</small>') ?>
									</div>
								</div>
								<div class="form-group">
									<label for="Username">Username</label>
									<input type="text" class="form-control" name="username">
									<div>
										<?php print form_error('username', '<small class="text-danger">', '</small>') ?>
									</div>
								</div>
								<div class="form-group">
									<label for="Password">Password</label>
									<input type="password" class="form-control" name="password">
									<div>
										<?php print form_error('password', '<small class="text-danger">', '</small>') ?>
									</div>
								</div>
								<div class="form-group">
									<label for="Alamat">Alamat</label>
									<textarea class="form-control" name="address" cols="30" rows="10"></textarea>
									<div>
										<?php print form_error('address', '<small class="text-danger">', '</small>') ?>
									</div>
								</div>
								<div class="form-group">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="customRadioInline1" name="role" class="custom-control-input" value="Administrator">
										<label class="custom-control-label" for="customRadioInline1">Administrator</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="customRadioInline2" name="role" class="custom-control-input" value="User">
										<label class="custom-control-label" for="customRadioInline2">User</label>
									</div>
									<div>
										<?php print form_error('role', '<small class="text-danger">', '</small>') ?>
									</div>
								</div>
								<div class="form-group">
									<?php print $buttonSave; ?>
									<?php print $buttonRestart; ?>
								</div>
							</form>
						</div>
					</div>
				</div>


				<div class="col-lg-8 col-md-12 col-12 col-sm-12">
					<div class="card ">
						<div class="card-header">
							<h4>Kelola Data User</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive ">
								<table class="table table-striped" id="list-Table">
									<thead>
										<tr>
											<th class="text-center">
												No
											</th>
											<th>Kode User</th>
											<th>Nama</th>
											<th>Username</th>
											<th>Group</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody class="data-list" data-link="<?php print base_url() . "user/tampil"; ?>">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</section>
</div>


<!-- //MODALL -->
<div class="tampil-modal"></div>