<div class="main-content">
    <section class="section">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
                <div class="card ">
                    <!-- <div class="card-header"> -->
                    <?php if ($this->session->flashdata('message')) { ?>
                        <div class="flashLogin" data-flashLogin="<?php print $this->session->flashdata('message'); ?>"></div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('modalMessage')) { ?>
                        <div class="flashModal" data-flashModal="<?php print $this->session->flashdata('modalMessage'); ?>"></div>
                    <?php } ?>
                    <!-- </div> -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <p>Modal hari ini</p>
                                <h5 class="getModal" data-link="<?php print base_url() . 'retur/getmodal'; ?>"></h5>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card-footer"></div> -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
                <div class="card card-statistic-1">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <form class="formCart">
                            <div class="row d-flex justify-content-end">
                                <div class="form-group">
                                    <a class="btn btn-primary" href="<?php print base_url() . 'retur'; ?>">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
                <div class="card ">
                    <div class="card-header">
                        <h4>Kelola Data User</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive ">
                            <table class="table table-striped" id="list-Table">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            No
                                        </th>
                                        <!-- <th>Kode barang / barcode</th> -->
                                        <th>Nama barang</th>
                                        <th>qty</th>
                                        <th>Harga satuan</th>
                                        <th>Diskon</th>
                                        <th>harga akhir</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="<?php print $listDataHistory; ?>">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

</section>
</div>

<!-- //MODALL -->
<div class="tampil-modal"></div>