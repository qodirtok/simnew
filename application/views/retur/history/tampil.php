<?php
$n = 1;
if (isset($getSearch)) {
    # code...
    foreach ($getSearch as $key => $value) :
        if (isset($value['id_barang_keluar'])) {
            $id = $value['id_barang_keluar'];
        } else if (isset($value['id_barang_masuk'])) {
            $id = $value['id_barang_masuk'];
        }
?>
        <tr>
            <td class="align-middle">
                <?php print $n; ?>
            </td>
            <td class="align-middle">
                <?php print $value['nama_barang']; ?>
            </td>
            <td class="align-middle">
                <?php print $value['stok']; ?>
            </td>
            <td class="align-middle">
                <?php print "Rp. " . number_format($value['harga_jual'], 2, ',', '.'); ?>
            </td>
            <td class="align-middle">
                <?php print $value['diskon'] . "%"; ?>
            </td>
            <td class="align-middle">
                <?php print "Rp. " . number_format($value['sub_total'], 2, ',', '.'); ?>
            </td>
            <td class="align-middle">
                <div class="buttons">
                    <button class="btn btn-sm btn-icon btn-warning btndetail open-modal" data-link="<?php print base_url() . "retur/show_history/" . $value['id_retur']; ?>"><i class=" fas fa-eye"></i></button>
                </div>
            </td>
        </tr>

<?php $n++;
    endforeach;
}
?>