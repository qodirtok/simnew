<div class="main-content">
	<section class="section">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
				<div class="card ">
					<!-- <div class="card-header"> -->
					<?php if ($this->session->flashdata('message')) { ?>
						<div class="flashLogin" data-flashLogin="<?php print $this->session->flashdata('message'); ?>"></div>
					<?php } ?>
					<?php if ($this->session->flashdata('modalMessage')) { ?>
						<div class="flashModal" data-flashModal="<?php print $this->session->flashdata('modalMessage'); ?>"></div>
					<?php } ?>
					<!-- </div> -->
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4">
								<p>Modal hari ini</p>
								<h5 class="getModal" data-link="<?php print base_url() . 'barang/getmodal'; ?>"></h5>
							</div>
						</div>
					</div>
					<!-- <div class="card-footer"></div> -->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
				<div class="card card-statistic-1">
					<div class="card-header">
					</div>
					<div class="card-body">
						<form class="formCart">
							<div class="row d-flex justify-content-between">
								<label for="Nota">Pilih Tipe Retur</label>
								<a class="btn btn-primary" href="<?php print base_url() . 'retur/history/'; ?>">Log History</a>
							</div>

							<div class="row">
								<div class="col-lg-3 col-sm-3 col-md-3 col-6">
									<div class="form-check">
										<input class="form-check-input" type="radio" name="tipe" value="pembelian" id="tipePembelian">
										<label class="form-check-label" for="tipePembelian">
											<small>Pembelian</small>
										</label>
									</div>
								</div>
								<div class="col-lg-3 col-sm-3 col-md-3 col-6">
									<div class="form-check">
										<input class="form-check-input" type="radio" name="tipe" value="penjualan" id="tipePenjualan">
										<label class="form-check-label text-sm" for="tipePenjualan">
											<small>Penjualan</small>
										</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-3 col-sm-3 col-md-3 col-3">
									<div class="form-group">
										<label for="Nota">No. Nota</label>
										<input type="text" class="form-control toDisable" name="nota" id="search_func_retur" value="">
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class=" card-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
								<div class="table-responsive">
									<table class="table table-striped" id="tabel-keranjang">
										<thead>
											<tr>
												<th class="text-center">
													No
												</th>
												<!-- <th>Kode barang / barcode</th> -->
												<th>Nama barang</th>
												<th>qty</th>
												<th>Harga satuan</th>
												<th>Diskon</th>
												<th>harga akhir</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody class="<?php print $listSearch; ?>">
									</table>
								</div>
							</div>
						</div>
						<!-- </div>
								</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>

	</section>
</div>

<!-- //MODALL -->
<div class="tampil-modal"></div>