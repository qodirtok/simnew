<div class="main-content">
	<section class="section">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
				<div class="card ">
					<!-- <div class="card-header"> -->
					<?php if ($this->session->flashdata('message')) { ?>
						<div class="flashLogin" data-flashLogin="<?php print $this->session->flashdata('message'); ?>"></div>
					<?php } ?>
					<?php if ($this->session->flashdata('modalMessage')) { ?>
						<div class="flashModal" data-flashModal="<?php print $this->session->flashdata('modalMessage'); ?>"></div>
					<?php } ?>
					<!-- </div> -->
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4">
								<p>Modal hari ini</p>
								<h5 class="getModal" data-link="<?php print base_url() . 'stokpembelianbarang/getmodal'; ?>"></h5>
							</div>
						</div>
					</div>
					<!-- <div class="card-footer"></div> -->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
				<div class="card card-statistic-1">
					<div class="card-header">
					</div>
					<div class="card-body">
						<form class="formCart">
							<div class="row">
								<div class="col-lg-3 col-sm-6 col-md-6 col-6">
									<div class="form-group">
										<label for="Nota">No. Nota</label>
										<input type="text" class="form-control" name="nota" id="">
									</div>
								</div>
								<div class="col-lg-3 col-sm-6 col-md-6 col-6">
									<div class="form-group">
										<label for="Nota">Tanggal</label>
										<input type="date" class="form-control" name="tanggal" id="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-3 col-md-6 col-6 col-sm-6">
									<div class="form-group">
										<label>Kategori</label>
										<select name="kategori" id="kategori" class="form-control select2">
											<option active>Pilih kategori</option>
											<?php foreach ($kategori as $value) { ?>
												<option value="<?php print $value['id_kategori']; ?>"><?php print $value['nama_kategori']; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-lg-3 col-md-6 col-6 col-sm-6">
									<div class="form-group">
										<label>Satuan</label>
										<select name="satuan" id="satuan" class="form-control select2">
											<option active>Pilih satuan</option>
											<?php foreach ($satuan as $value) { ?>
												<option value="<?php print $value['id_satuan']; ?>"><?php print $value['nama_satuan']; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-3 col-sm-6 col-md-6 col-6">
									<div class="form-group">
										<label for="Nota">kode</label>
										<input type="text" class="form-control" name="kode" id="">
									</div>
								</div>
								<div class="col-lg-3 col-sm-6 col-md-6 col-6">
									<div class="form-group">
										<label for="Nota">nama barang</label>
										<input type="text" class="form-control" name="namaBarang" id="">
									</div>
								</div>
								<div class="col-lg-1 col-sm-3 col-md-3 col-3">
									<div class="form-group">
										<label for="Nota">Jumlah/qty</label>
										<input type="text" class="form-control" name="qty" id="">
									</div>
								</div>
								<div class="col-lg-2 col-sm-3 col-md-3 col-3">
									<div class="form-group">
										<label for="Nota">Harga beli</label>
										<input type="text" class="form-control" name="hargaBeli" id="">
									</div>
								</div>
								<div class="col-lg-2 col-sm-3 col-md-3 col-3">
									<div class="form-group">
										<label for="Nota">Harga jual</label>
										<input type="text" class="form-control" name="hargaJual" id="">
									</div>
								</div>
								<div class="">
									<div class="form-group">
										<label for="Nota" class="">.</label>
										<button data-link="<?php print base_url() . 'stokpembelianbarang/addtocart' ?>" type="submit" class="btn-addCart form-control btn btn-icon btn-primary"> <i class="fas fa-plus"></i></button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="card-body">
						<!-- <div class="row ">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12"> -->
						<!-- <div class="card card-statistic-1">
									<div class="card-body"> -->
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
								<div class="table-responsive">
									<table class="table table-striped" id="tabel-keranjang">
										<thead>
											<tr>
												<th class="text-center">
													No
												</th>
												<!-- <th>Kode barang / barcode</th> -->
												<th>Nama barang</th>
												<th>qty</th>
												<th>Harga satuan</th>
												<th>harga akhir</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody class="<?php print $listData; ?>">


										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer">

						<!-- <div class="row"> -->
						<div class="float-right col-lg-2 col-md-2 col-sm-2 col-xl-2 col-">
							<div class="form-group">
								<form class="form">
									<input type="hidden" value="nothing" id="hidden" name="hidden">
									<?php print $buttonSave ?>
								</form>
							</div>
						</div>
						<!-- </div> -->
					</div>

				</div>
			</div>
		</div>

	</section>
</div>