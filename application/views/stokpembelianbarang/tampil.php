<?php
$n = 1;
if (!empty($tmp_pembelian)) {
    # code...
    foreach ($tmp_pembelian as $key => $value) :
?>
        <tr>
            <td class="align-middle">
                <?php print $n; ?>
            </td>
            <td class="align-middle">
                <?php print $value['nama_barang']; ?>
            </td>
            <td class="align-middle">
                <?php print $value['stok']; ?>
            </td>
            <td class="align-middle">
                <?php print "Rp. " . number_format($value['harga_jual'], 2, ',', '.'); ?>
            </td>
            <td class="align-middle">
                <?php print "Rp. " . number_format(($value['stok'] * $value['harga_jual']), 2, ',', '.'); ?>
            </td>
            <td class="align-middle">
                <div class="buttons">
                    <button class="btn btn-sm btn-icon btn-danger btndelete" data-link="<?php print base_url() . "stokpembelianbarang/destroy/" . $value['id_barang_masuk']; ?>"><i class="fas fa-trash"></i></button>
                </div>
            </td>
        </tr>

<?php $n++;
    endforeach;
}
?>