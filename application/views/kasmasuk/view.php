<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1><?php print $title; ?></h1>
		</div>

		<!-- <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
				<div class="card "> -->
		<!-- <div class="card-header"> -->
		<?php if ($this->session->flashdata('message')) { ?>
			<div class="flashLogin" data-flashLogin="<?php print $this->session->flashdata('message'); ?>"></div>
		<?php } ?>
		<?php if ($this->session->flashdata('modalMessage')) { ?>
			<div class="flashModal" data-flashModal="<?php print $this->session->flashdata('modalMessage'); ?>"></div>
		<?php } ?>
		<!-- </div> -->
		<!-- <div class="card-body">
							<div class="row">
								<div class="col-sm-4">
									<p>Modal hari ini</p>
									<h5 class="getModal" data-link="<?php print base_url() . 'barang/getmodal'; ?>"></h5>
								</div>
							</div>
						</div> -->
		<!-- <div class="card-footer"></div> -->
		<!-- </div>
			</div>
		</div> -->


		<div class="section-body">

			<h2 class="section-title">Data Kas</h2>
			<p class="section-lead">
				Kelola Data Kas Masuk.
			</p>

			<div class="row">
				<div class="col-lg-4 col-md-12 col-12 col-sm-12">
					<div class="card">
						<div class="card-header">
							<h4>Tambah Kas</h4>
						</div>
						<div class="card-body">
							<form class="form">
								<div class="form-group">
									<label>Jenis</label>
									<select name="kode_akun" id="kode_akun" class="form-control select2">
										<option active>Pilih Jenis Akun</option>
										<?php foreach ($kode_akun as $value) { ?>
											<option value="<?php print $value['kode_akun']; ?>"><?php print $value['nama_akun']; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label for="keterangan">Keterangan</label>
									<textarea class=" riset form-control" name="keterangan"></textarea>
								</div>
								<div class="form-group">
									<label for="jumlah">Jumlah</label>
									<input type="text" class="form-control" name="jumlah">
								</div>
								<div class="form-group">
									<?php print $buttonSave; ?>
									<?php print $buttonRestart; ?>
								</div>
							</form>
						</div>
					</div>
				</div>

				<div class="col-lg-8 col-md-12 col-12 col-sm-12">
					<div class="card ">
						<div class="card-header">
							<h4>Kelola Data Kas Masuk</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-striped" id="list-Table">
									<thead>
										<tr>
											<th class="text-center">
												No
											</th>
											<th>Jenis Akun</th>
											<th>Keterangan</th>
											<th>Debet</th>
											<th>Kredit</th>
											<th>Tanggal</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody class="<?php print $listData ?>">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</section>
</div>


<!-- //MODALL -->
<div class="tampil-modal"></div>