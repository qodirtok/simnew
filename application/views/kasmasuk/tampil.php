<?php
$n = 1;
$saldo_debet = 0;
if (!empty($kasmasuk)) {
	# code...
	foreach ($kasmasuk as $key => $value) :
		$saldo_debet += $value['debet'];
?>
		<tr>
			<td class="align-middle">
				<?php print $n; ?>
			</td>
			<td class="align-middle">
				<?php print $value['nama_akun']; ?>
			</td>
			<td class="align-middle">
				<?php print $value['keterangan']; ?>
			</td>
			<td class="align-middle">
				<?php print "Rp. " . number_format($value['debet'], 2, ',', '.'); ?>
			</td>
			<td class="align-middle">
				<?php print "Rp. " . number_format($value['kredit'], 2, ',', '.'); ?>
			</td>
			<td class="align-middle">
				<?php print date('Y-m-d', strtotime($value['tanggal'])); ?>
			</td>
			<td class="align-middle">
				<div class="buttons">
					<button class="btn btn-sm btn-icon btn-warning btndetail open-modal" data-link="<?php print base_url() . "kasmasuk/show/" . $value['id_kas_masuk']; ?>"><i class=" fas fa-eye"></i></button>
					<button class="btn btn-sm btn-icon btn-danger btndelete" data-link="<?php print base_url() . "kasmasuk/destroy/" . $value['id_kas_masuk']; ?>"><i class="fas fa-trash"></i></button>
				</div>
			</td>
		</tr>

	<?php $n++;
	endforeach;
	?>
	<!-- <tr>
		<td colspan="3 align-middle text-center">
			<strong>Jumlah</strong>
		</td>
		<td class="align-middle">
			<strong><?php print "Rp. " . number_format($saldo_debet, 2, ',', '.') ?></strong>
		</td>
	</tr> -->
<?php
}

?>