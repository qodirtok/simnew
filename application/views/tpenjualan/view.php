<div class="main-content">
	<section class="section">

		<!-- <div class="section-header">
			<h1>Top Navigation</h1>
			<div class="section-header-breadcrumb">
				<div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
				<div class="breadcrumb-item"><a href="#">Layout</a></div>
				<div class="breadcrumb-item">Top Navigation</div>
			</div>
		</div>
		<div class="section-body">
			<h2 class="section-title">This is Example Page</h2>
		</div> -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
				<div class="card ">
					<!-- <div class="card-header"> -->
					<?php if ($this->session->flashdata('message')) { ?>
						<div class="flashLogin" data-flashLogin="<?php print $this->session->flashdata('message'); ?>"></div>
					<?php } ?>
					<?php if ($this->session->flashdata('modalMessage')) { ?>
						<div class="flashModal" data-flashModal="<?php print $this->session->flashdata('modalMessage'); ?>"></div>
					<?php } ?>
					<!-- </div> -->
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4">
								<p>Modal hari ini</p>
								<h5 class="getModal" data-link="<?php print base_url() . 'tpenjualan/getmodal'; ?>"></h5>
							</div>
						</div>
					</div>
					<!-- <div class="card-footer"></div> -->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
				<div class="card card-statistic-1">
					<div class="card-header">
					</div>
					<div class="card-body">
						<form class="formCart">
							<div class="row">
								<div class="col-lg-3 col-sm-3 col-md-3 col-3">
									<div class="form-group">
										<label for="Nota">No. Nota</label>
										<input type="text" class="form-control toDisable" name="nota" id="nota_input" value="<?php print $nota ?>" readonly>
									</div>
								</div>
								<div class="col-lg-3 col-sm-3 col-md-3 col-3">
									<div class="form-group">
										<label for="Nota">Tanggal</label>
										<input type="text" class="toDisable form-control" value="<?php print date('d-m-Y'); ?>" name="tanggal" id="" readonly>
									</div>
								</div>
								<div class="col-lg-2 col-sm-6 col-md-6 col-6">
									<div class="form-group">
										<label for="Nota">kode/nama barang</label>
										<input type="text" class="toDisable form-control" name="kodeBarang" id="search_func">
									</div>
								</div>
								<div class="col-lg-1 col-sm-3 col-md-3 col-3">
									<div class="form-group">
										<label for="Nota">Jumlah/qty</label>
										<input type="text" class="form-control toDisable" name="qty" id="jumlah_func">
									</div>
								</div>
								<div class="col-lg-1 col-sm-3 col-md-3 col-3">
									<div class="form-group">
										<label for="Nota">Diskon(%)</label>
										<input type="text" class="form-control toDisable" name="diskon" id="" value="0">
									</div>
								</div>
								<div class="">
									<div class="form-group">
										<label for="Nota" class="">.</label>
										<button data-link="<?php print base_url() . 'tpenjualan/addtocart' ?>" type="submit" class="btn-addCart form-control btn btn-icon btn-primary"> <i class="fas fa-plus"></i></button>
									</div>
								</div>
							</div>
						</form>
						<div class="<?php print $listSearch ?>">
						</div>
					</div>
					<div class=" card-body">
						<!-- <div class="row ">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12"> -->
						<!-- <div class="card card-statistic-1">
									<div class="card-body"> -->
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
								<div class="table-responsive">
									<table class="table table-striped" id="tabel-keranjang">
										<thead>
											<tr>
												<th class="text-center">
													No
												</th>
												<!-- <th>Kode barang / barcode</th> -->
												<th>Nama barang</th>
												<th>qty</th>
												<th>Harga satuan</th>
												<th>Diskon</th>
												<th>harga akhir</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody class="<?php print $listData; ?>">
									</table>
								</div>
							</div>
						</div>
						<!-- </div>
								</div>
							</div>
						</div> -->
					</div>
					<div class="card-footer">
						<form class="form">
							<div class="row">
								<div class="col-lg-2 col-md-2 col-sm-2 col-xl-2 col-2">
									<div class="form-group">
										<label for="">Sub.total</label>
										<input type="text" name="subtotal" class="form-control" id="subtotal_input" value="<?php print $sub_total ?>" readonly>
									</div>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xl-2 col-2">
									<div class="form-group">
										<label for="">Diskon</label>
										<input type="text" name="diskon" class="form-control" id="diskon_func" value="0">
									</div>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xl-2 col-2">
									<div class="form-group">
										<label for="">Total</label>
										<input type="text" name="total" class="form-control" id="total_input" value="<?php print $sub_total ?>" readonly>
									</div>
								</div>
								<!-- </div> -->
								<!-- <div class="row"> -->
								<div class="col-lg-2 col-md-2 col-sm-2 col-xl-2 col-2">
									<div class="form-group">
										<label for="">Bayar</label>
										<input type="text" name="bayar" class="form-control" id="bayar_func" value="0">
									</div>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xl-2 col-2">
									<div class="form-group">
										<label for="">Kembalian</label>
										<input type="text" name="kembalian" class="form-control" id="kembalian_input" readonly>
									</div>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xl-2 col-2">
									<div class="form-group">
										<label for="Nota" class="">.</label>
										<?php print $buttonSave ?>
									</div>
								</div>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>

	</section>
</div>

<!-- //MODALL -->
<div class="tampil-modal"></div>