<?php
$n = 1;
if (!empty($tmp_penjualan)) {
    # code...
    foreach ($tmp_penjualan as $key => $value) :
?>
        <tr>
            <td class="align-middle">
                <?php print $n; ?>
            </td>
            <td class="align-middle">
                <?php print $value['nama_barang']; ?>
            </td>
            <td class="align-middle">
                <?php print $value['stok']; ?>
            </td>
            <td class="align-middle">
                <?php print "Rp. " . number_format($value['harga_jual'], 2, ',', '.'); ?>
            </td>
            <td class="align-middle">
                <?php print $value['diskon'] . "%"; ?>
            </td>
            <td class="align-middle">
                <?php print "Rp. " . number_format((($value['harga_jual'] - ($value['harga_jual'] * ($value['diskon'] / 100))) * $value['stok']), 2, ',', '.'); ?>
            </td>
            <td class="align-middle">
                <div class="buttons">
                    <button class="btn btn-sm btn-icon btn-danger btndelete" data-link="<?php print base_url() . "tpenjualan/destroy/" . $value['id_barang_keluar']; ?>"><i class="fas fa-trash"></i></button>
                </div>
            </td>
        </tr>

<?php $n++;
    endforeach;
}
?>