<?php
$n = 1;
if (!empty($getSearch)) {
    # code...
    foreach ($getSearch as $key => $value) :
?>
        <div class="row">
            <div class="col-lg-2 col-sm-3 col-md-3 col-3">
                <div class="form-group">
                    <label for="Nota">Kode Barang</label>
                    <input type="text" class="toDisable form-control" value="<?php print $value['kode_barang']; ?>" readonly>
                </div>
            </div>
            <div class="col-lg-3 col-sm-3 col-md-3 col-3">
                <div class="form-group">
                    <label for="Nota">Nama Barang</label>
                    <input type="text" class="toDisable form-control" value="<?php print $value['nama_barang']; ?>" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-sm-3 col-md-3 col-3">
                <div class="form-group">
                    <label for="Nota">Stok Tersedia</label>
                    <input type="text" class="toDisable form-control" value="<?php print $value['stok']; ?>" readonly>
                </div>
            </div>
            <div class="col-lg-3 col-sm-3 col-md-3 col-3">
                <div class="form-group">
                    <label for="Nota">Harga Beli</label>
                    <input type="text" class="toDisable form-control" value="<?php print $value['harga_beli']; ?>" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-sm-3 col-md-3 col-3">
                <div class="form-group">
                    <label for="Nota">Harga Jual</label>
                    <input type="text" class="toDisable form-control" value="<?php print $value['harga_jual']; ?>" readonly>
                </div>
            </div>
        </div>

<?php $n++;
    endforeach;
}
?>