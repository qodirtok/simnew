<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php print $title; ?></h1>
        </div>
        <h2 class="section-title"><?php print $sub_title; ?></h2>
        <p class="section-lead">
            Lihat <?php print $sub_title; ?>.
        </p>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <form class="form">
                                <input type="hidden" name="tipe" value="<?php print $tipe ?>">
                                <div class="form-row">
                                    <div class="col">
                                        <label for="tanggal" class="control-label">Tanggal Awal</label>
                                        <input type="date" class="input-sm form-control" name="tanggal" placeholder="Tanggal Awal" required />
                                    </div>
                                    <div class="col">
                                        <label for="sd" class="control-label text-center">-</label>
                                        <div class="input-group-prepend">
                                            <span name="sd" class="input-group-text">s.d</span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <label for="tanggal2" class="control-label">Tanggal Akhir</label>
                                        <input type="date" class="input-sm form-control" name="tanggal2" placeholder="Tanggal Akhir" required />
                                    </div>
                                </div>
                                <div class="form-group mt-4">
                                    <?php print $buttonCetak ?>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class=" card-body">
                        <div class="row">

                            <div class="table-responsive ">
                                <div class="form-group d-flex justify-content-end">
                                    <?php print $buttonLinktoCetak; ?>
                                </div>
                                <table class="table table-striped" id="tabel-keranjang">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Tanggal</th>
                                            <th>Tipe</th>
                                            <th>Nama Barang</th>
                                            <th>Harga Satuan</th>
                                            <th>Diskon</th>
                                            <th>Qty</th>
                                            <th>Sub Total</th>
                                            <th>Saldo</th>
                                        </tr>
                                    </thead>
                                    <tbody class="<?php print $listLaporan ?>">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- </div>
								</div>
							</div>
						</div> -->
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>