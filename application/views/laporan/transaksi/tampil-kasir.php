    <?php
    $n = 1;
    $saldo = 0;
    $diskon = 0;
    if (!empty($getSearch)) {
        # code...
        foreach ($getSearch as $key => $value) :
            if ($value['keterangan'] == 'Barang Masuk') {
                $tanggal = $value['tanggal_beli'];
                $harga = $value['harga_beli'];
                $diskon = $value['diskon_beli'];
                $sub_total = ($harga - ($harga * ($diskon / 100))) * $value['stok'];
                $saldo -= $sub_total;
                $tipe = "Pembelian";
            } else if ($value['keterangan'] == 'Barang Keluar') {
                $tanggal = $value['tanggal_jual'];
                $harga = $value['harga_jual'];
                $diskon = $value['diskon_jual'];
                $sub_total = ($harga - ($harga * ($diskon / 100))) * $value['stok'];
                $saldo += $sub_total;
                $tipe = "Penjualan";
            }
    ?>
            <tr>
                <td class="align-middle">
                    <?php print $n; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $tanggal; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $tipe; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $value['nama_barang']; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print "Rp. " . number_format($harga, 2, ',', '.'); ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $diskon . "%"; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $value['stok']; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print "Rp. " . number_format($sub_total, 2, ',', '.'); ?>
                </td>
                <td class="align-middle text-center">
                    <?php print "Rp. " . number_format($saldo, 2, ',', '.'); ?>
                </td>
            </tr>

        <?php $n++;
        endforeach;
        ?>

        <tr>
            <td colspan="8">
                Jumlah
            </td>
            <td class="align-middle text-center">
                <?php print "Rp. " . number_format($saldo, 2, ',', '.') ?>
            </td>
        </tr>
    <?php
    }

    ?>