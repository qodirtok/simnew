<?php
$n = 1;
$saldo = 0;
$sub_total = 0;
if (!empty($getSearch)) {
    # code...

    foreach ($getSearch as $key => $value) :
        $sub_total = ($value['harga_jual'] - ($value['harga_jual'] * ($value['diskon'] / 100))) * $value['stok'];
        $saldo += $sub_total;
?>
        <tr>
            <td class="align-middle">
                <?php print $n; ?>
            </td>
            <td class="align-middle">
                <?php print $value['nama_barang']; ?>
            </td>
            <td class="align-middle">
                <?php print $value['tanggal']; ?>
            </td>
            <td class="align-middle">
                <?php print $value['harga_jual']; ?>
            </td>
            <td class="align-middle">
                <?php print $value['diskon'] . "%"; ?>
            </td>
            <td class="align-middle">
                <?php print $value['stok']; ?>
            </td>
            <td class="align-middle">
                <?php print "Rp. " . number_format($sub_total, 2, ',', '.'); ?>
            </td>
            <td class="align-middle">
                <?php print "Rp. " . number_format($saldo, 2, ',', '.'); ?>
            </td>
        </tr>

    <?php $n++;
    endforeach;
    ?>
    <tr>
        <td colspan="7">
            Jumlah
        </td>
        <td>
            <?php print "Rp. " . number_format($saldo, 2, ',', '.') ?>
        </td>
    </tr>
<?php
}
?>