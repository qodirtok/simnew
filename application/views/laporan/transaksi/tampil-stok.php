    <?php
    $n = 1;
    $saldo = 0;
    if (!empty($getSearch)) {
        # code...
        foreach ($getSearch as $key => $value) :
            if ($value['keterangan'] == 'Barang Keluar' || $value['keterangan'] == 'Retur Barang Masuk') {
                $saldo -= $value['stok'];
            } else if ($value['keterangan'] == 'Barang Masuk') {
                $saldo += $value['stok'];
            }
    ?>
            <tr>
                <td class="align-middle">
                    <?php print $n; ?>
                </td>
                <td class="align-middle">
                    <?php print $value['nama_barang']; ?>
                </td>
                <td class="align-middle">
                    <?php print $value['keterangan']; ?>
                </td>
                <td class="align-middle">
                    <?php print $value['tanggal']; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $value['stok']; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $saldo; ?>
                </td>
            </tr>

        <?php $n++;
        endforeach;
        ?>

        <tr>
            <td colspan="5">
                Jumlah
            </td>
            <td class="align-middle text-center">
                <?php print $saldo ?>
            </td>
        </tr>
    <?php
    }

    ?>