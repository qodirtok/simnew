<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php print $title; ?></h1>
        </div>
        <h2 class="section-title"><?php print $sub_title; ?></h2>
        <p class="section-lead">
            Lihat <?php print $sub_title; ?>.
        </p>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <form class="form">
                                <div class="form-row">
                                    <input type='hidden' name='tipe' value="<?php print $tipe ?>">
                                    <div class="col">
                                        <label for="tanggal" class="control-label">Tanggal Awal</label>
                                        <input type="date" class="input-sm form-control" name="tanggal" placeholder="Tanggal Awal" required />
                                    </div>
                                    <div class="col">
                                        <label for="sd" class="control-label text-center">-</label>
                                        <div class="input-group-prepend">
                                            <span name="sd" class="input-group-text">s.d</span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <label for="tanggal2" class="control-label">Tanggal Akhir</label>
                                        <input type="date" class="input-sm form-control" name="tanggal2" placeholder="Tanggal Akhir" required />
                                    </div>
                                </div>
                                <div class="form-group mt-4">
                                    <?php print $buttonLinktoCetak ?>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- </div>
								</div>
							</div>
						</div> -->
                </div>
            </div>
        </div>
</div>

</section>
</div>