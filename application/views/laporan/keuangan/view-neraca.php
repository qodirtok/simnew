<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php print $title; ?></h1>
        </div>
        <h2 class="section-title"><?php print $sub_title; ?></h2>
        <p class="section-lead">
            Lihat <?php print $sub_title; ?>.
        </p>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <form class="form">
                                <div class="form-row">
                                    <input type='hidden' name='tipe' value="<?php print $tipe ?>">
                                    <div class="col">
                                        <label for="tanggal" class="control-label">Tanggal</label>
                                        <input type="date" class="input-sm form-control" name="tanggal" required />
                                    </div>
                                </div>
                                <div class="form-group mt-4">
                                    <?php print $buttonLinktoCetak ?>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- </div>
								</div>
							</div>
						</div> -->
                </div>
            </div>
        </div>
</div>

</section>
</div>