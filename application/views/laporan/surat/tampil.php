    <?php
    $n = 1;
    $saldo = 0;
    if (!empty($getSearch)) {
        # code...
        foreach ($getSearch as $key => $value) :
            if ($value['type'] == 'keluar') {
                $tipe = 'Surat Keluar';
            } else if ($value['type'] == 'masuk') {
                $tipe = 'Surat Masuk';
            }
    ?>
            <tr>
                <td class="align-middle">
                    <?php print $n; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $value['nomor_surat']; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $tipe; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $value['instansi']; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $value['topik']; ?>
                </td>
                <td class="align-middle text-center">
                    <?php print $value['tanggal']; ?>
                </td>
            </tr>

        <?php $n++;
        endforeach;
        ?>

        <tr>
            <td colspan="6">
                Jumlah
            </td>
            <td class="align-middle text-center">
                <?php print $n - 1 ?>
            </td>
        </tr>
    <?php
    }

    ?>