<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1><?php print $title; ?></h1>
		</div>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 col-12">
				<div class="card ">
					<!-- <div class="card-header"> -->
					<?php if ($this->session->flashdata('message')) { ?>
						<div class="flashLogin" data-flashLogin="<?php print $this->session->flashdata('message'); ?>"></div>
					<?php } ?>
					<?php if ($this->session->flashdata('modalMessage')) { ?>
						<div class="flashModal" data-flashModal="<?php print $this->session->flashdata('modalMessage'); ?>"></div>
					<?php } ?>
					<!-- </div> -->
					<?php if (
						$this->session->userdata('role') != "Administrator"
					) {
					?>
						<div class="card-body">
							<div class="row">
								<div class="col-sm-4">
									<p>Modal hari ini</p>
									<h5 class="getModal" data-link="<?php print base_url() . 'barang/getmodal'; ?>"></h5>
								</div>
							</div>
						</div>
					<?php } ?>
					<!-- <div class="card-footer"></div> -->
				</div>
			</div>
		</div>

		<div class="section-body">

			<h2 class="section-title">Data <?php print $title; ?></h2>
			<p class="section-lead">
				Kelola Data <?php print $title; ?>.
			</p>

			<div class="row">
				<div class="col-lg-4 col-md-12 col-12 col-sm-12">
					<div class="card ">
						<div class="card-header">
							<h4>Tambah <?php print $title; ?></h4>
						</div>
						<div class="card-body">
							<form class="form">
								<div class="form-group">
									<label for="Nama">Nama Kategori</label>
									<input type="text" class=" riset form-control" name="name" placeholder="">
									<div>
										<?php print form_error('name', '<small class="text-danger">', '</small>') ?>
									</div>
								</div>
								<div class="form-group">
									<?php print $buttonSave; ?>
									<?php print $buttonRestart; ?>
								</div>
							</form>
						</div>
					</div>
				</div>


				<div class="col-lg-8 col-md-12 col-12 col-sm-12">
					<div class="card ">
						<div class="card-header">
							<h4>Kelola Data <?php print $title; ?></h4>
						</div>
						<div class="card-body">
							<div class="table-responsive ">
								<table class="table table-striped" id="list-Table">
									<thead>
										<tr>
											<th class="text-center">
												No
											</th>
											<th>Kode User</th>

											<th>Aksi</th>
										</tr>
									</thead>
									<tbody class="<?php print $listData; ?>">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</section>
</div>


<!-- //MODALL -->
<div class="tampil-modal"></div>