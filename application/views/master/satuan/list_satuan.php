<?php
$n = 1;
if (!empty($satuan)) {
	# code...
	foreach ($satuan as $key => $value) :
?>
		<tr>
			<td class="align-middle">
				<?php print $n; ?>
			</td>
			<td class="align-middle">
				<?php print $value['nama_satuan']; ?>
			</td>

			<td class="align-middle">
				<div class="buttons">
					<button class="btn btn-sm btn-icon btn-warning btndetail open-modal" data-link="<?php print base_url() . "satuan/show/" . $value['id_satuan']; ?>"><i class=" fas fa-eye"></i></button>
					<button class="btn btn-sm btn-icon btn-danger btndelete" data-link="<?php print base_url() . "satuan/destroy/" . $value['id_satuan']; ?>"><i class="fas fa-trash"></i></button>
				</div>
			</td>
		</tr>

<?php $n++;
	endforeach;
}
?>
