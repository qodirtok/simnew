<!-- Main Content -->
<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1> Tambah <?php print $title; ?></h1>
		</div>

		<div class="section-body">
			<h2 class="section-title">Tambah <?php print $title; ?></h2>
			<p class="section-lead">
				Form Tambah <?php print $title; ?>
			</p>

			<div class="row">
				<div class="col-lg-12 col-md-12 col-12 col-sm-12">
					<div class="card">
						<form class="form">
							<div class="card-header">
								<h4>Tambah <?php print $title; ?></h4>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-lg-6 col-md-12 col-12 col-sm-12">
										<div class="form-group">
											<label>Kode barang / barcode</label>
											<input type="text" class="form-control" name="kd" required="">
										</div>
									</div>
									<div class="col-lg-6 col-md-12 col-12 col-sm-12">
										<div class="form-group">
											<label>Nama Barang</label>
											<input type="text" name="name" class="form-control" required="">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6 col-md-12 col-12 col-sm-12">
										<div class="form-group">
											<label>Kategori</label>
											<select name="kategori" class="form-control select2">
												<option active>Pilih kategori</option>
												<?php foreach ($kategori as $value) { ?>
													<option value="<?php print $value['id_kategori']; ?>"><?php print $value['nama_kategori']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="col-lg-6 col-md-12 col-12 col-sm-12">
										<div class="form-group">
											<label>Satuan</label>
											<select name="satuan" class="form-control select2">
												<option active>Pilih satuan</option>
												<?php foreach ($satuan as $value) { ?>
													<option value="<?php print $value['id_satuan']; ?>"><?php print $value['nama_satuan']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6 col-md-12 col-12 col-sm-12">
										<div class="form-group">
											<label>Harga beli</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<div class="input-group-text">
														Rp
													</div>
												</div>
												<input type="text" name="hrg_beli" class="form-control rupiah">
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-12 col-12 col-sm-12">
										<div class="form-group">
											<label>Harga Jual</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<div class="input-group-text">
														Rp
													</div>
												</div>
												<input type="text" name="hrg_jual" class="form-control rupiah">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class=" card-footer text-left">
								<?php print $buttonSave; ?>
								<?php print $buttonRestart; ?>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
