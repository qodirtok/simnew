<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('MY_Model', 'mod');
		$this->load->library('form_validation');
		$this->load->library('Template');
		$this->load->library('Createcode');
		$this->load->library('Modal');
		$this->load->library('cart');
		$this->load->library('unit_test');
		// MY_Controller::is_logged_in();
	}

	public function is_logged_in()
	{
		//check username from userdata 
		if ($this->session->userdata('username') == null || $this->session->userdata('username') == '') {
			# code...
			// var_dump($this->session->userdata());
			// die();
			redirect(base_url());
			//check role group
			if ($this->session->userdata('role') != null || $this->session->userdata('role') != '') {
				# code...
				redirect(base_url());
			}
		}
	}

	public function checkAdmin()
	{
		# code...
		if ($this->session->userdata('role') != 'Administrator') {
			# code...
			redirect(base_url());
		}
	}

	public function do_upload($scan, $name, $path)
	{
		# code...

		$config['upload_path'] =	$path;
		$config['allowed_types']	=	'gif|jpg|png|jpeg|JPEG';
		$config['overwrite']	=	TRUE;
		// $config['encrypt_name']		=	TRUE;

		$this->load->library('upload', $config);
		if ($this->upload->do_upload($name)) {
			# code...
			$data	=	array('upload_data'	=>	$this->upload->data());

			$config['image_library']	=	'gd2';
			$config['source_image']		=	$path . $scan['name'];
			$config['create_thumb']		=	FALSE;
			$config['maintain_ration']	=	FALSE;
			$config['quality']			=	'60%';
			$config['width']			=	600;
			$config['height']			=	600;
			$config['new_image']		=	$path . $scan['name'];
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();

			$image	=	$scan['name'];

			return $image;
		}
	}

	public function do_delete($path, $file)
	{
		# code...

		if ($file != 'default.jpg') {
			# code...
			$fileName = explode('.', $file['scan'])[0];
			return array_map('unlink', glob(FCPATH, $path . $file['scan']));
		}
	}

	public function error()
	{
		# code...
		$this->load->view('error/404_found');
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */
